<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_model extends CI_Model
{
	//fungsi cek session
    function logged_id()
    {
        return $this->session->userdata('id_users');
    }

	
    function cek_order(){
        $id_user = $this->session->userdata('id_user');
        $cek = $this->db->get_where('transaksi_temp', array('id_user' => $id_user));
        return $cek->num_rows();
    }

    function genrate_id(){
        $id_user = $this->session->userdata('id_user');
           $code = $this->db->query('SELECT  genrate_id from transaksi_temp  ORDER BY id DESC LIMIT 1');
            if($code->num_rows() > 0){
                $code = $code->row_array();
                $kodemax = $code['genrate_id'];
                $noUrut = substr($kodemax, 3, 4);
                $noUrut++;
                $kode_nota = sprintf("%04s", $noUrut);
                return $genrate_id = 'TR'.$id_user.$kode_nota;
                

            }else{
                return $genrate_id = 'TR'.$id_user.'0001'; 
               
                             
            }
    }

    function hari_pengiriman(){
        $sql = "SELECT 
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Monday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Monday'), '%X%V %W')),'tidak aktif') as senin,
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Wednesday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Wednesday'), '%X%V %W')),'tidak aktif') as rabu,
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Friday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),WEEK(NOW()), ' Friday'), '%X%V %W')),'tidak aktif') as jumat,
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Monday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Monday'), '%X%V %W')),'tidak aktif') as senin_depan,
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Wednesday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Wednesday'), '%X%V %W')),'tidak aktif') as rabu_depan,
        IF((SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Friday'), '%X%V %W'))>=DATE(NOW()),(SELECT STR_TO_DATE(CONCAT(YEAR(NOW()),(WEEK(NOW())+1), ' Friday'), '%X%V %W')),'tidak aktif') as jumat_depan";

        $get = $this->db->query($sql)->row_array();

    
        $tgl = array( $get['senin'], $get['rabu'], $get['jumat'], $get['senin_depan'], $get['rabu_depan'], $get['jumat_depan']);

        return $tgl;

    }
}
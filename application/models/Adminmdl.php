<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmdl extends CI_Model
{
	//fungsi cek session
    function logged_id()
    {
        return $this->session->userdata('id_users');
    }


    function count_all($table){
        $get = $this->db->query("SELECT * FROM produk ")->num_rows();

        return $get;
    }

    public function filter($search, $limit, $start, $order_field, $order_ascdesc){
        //$this->db->or_like('nama', $search); // Untuk menambahkan query where OR LIKE
        //$this->db->order_by($order_field, $order_ascdesc); // Untuk menambahkan query ORDER BY
        //$this->db->limit($limit, $start); // Untuk menambahkan query LIMIT
        $orderby = ' ORDER BY ';
        
            $search_sql = '';
        if($search != ''){
            $search_sql = " WHERE a.nama LIKE '%".$search."%' OR a.kode LIKE '%".$search."%' ";
        }

        if($order_field == 'kode_produk'){
            $order_field = 'a.kode_produk';
        }

        if($order_field == ''){
            $order_field = '';
            $order_ascdesc = '';
        }

        if($order_field == '0'){
            $order_field = '';
            $order_ascdesc = '';
            $orderby= '';
        }

        return $this->db->query("SELECT a.*,b.nama_kategori, c.kode_produk , c.nama_kode FROM produk a 
            LEFT JOIN kategori b ON a.id_kategori = b.id_kategori
            LEFT JOIN kode_produk c ON a.kode_produk = c.id
            ".$search_sql." GROUP BY a.id ".$orderby." ".$order_field." ".$order_ascdesc." LIMIT ".$start.",".$limit);

           

        // $this->db->get($table); // Eksekusi query sql sesuai kondisi diatas
    }

    public function count_filter($search){
       
            $search_sql = '';
        if($search != ''){
             $search_sql = " WHERE a.nama LIKE '%".$search."%' OR a.kode LIKE '%".$search."%' ";
        }

        return $this->db->query("SELECT a.*,b.nama_kategori,  c.kode_produk , c.nama_kode FROM produk a LEFT JOIN kategori b ON a.id_kategori = b.id_kategori LEFT JOIN kode_produk c ON a.kode_produk = c.kode_produk ".$search_sql." GROUP BY a.id")->num_rows();
    }


}
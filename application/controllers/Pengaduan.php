<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

	public function index()
	{
		$this->load->view('pengaduan');
	}

	public function simpan_pengaduan(){
		if($post = $this->input->post()){
			$data['nama'] = $post['nama'];
			$data['tlp'] = $post['tlp'];
			$data['judul'] = $post['judul'];
			$data['pesan'] = $post['pesan'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['id_user'] = $this->session->userdata('iduser');

			if($this->db->insert('pengaduan',$data)){
				echo 1;
			}
		}
	}

}

 ?>
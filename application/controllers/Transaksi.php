<?php 
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

    public function cetak_struk() {
        // me-load library escpos
        $this->load->library('escpos');

        // membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
        $connector = new Escpos\PrintConnectors\WindowsPrintConnector("pt210");

        // membuat objek $printer agar dapat di lakukan fungsinya
        $printer = new Escpos\Printer($connector);


        /* ---------------------------------------------------------
         * Teks biasa | text()
         */
        $printer->initialize();
        $printer->text("Ini teks biasa \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Select print mode | selectPrintMode()
         */
        // Printer::MODE_FONT_A
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_FONT_A);
        $printer->text("teks dengan MODE_FONT_A \n");
        $printer->text("\n");

        // Printer::MODE_FONT_B
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_FONT_B);
        $printer->text("teks dengan MODE_FONT_B \n");
        $printer->text("\n");

        // Printer::MODE_EMPHASIZED
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_EMPHASIZED);
        $printer->text("teks dengan MODE_EMPHASIZED \n");
        $printer->text("\n");

        // Printer::MODE_DOUBLE_HEIGHT
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_HEIGHT);
        $printer->text("teks dengan MODE_DOUBLE_HEIGHT \n");
        $printer->text("\n");

        // Printer::MODE_DOUBLE_WIDTH
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
        $printer->text("teks dengan MODE_DOUBLE_WIDTH \n");
        $printer->text("\n");

        // Printer::MODE_UNDERLINE
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_UNDERLINE);
        $printer->text("teks dengan MODE_UNDERLINE \n");
        $printer->text("\n");


        /* ---------------------------------------------------------
         * Teks dengan garis bawah  | setUnderline()
         */
        $printer->initialize();
        $printer->setUnderline(Escpos\Printer::UNDERLINE_DOUBLE);
        $printer->text("Ini teks dengan garis bawah \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Rata kiri, tengah, dan kanan (JUSTIFICATION) | setJustification()
         */
        // Teks rata kiri JUSTIFY_LEFT
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_LEFT);
        $printer->text("Ini teks rata kiri \n");
        $printer->text("\n");

        // Teks rata tengah JUSTIFY_CENTER
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        $printer->text("Ini teks rata tengah \n");
        $printer->text("\n");

        // Teks rata kanan JUSTIFY_RIGHT
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_RIGHT);
        $printer->text("Ini teks rata kanan \n");
        $printer->text("\n");


        /* ---------------------------------------------------------
         * Font A, B dan C | setFont()
         */
        // Teks dengan font A
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_A);
        $printer->text("Ini teks dengan font A \n");
        $printer->text("\n");

        // Teks dengan font B
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_B);
        $printer->text("Ini teks dengan font B \n");
        $printer->text("\n");

        // Teks dengan font C
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_C);
        $printer->text("Ini teks dengan font C \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Jarak perbaris 40 (linespace) | setLineSpacing()
         */
        $printer->initialize();
        $printer->setLineSpacing(40);
        $printer->text("Ini paragraf dengan \nline spacing sebesar 40 \ndi printer dotmatrix \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Jarak dari kiri (Margin Left) | setPrintLeftMargin()
         */
        $printer->initialize();
        $printer->setPrintLeftMargin(10);
        $printer->text("Ini teks berjarak 10 dari kiri (Margin left) \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * membalik warna teks (background menjadi hitam) | setReverseColors()
         */
        $printer->initialize();
        $printer->setReverseColors(TRUE);
        $printer->text("Warna Teks ini terbalik \n");
        $printer->text("\n");


        /* ---------------------------------------------------------
         * Menyelesaikan printer
         */
        $printer->feed(5); // mencetak 5 baris kosong, agar kertas terangkat ke atas
        $printer->close();
    }

    public function cetak($id_penjualan) {
        // me-load library escpos
        $this->load->library('escpos');

        // membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
        $connector = new Escpos\PrintConnectors\WindowsPrintConnector("pt210");

        // membuat objek $printer agar dapat di lakukan fungsinya
        $printer = new Escpos\Printer($connector);



        // $printer->initialize();
        // $printer->selectPrintMode(Escpos\Printer::MODE_FONT_A);
        // $printer->text("teks dengan MODE_FONT_A \n");
        // $printer->text("\n");

        

        // $printer->initialize();
        // $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        // $printer->text("Ini teks rata tengah \n");
        // $printer->text("\n");

        $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
        $printer->text("Kolabs Kopi\n");
        $printer->selectPrintMode();
        $printer->text("Jalan Prenjak timur 7.\n");
        $printer->feed();

        //penjualan detail
        $penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
            LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
            LEFT JOIN produk c ON b.id_produk = c.id 
            WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
        $penjualan = $this->db->query("SELECT * FROM penjualan WHERE id=".$id_penjualan)->row_array();
        $harga_subtotal = decimals($penjualan['sub_total']);
        $total_bayar = decimals($penjualan['bayar']);
        $total_kembalian = decimals($penjualan['kembali']);

        $text_total = strlen($harga_subtotal);
        $text_bayar = strlen($total_bayar);
        $text_kembali = strlen($total_kembalian);

        $hasil_total = ((int)32 - (int)13 - (int)$text_total);
        $hasil_bayar = ((int)32 - (int)11 - (int)$text_bayar);
        $hasil_kembali = ((int)32 - (int)9 - (int)$text_kembali); 

        $spaci_total = array();
        for ($i=1; $i <= $hasil_total; $i++) { 
            $spaci_total[] =  ' ';

        } 

        $imp1 = implode("|",$spaci_total);
        $spaci1 = str_replace("|", "", $imp1);

        $spaci_total2 = array();
        for ($i=1; $i <= $hasil_bayar; $i++) { 
            $spaci_total2[] =  ' ';

        } 

        $imp2 = implode("|",$spaci_total2);
        $spaci2 = str_replace("|", "", $imp2);


        $spaci_total3 = array();
        for ($i=1; $i <= $hasil_kembali; $i++) { 
            $spaci_total3[] =  ' ';

        } 

        $imp3 = implode("|",$spaci_total3);
        $spaci3 = str_replace("|", "", $imp3);




      







        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_LEFT);
        $printer->text("--------------------------------");
        

        foreach ($penjualan_detail as $row) {


        $jumlah = $row['qty'].' '; //2 text
        $nama = $row['nama']; //7 text
        $harga = decimals($row['total']); // 7 text

        $total_text =  strlen($jumlah) + strlen($nama) + strlen($harga);
        $total_spaci = ((int)32 - (int)$total_text);

        $spaci_array= array();
        for ($i=1; $i <= $total_spaci; $i++) { 
            $spaci_array[] =  ' ';

        } 

        $imp = implode("|",$spaci_array);
        $spaci = str_replace("|", "", $imp);


        $printer->text($jumlah);
        $printer->text($nama);
        $printer->text($spaci);
        $printer->text($harga);

        }
         $printer->text("\n");
         $printer->text("--------------------------------");

        $printer->text('Total Tagihan');
        $printer->text($spaci1);
        $printer->text($harga_subtotal);
        $printer->text("\n");
        $printer->text("--------------------------------");

        $printer->text('Total Bayar');
        $printer->text($spaci2);
        $printer->text($total_bayar);

         $printer->text('Kembalian');
        $printer->text($spaci3);
        $printer->text($total_kembalian);

        





        /* ---------------------------------------------------------
         * Jarak perbaris 40 (linespace) | setLineSpacing()
         */
        // $printer->initialize();
        // $printer->setLineSpacing(40);
        // $printer->text("Ini paragraf dengan \nline spacing sebesar 40 \ndi printer dotmatrix \n");
        // $printer->text("\n");




        /* ---------------------------------------------------------
         * Menyelesaikan printer
         */
        $printer->feed(4); // mencetak 5 baris kosong, agar kertas terangkat ke atas
        $printer->close();
    }
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

    public function update_harga(){

        $get = $this->db->get('produk')->result_array();
        foreach ($get as $row) {
            $up['harga_beli'] = stringtonumber($row['harga_beli']);
            $up['harga'] = stringtonumber($row['harga']);
            $up['harga_3_satuan'] = stringtonumber($row['harga_3_satuan']);

            $id = $row['id'];

            $this->db->update('produk',$up, array('id'  => $id));
            echo $this->db->last_query()."<br>";

        }
    }

    public function delete_stock(){
        if($this->db->delete('produk_transaksi',array('no_nota' => 'SM20210214073209'))){
            echo $this->db->last_query();
        }
    }

    public function upload_stok_masuk(){
           // Load plugin PHPExcel nya
      include APPPATH.'third_party\PHPExcel\Classes\PHPExcel.php';

      $config['upload_path'] = realpath('excel');
      $config['allowed_types'] = 'xlsx|xls|csv';
      $config['max_size'] = '10000';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload()) {

            //upload gagal
       echo 'Gagal Import Data!';


   } else {

       $data_upload = $this->upload->data();

       $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
                if($numrow > 1){

                  if($row['A'] !== '' && $row['A'] != null){

                    $get_id = $this->db->get_where('produk', array('kode' => $row['A']))->row_array();

                    array_push($data, array(
                        'uid' => $this->session->userdata('userid'),
                        'keluar'        => 0,
                        'retur'         => 0, 
                        'no_nota'       => $row['C'],
                        'id_produk'     => $get_id['id'],
                        'masuk'         => $row['D'], 
                        'harga'         => stringtonumber($row['B']),
                        'keterangan'    => 'stok masuk excel',
                        'status'        => 'in'
                    ));
                  }
                }
                $numrow++;
            }
            $this->db->insert_batch('excel_produk_transaksi', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));


            echo $this->stok_masuk();


        }
    }

    public function stok_masuk(){

      $this->db->select('a.*, b.nama, b.barcode');
      $this->db->join('produk b', 'a.id_produk = b.id', 'LEFT');
      $get = $this->db->get('excel_produk_transaksi a')->result_array();
      $data['stok'] = $get;
      $this->load->view('inventori/table_stok_masuk',$data);
    }


    public function upload()
    {
        // Load plugin PHPExcel nya
      include APPPATH.'third_party\PHPExcel\Classes\PHPExcel.php';

      $config['upload_path'] = realpath('excel');
      $config['allowed_types'] = 'xlsx|xls|csv';
      $config['max_size'] = '10000';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload()) {

            //upload gagal
       $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
       echo "PROSES IMPORT GAGAL!";


   } else {

       $data_upload = $this->upload->data();

       $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
            	if($numrow > 1){

            		$harga_24 = preg_replace('/\D/', '', $row['K']);

                    if($row['N'] == 'PCS'){
                        $satuan = 1;
                    }else if($row['N'] == 'PSG'){
                        $satuan = 2;
                    }else{
                        $satuan = '';
                    }

                    array_push($data, array(
                     'id_user' => $this->session->userdata('userid'),
                     'id_supplier'      => $row['A'],
                     'id_kategori'      => $row['B'],
                     'id_jenis'         => $row['C'], 
                     'id_merek'         => $row['D'],
                     'kode_produk'      => $row['E'],
                     'stok_alert'       => $row['F'],
                     'kode'             => $row['G'],
                     'nama'      	   => $row['H'],
                     'harga_beli'       => stringtonumber($row['I']),
                     'harga_3_satuan'   => stringtonumber($row['J']),
                     'harga'            => stringtonumber($row['K']),
                     'ket'              => $row['L'],
                     'status'		   => 1,
                     'barcode'          => $row['M'],
                     'satuan'           => $satuan
                 ));
                }
                $numrow++;
            }
            $this->db->insert_batch('produk', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            echo "PROSES IMPORT BERHASIL!</b> Data berhasil diimport!";
            

        }
    }

    public function upload_supplier()
    {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party\PHPExcel\Classes\PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            echo "PROSES IMPORT GAGAL!";


        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
                if($numrow > 1){

                    $harga_24 = preg_replace('/\D/', '', $row['K']);
                    array_push($data, array(
                        'id_user' => $this->session->userdata('userid'),
                        'nama'          => $row['A'],
                        'nama_owner'    => $row['B'],
                        'alamat'        => $row['C'], 
                        'kota'          => $row['D'],
                        'kode_pos'      => $row['E'],
                        'tlp'           => $row['F'],
                        'nama_rekening' => $row['G'],
                        'id_bank'       => $row['H'],
                        'no_rekening'   => $row['I'],
                        'status'        => 1,
                    ));
                }
                $numrow++;
            }
            $this->db->insert_batch('supplier', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            echo "PROSES IMPORT BERHASIL!</b> Data berhasil diimport!";
            

        }
    }

    // public function update_sql(){
    // 	$get_where = $this->db->query("SELECT * FROM produk ")->result_array();

    // 	foreach ($get_where as $row) {
    // 		$harga = preg_replace('/\D/', '', $row['harga']);
    // 		$data['harga'] = $harga;
    // 		//echo $harga;
    // 		if($this->db->update('produk', $data, array('id' => $row['id']))){
    // 			echo "berhasil".$row['nama']."<br>";
    // 		}
    // 	}
    // }

    // public function while(){
    // 	$x = 1;

    // 	while($x <= 1000000) {
    // 		echo "The number is: $x <br>";
    // 		$x++;
    // 	}
    // }
}

?>
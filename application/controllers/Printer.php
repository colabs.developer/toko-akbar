<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    if(!isset($_SESSION['userid']))
    {
      redirect(base_url().'masuk','refresh');
    }else{
      $this->load->model('Adminmdl');
    }
  }

  public function penjualan(){
    $where = '';
    if($_SESSION['level'] != 1){

      $where .= "WHERE DATE(a.tgl) = '".date('Y-m-d')."'";

    }

    if($_SESSION['level'] == 1){
      $where .= "WHERE DATE(a.tgl) = '".date('Y-m-d')."'";
    }


    $get_penjualan= $this->db->query("SELECT SUM(a.sub_total - a.diskon) as total FROM penjualan a 
      ".$where."");


    $data['penjualan'] = $get_penjualan->row_array();
    $this->load->view('print/penjualan',$data);
  }


  public function stok_masuk($no_nota='',$tipe=''){
  	$get = $this->db->query('SELECT a.*, b.nama, c.nama_satuan, b.harga as harga_jual , b.harga_3_satuan as harga_konsi FROM produk_transaksi a 
  		LEFT JOIN produk b ON a.id_produk = b.id
  		LEFT JOIN t_unit c ON b.satuan= c.id
     WHERE a.no_nota="'.$no_nota.'" ')->result_array();

    $row = $this->db->query("SELECT a.*, d.nama as nama_supplier, d.alamat FROM produk_transaksi a 
      LEFT JOIN supplier d ON a.id_supplier = d.id
      WHERE no_nota='".$no_nota."' ")->row_array();

    $data['stok_masuk'] = $get;
    $data['detail'] = $row;
    $data['tipe'] = $tipe;
    $this->load->view('print/stok_masuk',$data);
  }

  public function penjualan_kode($tgl_awal,$tgl_akhir){




    $get_kode = $this->db->query("SELECT a.sub_total, a.diskon, d.kode_produk, d.id as id_kode FROM penjualan a 
      LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
      LEFT JOIN produk c ON b.id_produk = c.id 
      LEFT JOIN kode_produk d ON c.kode_produk = d.id
      WHERE DATE(a.tgl) >= '".$tgl_awal."' AND DATE(a.tgl) <= '".$tgl_akhir."' GROUP BY c.kode_produk")->result_array();

    $get = $this->db->query("SELECT c.kode , c.nama , a.sub_total, b.diskon ,  SUM(b.qty) as qty, c.harga_beli, b.harga, c.harga_3_satuan as harga_konsi , c.kode_produk FROM penjualan a 
      LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
      LEFT JOIN produk c ON b.id_produk = c.id 
      LEFT JOIN kode_produk d ON c.kode_produk = d.kode_produk
      WHERE DATE(a.tgl) >= '".$tgl_awal."' AND DATE(a.tgl) <= '".$tgl_akhir."' GROUP BY b.id_produk ")->result_array();
    $data['penjualan_detail'] = $get;
    $data['barcode'] = $get_kode;
    $data['tgl_awal'] = $tgl_awal;
    $data['tgl_akhir'] = $tgl_akhir;
    $this->load->view('print/penjualan_kode',$data);
  }

  public function excel_penjualan_kode($tgl_awal,$tgl_akhir){
   $get_kode = $this->db->query("SELECT d.kode_produk, d.id as id_kode FROM penjualan a 
    LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
    LEFT JOIN produk c ON b.id_produk = c.id 
    LEFT JOIN kode_produk d ON c.kode_produk = d.id
    WHERE DATE(a.tgl) >= '".$tgl_awal."' AND DATE(a.tgl) <= '".$tgl_akhir."' GROUP BY c.kode_produk")->result_array();

   $get = $this->db->query("SELECT c.kode , c.nama , SUM(b.qty) as qty, c.harga_beli, b.harga, c.harga_3_satuan as harga_konsi , c.kode_produk FROM penjualan a 
    LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
    LEFT JOIN produk c ON b.id_produk = c.id 
    LEFT JOIN kode_produk d ON c.kode_produk = d.kode_produk
    WHERE DATE(a.tgl) >= '".$tgl_awal."' AND DATE(a.tgl) <= '".$tgl_akhir."' GROUP BY b.id_produk ")->result_array();
   $data['penjualan_detail'] = $get;
   $data['barcode'] = $get_kode;
   $data['tgl_awal'] = $tgl_awal;
   $data['tgl_akhir'] = $tgl_akhir;
   $this->load->view('print/penjualan_kode_excel',$data);
 }

 public function kartu_stok($bulan,$tahun,$supplier='',$kode_produk=''){
  $where ='';
  if(isset($supplier) && $supplier != 'kosong'){
    $where .= " AND b.id_supplier='".$supplier."' ";
  }

  if(isset($kode_produk) && $kode_produk != 'kosong'){
    $where .= " AND b.kode_produk='".$kode_produk."' ";
  }
  $sql = "SELECT
  b.nama,
  b.id,
  c.nama_kategori,
  a.stok_awal,
  a.stok_masuk,
  a.stok_keluar,
  a.sisa_stok,
  b.harga_3_satuan as harga_konsi,
  a.id_produk
  FROM
  `laporan_stok` a
  LEFT JOIN produk b ON a.id_produk = b.id
  LEFT JOIN kategori c ON b.id_kategori = c.id_kategori
  WHERE bulan = '".$bulan."' AND tahun = '".$tahun."'".$where." ";

  $data['transaksi'] = $this->db->query($sql)->result_array();
  $data['bulan'] = $bulan;
  $data['tahun'] = $tahun;
  $data['id_supplier'] = $supplier;
  $data['kode_produk'] = $kode_produk;
  $this->load->view('print/kartu_stok',$data);
}

public function barcode_produk(){

  $this->load->library('zend');
  $this->zend->load('Zend/Barcode');

  $post = $this->input->post();


  $get_barang = $this->db->query("SELECT a.*,b.nama_kategori FROM produk a LEFT JOIN kategori b ON a.id_kategori = b.id_kategori ");
  $data['barang'] = $get_barang->result_array();
  $data['total'] = $post['total'];
  $data['post'] = $post;

  foreach ($data['barang'] as $row) {
    $code = $row['kode'];
      //generate barcode
    $imageResource = Zend_Barcode::factory('code128', 'image', array('text'=>$code), array())->draw();
    imagepng($imageResource, 'barcodes/'.$code.'.png');
  }

    // $barcode = 'barcodes/'.$code.'.png';
    // echo "<img src='".base_url($barcode)."'>";

  $this->load->view('print/barcode_produk',$data);
}



}

?>
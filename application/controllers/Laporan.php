<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

	public function transaksi_penjualan($id_penjualan='')
	{	

		if($post = $this->input->post()){

			$where = '';
			$status = '';

			if($_SESSION['level'] == 1){
				if($post = $this->input->post()){
					if($post['tgl_awal'] != '' && $post['tgl_akhir'] != ''){



						if($post['id_user'] != ''){
							$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' AND a.uid='".$post['id_user']."' ".$status;

							$data['tgl_awal'] = $post['tgl_awal'];
							$data['tgl_akhir'] = $post['tgl_akhir'];
							$data['id_user'] = $post['id_user'];
						}else{
							$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ".$status;

							$data['tgl_awal'] = $post['tgl_awal'];
							$data['tgl_akhir'] = $post['tgl_akhir'];
						}

					}else{

						$where = " WHERE a.uid='".$post['id_user']."' ".$status;
						$data['id_user'] = $post['id_user'];
					}
				}
			}


			if($_SESSION['level'] == 3){
				$where .= " WHERE a.uid ='".$_SESSION['userid']."' ";
				if($post = $this->input->post()){
					$where .= " AND DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ";

					$data['tgl_awal'] = $post['tgl_awal'];
					$data['tgl_akhir'] = $post['tgl_akhir'];
				}
			}


			$get_penjualan= $this->db->query("SELECT e.nama as nama_customer, f.nama as nama_kasir, a.status_penjualan, a.id,a.tgl,GROUP_CONCAT(DISTINCT CONCAT(d.nama,' x ',b.qty,' <br> ') SEPARATOR '')as produk ,(a.sub_total - a.diskon) as sub_total FROM penjualan a 
				LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan 
				LEFT JOIN produk d ON b.id_produk = d.id 
				LEFT JOIN pelanggan e ON a.id_pelanggan = e.id 
				LEFT JOIN admin f ON a.uid = f.id
				".$where."
				GROUP BY a.id ORDER BY a.id DESC");


			$data['penjualan'] = $get_penjualan->result_array();

		}
		
		$user = $this->db->query("SELECT * FROM admin")->result_array();
		$pelanggan = $this->db->query("SELECT * FROM pelanggan")->result_array();
		
		$data['id_penjualan'] = $id_penjualan;
		$data['user'] = $user;
		$data['pelanggan'] = $pelanggan;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/penjualan',$data);
		$this->load->view('admin/footer');

	}

	public function pos_edit($id_penjualan=''){
		$data['id_penjualan'] = $id_penjualan;
		$this->load->view('pos/pos_edit',$data);
	}

	public function get_total(){
		$post = $this->input->post();
		$get = $this->db->get_where('penjualan',  array('id' => $post['id_penjualan']))->row_array();

		$json['total'] = $get['sub_total'];
		$json['total_text'] = decimals($get['sub_total']);
		echo json_encode($json);
	}

	public function edit_transaksi(){
		if($post = $this->input->post()){
			$get = $this->db->get_where('penjualan', array('id' => $post['id_penjualan']))->row_array();

			$id_penjualan = $post['id_penjualan'];
			$tanggal = date('Y-m-d H:i:s');

			$add['update_at'] = $tanggal;
			$add['edited'] = (int)$get['edited'] + 1;
			$add['id_pembayaran'] = $post['id_pembayaran'];
			$add['ongkir'] = $post['ongkir'];
			$add['cod'] = $post['cod'];
			$add['diskon'] = $post['diskon'];
			$add['tgl_sampai'] = $post['tgl_sampai'];
			$add['jam_sampai'] = $post['jam_sampai'];

			if($this->db->update('penjualan',$add, array('id' => $post['id_penjualan']))){


				if($post['invoice'] == 1){
					
					//$this->cetak($id_penjualan);
					$json['id_penjualan'] = $id_penjualan;
					$json['status'] =1;

					echo json_encode($json);
					
				}else{
					//$this->cetak_indo($id_penjualan);
					$json['id_penjualan'] = $id_penjualan;
					$json['status'] =2;

					echo json_encode($json);
				}
				
			}
			
		}
	}

	public function add_temp(){
		
		$post = $this->input->post();
		$id_penjualan = $post['id_penjualan'];
		$id = $this->db->get_where('penjualan',  array('id' => $id_penjualan));

		if($id->num_rows() > 0){
			$id = $id->row_array();

			$ins['sub_total'] = $id['sub_total'] + ($post['harga_produk'] * $post['qty']);
			$this->db->update('penjualan',$ins, array('id' => $id_penjualan));

		}

		$cek_produk = $this->db->get_where('penjualan_detail', array('id_penjualan' => $id_penjualan, 'id_produk' => $post['id_produk']));

		if($cek_produk->num_rows() > 0){
			$prd = $cek_produk->row_array();

			$total_qty = $prd['qty'] + $post['qty'];
			$add['qty'] = $total_qty;
			$add['harga'] = $post['harga_produk'];
			$add['total'] = $post['harga_produk'] * $total_qty;

			if($this->db->update('penjualan_detail',$add, array('id_penjualan' => $id_temp, 'id_produk' => $post['id_produk']))){

				$upp['no_nota'] = 'TRD'.date('YmdHis');
				$upp['id_produk'] = $post['id_produk'];
				$upp['masuk'] = $post['qty'];
				$upp['harga'] = $post['harga_produk'];
				$upp['keterangan'] = 'update transaksi penjualan marketing';
				$upp['uid'] = $this->session->userdata('userid');
				$upp['waktu'] = date('Y-m-d H:i:s');
				$upp['status'] = 'in';

				if($this->db->insert('produk_transaksi',$upp)){

				}
			}
		}else{

			$brg = $this->db->get_where('produk',array('id' => $post['id_produk']))->row_array();

			$add['id_penjualan'] = $id_penjualan;
			$add['id_produk'] = $post['id_produk'];
			$add['qty'] = $post['qty'];
			$add['harga'] = $post['harga_produk'];
			$add['total'] = $post['harga_produk'] * $post['qty'];
			$add['harga_beli'] = $brg['harga_beli'];
			$add['tgl'] = date('Y-m-d H:i:s');

			if($this->db->insert('penjualan_detail',$add)){

				$upp['no_nota'] = 'TRD'.date('YmdHis');
				$upp['id_produk'] = $post['id_produk'];
				$upp['masuk'] = $post['qty'];
				$upp['harga'] = $post['harga_produk'];
				$upp['keterangan'] = 'update transaksi penjualan marketing';
				$upp['uid'] = $this->session->userdata('userid');
				$upp['waktu'] = date('Y-m-d H:i:s');
				$upp['status'] = 'in';

				if($this->db->insert('produk_transaksi',$upp)){

				}
			}
		}



		$this->load_keranjang($id_penjualan);

	}
	

	public function delete_produk(){
		if($post = $this->input->post()){
			$detail = $this->db->get_where('penjualan_detail', array('id' => $post['id']))->row_array();

			$get = $this->db->query("SELECT SUM(a.total) as total, a.id_penjualan, b.sub_total FROM penjualan_detail a LEFT JOIN penjualan b ON a.id_penjualan = b.id WHERE b.id=".$post['id_penjualan'])->row_array();;


			$add['no_nota'] = 'TRD'.date('YmdHis');
			$add['id_produk'] = $detail['id_produk'];
			$add['masuk'] = $detail['qty'];
			$add['harga'] = $detail['harga'];
			$add['keterangan'] = 'update transaksi penjualan marketing';
			$add['uid'] = $this->session->userdata('userid');
			$add['waktu'] = date('Y-m-d H:i:s');
			$add['status'] = 'in';

			$sub_total = $get['sub_total'] - $detail['total'];

			if($this->db->insert('produk_transaksi',$add)){

				if($this->db->delete('penjualan_detail', array('id' => $post['id']))){



					if($get['sub_total'] != ''){

						$data['sub_total'] = $sub_total;
						if($this->db->update('penjualan',$data, array('id' => $post['id_penjualan']))){

						}
					}else{
						$this->db->delete('penjualan', array('id' => $post['id_penjualan']));
					}


					$this->load_keranjang($detail['id_penjualan']);
				}

			}


		}
	}

	public function load_keranjang($id_penjualan=''){
		$data['id_penjualan'] = $id_penjualan;
		$this->load->view('pos/keranjang_edit',$data);
	}

	public function transaksi_penjualan_harian($id_penjualan='')
	{	


		$where = '';
		$status = '';

		if($_SESSION['level'] == 1){
			if($post = $this->input->post()){
				if($post['tgl_awal'] != '' ){

					if($post['status'] != ''){
						$status = " AND a.status_penjualan=".$post['status']." ";
						$data['status'] = $post['status'];
					}

					if($post['id_user'] != ''){
						$where = " WHERE DATE(a.tgl) = '".$post['tgl_awal']."'  AND a.uid='".$post['id_user']."' ".$status;

						$data['tgl_awal'] = $post['tgl_awal'];
						$data['id_user'] = $post['id_user'];
					}else{
						$where = " WHERE DATE(a.tgl) = '".$post['tgl_awal']."' ".$status;

						$data['tgl_awal'] = $post['tgl_awal'];
					}

				}else{

					if($post['status'] != ''){
						$where = " WHERE a.status_penjualan=".$post['status']." ";
						$status = " AND a.status_penjualan=".$post['status']." ";
						$data['status'] = $post['status'];
					}

					if($post['id_user'] != ''){
						$where = " WHERE a.uid='".$post['id_user']."' ".$status;
						$data['id_user'] = $post['id_user'];
					}

				}
			}
		}




		$get_penjualan= $this->db->query("SELECT e.nama as nama_customer, a.status_penjualan, a.id,a.tgl,GROUP_CONCAT(DISTINCT CONCAT(d.nama,' x ',b.qty,' <br> ') SEPARATOR '')as produk ,a.sub_total FROM penjualan a 
			LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan 
			LEFT JOIN produk d ON b.id_produk = d.id 
			LEFT JOIN pelanggan e ON a.id_pelanggan = e.id 
			".$where."
			GROUP BY a.id ORDER BY a.id DESC");




		$user = $this->db->query("SELECT * FROM admin")->result_array();

		$data['penjualan'] = $get_penjualan->result_array();
		$data['id_penjualan'] = $id_penjualan;
		$data['user'] = $user;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/penjualan_harian',$data);
		$this->load->view('admin/footer');

	}

	public function print($tanggal=''){
		$data['tanggal'] = $tanggal;
		$this->load->view('pos/print_struk_harian',$data);	
	}

	public function update_pelanggan(){
		if($post = $this->input->post()){
			$data['id_pelanggan'] = $post['id_pelanggan'];

			if($this->db->update('penjualan',$data, array('id' => $post['id_penjualan']))){
				//echo $this->db->last_query();
				echo 1;
			}
		}
	}

	public function get_detail_struk(){
		if($post = $this->input->post()){
			$data['id_penjualan'] = $post['id_penjualan'];
			$this->load->view('laporan/load_detail_struk',$data);
		}
	}

	public function invoice($id=''){
		$data['detail'] = $this->db->query("SELECT a.*, b.nama , b.merek FROM penjualan_detail a 
			LEFT JOIN produk b ON a.id_produk = b.id 
			WHERE a.id_penjualan=$id")->result_array();

		$data['pen'] = $this->db->query("SELECT a.*, b.nama , b.kota , b.kode_pos, b.alamat, b.tlp FROM penjualan a 
			LEFT JOIN pelanggan b ON a.id_pelanggan = b.id WHERE a.id=$id ")->row_array();

		$this->load->view('laporan/invoice',$data);
	}

	public function stok(){

		$sql = "SELECT a.id_produk, b.nama , c.nama_kategori, b.kode , SUM(a.masuk) as masuk ,  SUM(a.keluar) as keluar , a.harga FROM produk_transaksi a 
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN kategori c ON b.id_kategori = c.id_kategori
		GROUP BY id_produk";

		$get_stok = $this->db->query($sql)->result_array();

		$data['stok'] = $get_stok;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/stok',$data);
		$this->load->view('admin/footer');

	}

	public function kas(){

		$where = '';
		if($post = $this->input->post()){
			$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ";
		}
		$sql = "SELECT a.id, a.tgl , a.`status`, a.keterangan , a.total FROM kas a  ".$where;

		$kas = $this->db->query($sql)->result_array();

		$data['kas'] = $kas;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/kas',$data);
		$this->load->view('admin/footer');

	}

	public function laba_produk(){
		$sql = "SELECT a.id, a.nama , b.harga as harga_jual , SUM(c.sub_total) as total_penjualan , (SUM(d.masuk) * d.harga) as total_pembelian , SUM(b.profit) as laba FROM produk a 
		LEFT JOIN penjualan_detail b ON a.id = b.id_produk
		LEFT JOIN penjualan c ON c.id = b.id_penjualan
		LEFT JOIN produk_transaksi d ON a.id = d.id_produk
		WHERE d.`status` = 'in'
		GROUP BY b.id_produk";

		$laba = $this->db->query($sql)->result_array();

		$data['laba'] = $laba;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/laba_produk',$data);
		$this->load->view('admin/footer');
	}

}

?>
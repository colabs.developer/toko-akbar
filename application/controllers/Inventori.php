<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventori extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}else{
			$this->load->model('kasirmdl');
		}
	}

	function pagination($search,$item_per_page, $current_page, $total_records, $total_pages, $link=''){
		$pagination = '';

		$mindate = '';
		$maxdate= '';
		$status = '';


		if($mindate ==''){
			$mindate='all';
		}
		if($maxdate ==''){
			$maxdate='all';
		}
		if($status ==''){
			$status='all';
		}




		if($total_pages >= 0 && $current_page <= $total_pages){

			$pagination .= '<ul class="pagination">';


			$right_links    = $current_page + 10;
			$previous       = $current_page - 1;
			$next           = $current_page + 1;
			$first_link     = true;

			if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
      $pagination .= '<li class="first page-item"><a class="page-link" href="'.base_url().$link.'1" title="First">&laquo;</a></li>'; //
      $pagination .= '<li class=" page-item"><a class="page-link" href="'.base_url().$link.'/'.$previous_link.'" title="Previous">&lt;</a></li>';
      for($i = ($current_page-2); $i < $current_page; $i++){
      	if($i > 0){
      		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
      	}
      }
      $first_link = false;
  }

  if($first_link){
  	$pagination .= '<li class="first active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }elseif($current_page == $total_pages){
  	$pagination .= '<li class="last active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }else{
  	$pagination .= '<li class="active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }

  for($i = $current_page+1; $i < $right_links ; $i++){
  	if($i<=$total_pages){
  		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
  	}
  }

  if($current_page < $total_pages){

	      $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$next.'" title="Next">&gt;</a></li>'; //
	      $pagination .= '<li class="last page-item"><a class="page-link" href="'.base_url().$link.'/'.$total_pages.'" title="Last">&raquo;</a></li>';
	  }

	  $pagination .= '</ul>';
	}
	return $pagination;
}

public function index()
{
	$userid = $_SESSION['userid'];
	$data['mercant'] = $this->db->query("SELECT a.*,b.nama as pemilik FROM `data_mercant` a LEFt JOIN data_pelanggan b ON b.id_mercant = a.id_mercant WHERE a.status=1 AND a.sales_id='".$userid."'")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/mercant',$data);
	$this->load->view('admin/footer');
}

public function supplier()
{
	
	$data['supplier'] = $this->db->get('supplier')->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/supplier',$data);
	$this->load->view('admin/footer');
}

public function form_supplier($id='')
{
	
	if($id != '')
	{
		$data['id'] = $id;
		$data['supplier'] = $this->db->get_where('supplier', array('id' => $id))->row_array();
	}
	$data['bank'] = $this->db->get('data_bank')->result_array();

	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_supplier',$data);
	$this->load->view('admin/footer');
}

public function simpan_supplier()
{
	$data = $this->input->post();

	$get = $this->db->get_where('supplier', array('tlp' => $data['tlp']));

	

	if(isset($data['status'])){
		$status = 1;
	}else{
		$status = 0;
	}

	$add['nama'] = $data['nama'];
	$add['nama_owner'] = $data['nama_owner'];
	$add['alamat'] = $data['alamat'];
	$add['kota'] = $data['kota'];
	$add['kode_pos'] = $data['kode_pos'];
	$add['tlp'] = $data['tlp'];
	$add['nama_rekening'] = $data['nama_rekening'];
	$add['no_rekening'] = $data['no_rekening'];
	$add['id_bank'] = $data['id_bank'];
	$add['status'] = $status;
	if(isset($data['id'])){
		if($this->db->update('supplier',$add, array('id' => $data['id'])))
		{
			echo 1;
		}
	}else{

		if($get->num_rows() > 0){
			echo "Nomor Telphone sudah terdaftar";
			exit;
		}

		if($this->db->insert('supplier',$add))
		{
			echo 1;
		}
	}
}

public function stok_masuk()
{
	
	$data['transaksi'] = $this->db->query("SELECT a.*, b.nama as nama_supplier FROM produk_transaksi a LEFT JOIN supplier b ON a.id_supplier = b.id WHERE a.status='in' GROUP BY a.no_nota ORDER BY a.id DESC")->result_array();


	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_masuk',$data);
	$this->load->view('admin/footer');
}

public function opname()
{
	$this->db->group_by('no_nota');
	$data['transaksi'] = $this->db->get_where('produk_transaksi',  array('status' => 'op'))->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_opname',$data);
	$this->load->view('admin/footer');
}

// public function kartu_stok()
// {

// 	$data['produk'] = $this->db->get_where('produk',array('status' => 1))->result_array();
// 	$data['kode'] = $this->db->get('kode_produk')->result_array();
// 	$data['supplier'] = $this->db->get_where('supplier', array('status' => 1))->result_array();
// 	$this->load->view('admin/header',null);
// 	$this->load->view('inventori/kartu_stok',$data);
// 	$this->load->view('admin/footer');
// }

public function kartu_stok()
{

	$data['produk'] = $this->db->get_where('produk',array('status' => 1))->result_array();
	$data['kode'] = $this->db->get('kode_produk')->result_array();
	$data['supplier'] = $this->db->get_where('supplier', array('status' => 1))->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/kartu_stok_fix',$data);
	$this->load->view('admin/footer');
}

public function cari_stok(){
	if($post = $this->input->post()){
		$where ='';
		if(isset($post['id_supplier']) && $post['id_supplier'] != ''){
			$where .= " AND b.id_supplier='".$post['id_supplier']."' ";
		}

		if(isset($post['kode_produk']) && $post['kode_produk'] != ''){
			$where .= " AND b.kode_produk='".$post['kode_produk']."' ";
		}

		$sql = "SELECT
		b.nama,
		b.id,
		c.nama_kategori,
		a.stok_awal,
		a.stok_masuk,
		a.stok_keluar,
		a.sisa_stok,
		b.harga_3_satuan as harga_konsi,
		a.id_produk
		FROM
		`laporan_stok` a
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN kategori c ON b.id_kategori = c.id_kategori
		WHERE bulan = '".$post['bulan']."' AND tahun = '".$post['tahun']."'".$where." ";

		$data['transaksi'] = $this->db->query($sql)->result_array();
		$data['id_supplier'] = $post['id_supplier'];
		$data['kode_produk'] = $post['kode_produk'];
		

		$this->load->view('inventori/view_kartu_stok',$data);

	}


}


public function stok_keluar()
{
	//$this->db->group_by('no_nota');
	$data['transaksi'] = $this->db->query('SELECT * FROM produk_transaksi WHERE status="out" GROUP BY id DESC LIMIT 50')->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_keluar',$data);
	$this->load->view('admin/footer');
}
public function retur_produk()
{
	$data['transaksi'] = $this->db->query('SELECT * FROM produk_transaksi WHERE status="out" GROUP BY id DESC')->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/retur_produk',$data);
	$this->load->view('admin/footer');
}
public function retur_suplier()
{
	// $data['transaksi'] = $this->db->query('SELECT * FROM produk_transaksi WHERE status="out" GROUP BY id DESC')->result_array();

	$data['transaksi'] = $this->db->query("SELECT a.*, b.nama as nama_supplier FROM produk_transaksi a LEFT JOIN supplier b ON a.id_supplier = b.id WHERE a.status='out' GROUP BY a.id DESC")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/retur_suplier',$data);
	$this->load->view('admin/footer');
}



public function act_produk()
{
	$id = $this->input->post('id');

	$get = $this->db->get_where('produk', array('id' => $id))->row_array();
	if($get['status'] == 1)
	{
		$add['status'] = 0;
	}else{
		$add['status'] = 1;
	}

	if($this->db->update('produk',$add, array('id' => $id))){
		echo 1;
	}
}

public function act_supplier()
{
	$id = $this->input->post('id');

	$get = $this->db->get_where('supplier', array('id' => $id))->row_array();
	if($get['status'] == 1)
	{
		$add['status'] = 0;
	}else{
		$add['status'] = 1;
	}

	if($this->db->update('supplier',$add, array('id' => $id))){
		echo 1;
	}
}

public function form_stok_masuk_excel()
{
	$this->db->select('a.*, b.nama, b.barcode');
	$this->db->join('produk b', 'a.id_produk = b.id', 'LEFT');
	$stok = $this->db->get('excel_produk_transaksi a')->result_array();
	$data['stok'] = $stok;


	$get = $this->db->query('SELECT * FROM produk ORDER BY nama ASC')->result_array();
	$data['produks'] = $get;
	$this->load->view('admin/header',null);
	// $this->load->view('inventori/form_stok_masuk1',$data);
	$this->load->view('inventori/form_stok_masuk_excel',$data);
	$this->load->view('admin/footer');
}

public function reset_temp_stok_masuk(){
	if($this->db->truncate('excel_produk_transaksi')){
			echo json_encode(array('status' => true, 'msg' => 'Berhasil reset'));
		}
}

public function form_stok_masuk()
{
	$this->db->select('a.*, b.nama, b.barcode');
	$this->db->join('produk b', 'a.id_produk = b.id', 'LEFT');
	$stok = $this->db->get('excel_produk_transaksi a')->result_array();
	$data['stok'] = $stok;


	$get = $this->db->query('SELECT * FROM produk ORDER BY nama ASC')->result_array();
	$data['produks'] = $get;
	$this->load->view('admin/header',null);
	 $this->load->view('inventori/form_stok_masuk1',$data);
	
	$this->load->view('admin/footer');
}

public function cari($q='')
{
	$query = $this->db->query('SELECT * FROM produk  WHERE nama LIKE "%'.$q.'%" GROUP BY id LIMIT 10');



	$output = '<ul class="list-unstyled ul-custom">';
	if($query->num_rows() > 0){
		foreach ($query->result_array() as $row) {
			$output .= '<li class="li-items" key="'.$row['id'].'">'.$row['nama'].'</li>';  
		}
	} else {
		$output .= '<li>Tidak ada yang cocok.</li>';  
	}  
	$output .= '</ul>';
	echo $output;
}

public function cari_produk_by_id(){
	if($post = $this->input->post()){
		$json = $this->db->query("SELECT * FROM produk WHERE id='".$post['code']."' ")->row_array();

		echo json_encode($json);
	}
}

public function form_opname()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_opname',$data);
	$this->load->view('admin/footer');
}

public function form_stok_keluar()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_stok_keluar',$data);
	$this->load->view('admin/footer');
}
public function form_retur_produk()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_retur_produk',$data);
	$this->load->view('admin/footer');
}
public function form_retur_suplier()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_retur_suplier',$data);
	$this->load->view('admin/footer');
}

public function detail_produk()
{	
	$id = $this->input->post('value');
	$get = $this->db->get_where('produk', array('id' => $id));

	if ($get->num_rows() > 0){ 

		$hasil = $get->row_array();

		echo json_encode($hasil);
		
	}

}

public function cari_produk()
{	
	$id = $this->input->post('value');
	$get = $this->db->get_where('produk', array('kode' => $id));

	if ($get->num_rows() > 0){ 

		$hasil = $get->row_array();

		echo json_encode($hasil);
		
	}

}


public function fetch()
{
	echo $this->kasirmdl->produk_fetch_inven($this->uri->segment(3));
}


public function detail_produk_op(){
	$id = $this->input->post('value');
	$get = $this->db->query('SELECT (SUM(a.masuk) - SUM(a.keluar)) as sisa_stok , c.nama_satuan FROM produk_transaksi a 
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN t_unit c ON b.satuan = c.id WHERE a.id_produk='.$id);

	if ($get->num_rows() > 0) {

		$hasil = $get->row_array();

		echo json_encode($hasil);
	}
}	



public function simpan_stok_masuk()
{
	$data = $this->input->post();
	$jumlah = count($data['id_produk']);
	$waktu = date('Y-m-d h:i:s');

	if($data['status_tr'] == 'OP'){
		$status_nota = 'OP';
		$ket = '';
		$retur = '0';
	}else if($data['status_tr'] == 'SM'){
		$status_nota = 'SM';
		$ket = '';
		$retur = '0';
	}else if($data['status_tr'] == 'SK'){
		$status_nota = 'SK';
		$ket = '';
		$retur = '0';
	}else if($data['status_tr'] == 'RP'){
		$status_nota = 'RP';
		$ket = 'Retur Produk';
		$retur = '1';
	}else if($data['status_tr'] == 'RS'){
		$status_nota = 'RS';
		$ket = 'Retur Supplier';
		$retur = '2';
	}else{
		$status_nota = 'TR';
		$ket = '';
		$retur = '0';
	}
	
	$no_nota = $status_nota.date('Ymdhis');
	// $idsupli = $data['id_supplier'];
	
	if($data['status_tr'] == 'RS'){
		$add['id_supplier'] = $data['id_supplier'][$i];
	}else if($data['status_tr'] == 'SM'){
		$add['id_supplier'] = $data['id_supplier'][$i];
	}
	
	
	for ($i=0; $i < $jumlah; $i++) { 

		if(!empty($data['id_produk'][$i]) && $data['id_produk'][$i] != ''){

			$produk = $this->db->get_where('produk', array('id' => $data['id_produk'][$i]))->row_array(); 

			
			$add['id_produk'] = $data['id_produk'][$i];	
			$add['harga'] = $data['harga'][$i];
			$add['keterangan'] = $ket;
			$add['waktu'] = $waktu;
			$add['uid'] = $_SESSION['userid'];
			$add['status'] = $data['status'][$i];
			$add['no_nota'] = $no_nota;
			$add['retur'] = $retur;
			

			if($data['status'][$i] == 'in'){
				$add['masuk'] = $data['jumlah'][$i];
			}else if($data['status'][$i] == 'out'){
				$add['keluar'] = $data['jumlah'][$i];
			}else{
				$add['masuk'] = $data['jumlah'][$i];
			}
			$this->db->insert('produk_transaksi',$add);

			if($data['status'][$i] == 'in'){
			//cek laporan stok
			$bulan = date("m");
			$tahun = date("Y");
			$cek_laporan = $this->db->get_where('laporan_stok',
				array('id_produk' => $data['id_produk'][$i],
					  'bulan' => $bulan,
					  'tahun' => $tahun
					)
				);
			if($cek_laporan->num_rows() > 0){
				$get_laporan = $cek_laporan->row_array();

				$ustok['stok_masuk'] = $get_laporan['stok_masuk'] + $data['jumlah'][$i];
				$ustok['sisa_stok'] = $get_laporan['stok_awal'] + $ustok['stok_masuk'] - $get_laporan['stok_keluar'];

				$this->db->update('laporan_stok',$ustok, array('id_produk' => $data['id_produk'][$i],
				'bulan' => $bulan,
				'tahun' => $tahun
				));
			}else{
				$ustok['id_produk'] = $data['id_produk'][$i];
				$ustok['bulan'] = $bulan;
				$ustok['tahun'] = $tahun;
				$ustok['stok_awal'] = $produk['real_stok'];
				$ustok['stok_masuk'] = $data['jumlah'][$i];
				$ustok['sisa_stok'] = $produk['real_stok'] + $data['jumlah'][$i] - 0;

				$this->db->insert('laporan_stok',$ustok);
			}
			//end cek laporan
		}

		}
	}


	echo 1;
	
}

public function simpan_stok_masuk_excel(){
	if($post = $this->input->post()){

		$id_supplier = $post['id_supplier'];

		$get = $this->db->get('excel_produk_transaksi')->result_array();
		foreach ($get as $row) {

			$produk = $this->db->get_where('produk', array('id' => $row['id_produk']))->row_array(); 
			
			$data['no_nota'] = $row['no_nota'];
			$data['id_produk'] = $row['id_produk'];
			$data['masuk'] = $row['masuk'];
			$data['keluar'] = 0;
			$data['harga'] = $row['harga'];
			$data['no_nota'] = $row['no_nota'];
			$data['keterangan'] = $row['keterangan'];
			$data['waktu'] = date('Y-m-d H:i:s');
			$data['uid'] = $this->session->userdata('userid');
			$data['status'] = $row['status'];
			$data['retur'] = 0;
			$data['id_supplier'] = $id_supplier;

			$this->db->insert('produk_transaksi',$data);

			//cek laporan stok
			$bulan = date("m");
			$tahun = date("Y");
			$cek_laporan = $this->db->get_where('laporan_stok',
				array('id_produk' => $row['id_produk'],
					  'bulan' => $bulan,
					  'tahun' => $tahun
					)
				);
			if($cek_laporan->num_rows() > 0){
				$get_laporan = $cek_laporan->row_array();

				$ustok['stok_masuk'] = $get_laporan['stok_masuk'] + $row['masuk'];
				$ustok['sisa_stok'] = $get_laporan['stok_awal'] + $ustok['stok_masuk'] - $get_laporan['stok_keluar'];

				$this->db->update('laporan_stok',$ustok, array('id_produk' => $row['id_produk'],
				'bulan' => $bulan,
				'tahun' => $tahun
				));
			}else{
				$ustok['id_produk'] = $row['id_produk'];
				$ustok['bulan'] = $bulan;
				$ustok['tahun'] = $tahun;
				$ustok['stok_awal'] = $produk['real_stok'];
				$ustok['stok_masuk'] = $row['masuk'];
				$ustok['sisa_stok'] = $produk['real_stok'] + $row['masuk'] - 0;

				$this->db->insert('laporan_stok',$ustok);
			}
			//end cek laporan
			

		}

		if($this->db->truncate('excel_produk_transaksi')){
			echo json_encode(array('status' => true, 'msg' => 'Berhasil insert stock'));
		}
	}
}

public function simpan_stok_op(){
	$data = $this->input->post();
	$jumlah = count($data['id_produk']);
	$waktu = date('Y-m-d h:i:s');
	$no_nota = 'OP'.date('Ymdhis');
	for ($i=0; $i < $jumlah; $i++) { 

		if(!empty($data['id_produk'][$i]) && $data['id_produk'][$i] != ''){
			
			$id_produk = $data['id_produk'][$i];

			$get = $this->db->query('SELECT (SUM(masuk) - SUM(keluar)) as sisa_stok , harga FROM produk_transaksi WHERE id_produk="'.$id_produk.'" ')->row_array();

			$jumlah = $data['jumlah'][$i];
			$sisa_stok = $get['sisa_stok'];

			if($sisa_stok > $jumlah){
				$add['masuk'] = $sisa_stok - $jumlah;
				$add['status'] = 'op';
			}else if($jumlah > $sisa_stok){
				$add['masuk'] = $jumlah - $sisa_stok;
				$add['status'] = 'op';
			}

			$add['id_produk'] = $data['id_produk'][$i];	
			$add['harga'] = $get['harga'];
			$add['waktu'] = $waktu;
			$add['uid'] = $_SESSION['userid'];
			$add['no_nota'] = $no_nota;

			
			$this->db->insert('produk_transaksi',$add);
		}
	}


	echo 1;
}

public function form_mercant($id='')
{
	$data['mercant'] = array();
	if($id != '')
	{
		$data['mercant'] = $this->db->get_where('data_mercant',array('id_mercant' => $id))->row_array();
	}

	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_mercant',$data);
	$this->load->view('admin/footer');
}


public function simpan()
{
	$data = $this->input->post();

	$namefile ='';
	if(!empty($_FILES['foto']['name']))
	{
		$namefile = str_replace(' ', '-',$_FILES['foto']['name']);
		$config['upload_path']          = './assets/mercant/';
		$config['allowed_types']        = 'jpg|jpeg|png';
		$config['max_size']             = 2000;
		$confiq['file_name']						= $namefile;
							// $config['max_width']            = 1024;
							// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('foto'))
		{
			echo $this->upload->display_errors();
		}
		else
		{
												//echo 'success';
		}
	}

	$add['nama'] = $data['nama'];
	$add['alamat']= $data['alamat'];
	$add['disc'] = $data['dis'];
	$add['foto'] =$namefile;
	$add['ket'] = $data['ket'];
	$add['status'] = $data['status'];
	$add['input_date'] = date('Y-m-d');
	$add['latitude'] = $data['lat'];
	$add['longtitude'] = $data['long'];
	$add['sales_id'] = $_SESSION['userid'];

	if($this->db->insert('data_mercant',$add))
	{
		$id =  $this->db->insert_id();
		$user_id = $data['user_id'];
		$up['id_mercant'] = $id;
		$this->db->update('data_pelanggan',$up, array('id' => $user_id));

		if(isset($data['promo']))
		{
			$text = 'Dapatkan diskon '.$data['dis'].'% dengan kartu mamber di '.$data['nama'];
			push('New Merchant',$text);
		}
		echo 1;
	}




}

}
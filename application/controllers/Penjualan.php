<?php
header('Access-Control-Allow-Origin: *');  

class Penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}else{
			$this->load->model('kasirmdl');
		}
	}



	public function index(){
		$this->load->view('pos/pos_marketing');
	}

	public function orders($id_penjualan=''){
		$get_penjualan= $this->db->query("SELECT  a.no_resi, a.bukti,a.status,a.id,a.tgl,GROUP_CONCAT(CONCAT(d.nama,' x ',b.qty))as produk ,a.sub_total FROM penjualan a 
			LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan 
			LEFT JOIN produk d ON b.id_produk = d.id 
			WHERE a.uid ='".$_SESSION['userid']."' AND a.status_penjualan=1
			GROUP BY a.id ORDER BY a.tgl DESC");

		$data['penjualan'] = $get_penjualan->result_array();
		$data['id_penjualan'] = $id_penjualan;

		$this->load->view('admin/header',null);
		$this->load->view('marketing/orders',$data);
		$this->load->view('admin/footer');
	}

	public function cari_produk(){
		if($post = $this->input->post()){
			$json = $this->db->query("SELECT * FROM produk WHERE kode='".$post['code']."' ")->row_array();

			echo json_encode($json);
		}
	}

	public function cari_produk_by_id(){
		if($post = $this->input->post()){
			$row = $this->db->query("SELECT * FROM produk WHERE id='".$post['code']."' ")->row_array();

			$json['nama'] = $row['nama'];
			$json['harga_marketing'] = (int)$row['harga_marketing'];
			$json['harga_3_marketing'] = (int)$row['harga_3_marketing'];
			$json['harga_6_marketing'] = (int)$row['harga_6_marketing'];
			$json['harga_12_marketing'] = (int)$row['harga_12_marketing'];
			$json['harga_24_marketing'] = (int)$row['harga_24_marketing'];
			$json['kode']				= $row['kode'];
			echo json_encode($json);
		}
	}

	public function cari_customer_by_id(){
		if($post = $this->input->post()){
			$json = $this->db->query("SELECT * FROM pelanggan WHERE id='".$post['code']."' ")->row_array();

			echo json_encode($json);
		}
	}

	 public function upload_bukti()
        {
        	if(!empty($_FILES['bukti']['name'])){
                $config['upload_path']          = './bukti/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('bukti'))
                {
                        echo $this->upload->display_errors();

                       
                }
                else
                {
                	 $uploadData = $this->upload->data();
                		$data['bukti'] = $uploadData['file_name'];
                		if($this->db->update('penjualan',$data, array('id' => $this->input->post('id_penjualan')))){
                			
                			echo 1;	
                		}
                        
                }
            }
        }

	public function simpan_pelanggan()
	{
		$data = $this->input->post();

		$add['nama'] = $data['nama'];
		$add['tlp'] = $data['tlp'];
		$add['id_user'] = $_SESSION['userid'];
		$add['alamat'] = $data['alamat'];
		$add['email'] = $data['email'];
		$add['status'] =1;
		$add['kota'] = $data['kota'];

		if($this->db->insert('pelanggan',$add))
		{
			echo 1;
		}

	}

	public function fetch()
	{
		echo $this->kasirmdl->produk_fetch($this->uri->segment(3));
	}

	public function fetch_customer()
	{
		echo $this->kasirmdl->customer_fetch($this->uri->segment(3));
	}

	public function load_keranjang(){
		$this->load->view('pos/keranjang_marketing');
	}

	public function get_total(){
		$get = $this->db->get_where('temp_penjualan',  array('id_user' => $this->session->userdata('userid')))->row_array();

		$json['total'] = $get['total'];
		$json['total_text'] = decimals($get['total']);
		echo json_encode($json);
	}

	public function add_temp(){
		
		$post = $this->input->post();
		$id = $this->db->get_where('temp_penjualan',  array('id_user' => $this->session->userdata('userid')));

		if($id->num_rows() > 0){
			$id = $id->row_array();
			$id_temp = $id['id_temp'];

			$ins['total'] = $id['total'] + ($post['harga_produk'] * $post['qty']);
			$this->db->update('temp_penjualan',$ins, array('id_temp' => $id['id_temp']));

		}else{
			$ins['tanggal_order'] = date('Y-m-d H:i:s');
			$ins['id_user'] = $this->session->userdata('userid');
			$ins['total'] = $post['harga_produk'] * $post['qty'];

			$id = $this->db->insert('temp_penjualan',$ins);
			$id_temp = $this->db->insert_id();
		}

		$cek_produk = $this->db->get_where('temp_penjualan_detail', array('id_temp_penjualan' => $id_temp, 'id_produk' => $post['id_produk']));

		if($cek_produk->num_rows() > 0){
			$prd = $cek_produk->row_array();

			$total_qty = $prd['qty'] + $post['qty'];
			$add['qty'] = $total_qty;
			$add['harga'] = $post['harga_produk'];
			$add['total'] = $post['harga_produk'] * $total_qty;

			if($this->db->update('temp_penjualan_detail',$add, array('id_temp_penjualan' => $id_temp, 'id_produk' => $post['id_produk']))){

			}
		}else{
			$add['id_temp_penjualan'] = $id_temp;
			$add['id_produk'] = $post['id_produk'];
			$add['qty'] = $post['qty'];
			$add['harga'] = $post['harga_produk'];
			$add['total'] = $post['harga_produk'] * $post['qty'];

			if($this->db->insert('temp_penjualan_detail',$add)){

			}
		}

		

		$this->load_keranjang();

	}

	public function delete_produk(){
		if($post = $this->input->post()){

			if($this->db->delete('temp_penjualan_detail', array('id' => $post['id']))){

				$get = $this->db->query("SELECT SUM(a.total) as total, a.id_temp_penjualan FROM temp_penjualan_detail a LEFT JOIN temp_penjualan b ON a.id_temp_penjualan = b.id_temp WHERE b.id_user=".$_SESSION['userid'])->row_array();;

				if($get['total'] != ''){
				
					$data['total'] = $get['total'];
					if($this->db->update('temp_penjualan',$data, array('id_temp' => $get['id_temp_penjualan']))){

					}
				}else{
					$this->db->delete('temp_penjualan', array('id_user' => $_SESSION['userid']));
				}

				

				$this->load_keranjang();
			}
		}
	}

	public function reset(){
		$get = $this->db->get_where('temp_penjualan',array('id_user' => $_SESSION['userid']));
		if($get->num_rows() > 0){
			$get = $get->row_array();
			$id_penjualan = $get['id_temp'];
			$this->db->delete('temp_penjualan_detail', array('id_temp_penjualan', $id_penjualan));
			$this->db->delete('temp_penjualan', array('id_user', $_SESSION['userid']));
			echo 1;
		}
		
	}
	
	public function bayar(){
		if($post = $this->input->post()){
			$get = $this->db->get_where('temp_penjualan', array('id_user' => $this->session->userdata('userid')))->row_array();

			$detail_penjualan = $this->db->get_where('temp_penjualan_detail', array('id_temp_penjualan' => $get['id_temp']))->result_array();

			$tanggal = date('Y-m-d H:i:s');
			$no_nota = 'TRM'.date('Ymsdhi');

			$add['no_nota'] = $no_nota;
			$add['uid'] = $get['id_user'];
			$add['sub_total'] = $get['total'];
			$add['tgl'] = $tanggal;
			$add['id_pembayaran'] = $post['id_pembayaran'];
			$add['ongkir'] = $post['ongkir'];
			$add['cod'] = $post['cod'];
			$add['diskon'] = $post['diskon'];
			$add['id_pelanggan'] = $post['id_customer'];
			$add['status'] = 1;
			$add['status_penjualan'] = 1;
			$add['tgl_sampai'] = $post['tgl_sampai'];
			$add['jam_sampai'] = $post['jam_sampai'];

			if($this->db->insert('penjualan',$add)){
				$id_penjualan = $this->db->insert_id();

				foreach($detail_penjualan as $row)
				{

					$produk = $this->db->get_where('produk', array('id' => $row['id_produk']))->row_array(); 

					$detail['id_penjualan'] = $id_penjualan;
					$detail['id_produk'] = $row['id_produk'];
					$detail['qty'] = $row['qty'];
					$detail['harga'] = $row['harga'];
					$detail['total'] = $row['total'];
					$detail['tgl'] = $tanggal;
					$detail['harga_beli'] = $produk['harga_beli'];
					$detail['profit'] = ($row['harga'] - $produk['harga_beli']) * $row['qty'];

					$this->db->insert('penjualan_detail',$detail);

					$tr['id_penjualan'] = $id_penjualan;
					$tr['no_nota'] = $no_nota;
					$tr['id_produk'] = $row['id_produk'];
					$tr['keluar'] = $row['qty'];
					$tr['harga'] = $row['harga'];
					$tr['waktu'] = $tanggal;
					$tr['uid'] = $get['id_user'];
					$tr['status'] = 'out';

					//$this->db->insert('produk_transaksi',$tr);
				}

				$this->db->delete('temp_penjualan', array('id_temp' => $get['id_temp']));
				$this->db->delete('temp_penjualan_detail', array('id_temp_penjualan' => $get['id_temp']));

				if($post['invoice'] == 1){
					
					//$this->cetak($id_penjualan);
					$json['id_penjualan'] = $id_penjualan;
					$json['status'] =1;

					echo json_encode($json);
					
				}else{
					//$this->cetak_indo($id_penjualan);
					$json['id_penjualan'] = $id_penjualan;
					$json['status'] =2;

					echo json_encode($json);
				}
				
			}
			
		}
	}

	public function cetak($id_penjualan) {
        // me-load library escpos
		$this->load->library('escpos');

        // membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
		$connector = new Escpos\PrintConnectors\WindowsPrintConnector("pt210");

        // membuat objek $printer agar dapat di lakukan fungsinya
		$printer = new Escpos\Printer($connector);



        // $printer->initialize();
        // $printer->selectPrintMode(Escpos\Printer::MODE_FONT_A);
        // $printer->text("teks dengan MODE_FONT_A \n");
        // $printer->text("\n");



        // $printer->initialize();
        // $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        // $printer->text("Ini teks rata tengah \n");
        // $printer->text("\n");

		$printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
		//$printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
		$printer->text("Sariraya Halal Mart\n");
		$printer->selectPrintMode();
		$printer->text("87-1 ichikawahara, tanuki cho, nishio shi 444-0301\n");
    	$printer->text("0563-77-6629\n");
		$printer->feed();

        //penjualan detail
		$penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
			LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
			LEFT JOIN produk c ON b.id_produk = c.id 
			WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
		$penjualan = $this->db->query("SELECT a.*,b.nama as kasir FROM penjualan a LEFT JOIN admin b ON a.uid = b.id WHERE a.id=".$id_penjualan)->row_array();
		$harga_subtotal = decimals($penjualan['sub_total']);
		$total_bayar = decimals($penjualan['bayar']);
		$total_kembalian = decimals($penjualan['kembali']);

		$text_total = strlen($harga_subtotal);
		$text_bayar = strlen($total_bayar);
		$text_kembali = strlen($total_kembalian);
		$kasir = strlen($penjualan['kasir']);

		$hasil_total = ((int)32 - (int)10 - (int)$text_total);
		$hasil_bayar = ((int)32 - (int)15 - (int)$text_bayar);
		$hasil_kembali = ((int)32 - (int)12 - (int)$text_kembali); 
		$hasil_kasir = ((int)32 - (int)13 - (int)$kasir);

		$spaci_total = array();
		for ($i=1; $i <= $hasil_total; $i++) { 
			$spaci_total[] =  ' ';

		} 

		$imp1 = implode("|",$spaci_total);
		$spaci1 = str_replace("|", "", $imp1);

		$spaci_total2 = array();
		for ($i=1; $i <= $hasil_bayar; $i++) { 
			$spaci_total2[] =  ' ';

		} 

		$imp2 = implode("|",$spaci_total2);
		$spaci2 = str_replace("|", "", $imp2);


		$spaci_total3 = array();
		for ($i=1; $i <= $hasil_kembali; $i++) { 
			$spaci_total3[] =  ' ';

		} 

		$spaci_total4 = array();
		for ($i=1; $i <= $hasil_kasir; $i++) { 
			$spaci_total4[] =  ' ';

		}

		$imp3 = implode("|",$spaci_total3);
		$spaci3 = str_replace("|", "", $imp3);

		$imp4 = implode("|",$spaci_total4);
		$spaci4 = str_replace("|", "", $imp4);


		$printer->initialize();
		$printer->setJustification(Escpos\Printer::JUSTIFY_LEFT);
		$printer->text('Cashier Staff');
		$printer->text($spaci4);
		$printer->text($penjualan['kasir']);
		$printer->text("\n");
		$printer->text("--------------------------------");


		foreach ($penjualan_detail as $row) {


        $jumlah = $row['qty'].' '; //2 text
        $nama = $row['nama']; //7 text
        $harga = decimals($row['total']); // 7 text

        if(strlen($row['nama']) > 20){
        	$nama = substr($row['nama'], 0,20);
        }

        $total_text =  strlen($jumlah) + strlen($nama) + strlen($harga);
        $total_spaci = ((int)32 - (int)$total_text);

        $spaci_array= array();
        for ($i=1; $i <= $total_spaci; $i++) { 
        	$spaci_array[] =  ' ';

        } 

        $imp = implode("|",$spaci_array);
        $spaci = str_replace("|", "", $imp);


        $printer->text($jumlah);
        $printer->text($nama);
        $printer->text($spaci);
        $printer->text($harga);

    }
    $printer->text("\n");
    $printer->text("--------------------------------");

    $printer->text('Total Bill');
    $printer->text($spaci1);
    $printer->text($harga_subtotal);
    $printer->text("\n");
    $printer->text("--------------------------------");

    $printer->text('Nominal Payment');
    $printer->text($spaci2);
    $printer->text($total_bayar);

    $printer->text('Change Money');
    $printer->text($spaci3);
    $printer->text($total_kembalian);







        /* ---------------------------------------------------------
         * Jarak perbaris 40 (linespace) | setLineSpacing()
         */
        // $printer->initialize();
        // $printer->setLineSpacing(40);
        // $printer->text("Ini paragraf dengan \nline spacing sebesar 40 \ndi printer dotmatrix \n");
        // $printer->text("\n");




        /* ---------------------------------------------------------
         * Menyelesaikan printer
         */
        $printer->feed(4); // mencetak 5 baris kosong, agar kertas terangkat ke atas
        $printer->close();
    }

    public function cetak_indo($id_penjualan) {
        
		$this->load->library('escpos');

        
		$connector = new Escpos\PrintConnectors\WindowsPrintConnector("pt210");

        
		$printer = new Escpos\Printer($connector);


		$printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
		$printer->text("Sariraya Halal Mart\n");
		$printer->selectPrintMode();
		$printer->text("87-1 ichikawahara, tanuki cho, nishio shi 444-0301\n");
    	$printer->text("0563-77-6629\n");
		$printer->feed();

        //penjualan detail
		$penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
			LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
			LEFT JOIN produk c ON b.id_produk = c.id 
			WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
		$penjualan = $this->db->query("SELECT a.*,b.nama as kasir FROM penjualan a LEFT JOIN admin b ON a.uid = b.id WHERE a.id=".$id_penjualan)->row_array();
		$harga_subtotal = decimals($penjualan['sub_total']);
		$total_bayar = decimals($penjualan['bayar']);
		$total_kembalian = decimals($penjualan['kembali']);

		$text_total = strlen($harga_subtotal);
		$text_bayar = strlen($total_bayar);
		$text_kembali = strlen($total_kembalian);
		$kasir = strlen($penjualan['kasir']);

		$hasil_total = ((int)32 - (int)13 - (int)$text_total);
		$hasil_bayar = ((int)32 - (int)11 - (int)$text_bayar);
		$hasil_kembali = ((int)32 - (int)9 - (int)$text_kembali); 
		$hasil_kasir = ((int)32 - (int)10 - (int)$kasir); 


		$spaci_total = array();
		for ($i=1; $i <= $hasil_total; $i++) { 
			$spaci_total[] =  ' ';

		} 

		$imp1 = implode("|",$spaci_total);
		$spaci1 = str_replace("|", "", $imp1);

		$spaci_total2 = array();
		for ($i=1; $i <= $hasil_bayar; $i++) { 
			$spaci_total2[] =  ' ';

		} 

		$imp2 = implode("|",$spaci_total2);
		$spaci2 = str_replace("|", "", $imp2);


		$spaci_total3 = array();
		for ($i=1; $i <= $hasil_kembali; $i++) { 
			$spaci_total3[] =  ' ';

		} 


    	$spaci_total4 = array();
		for ($i=1; $i <= $hasil_kasir; $i++) { 
			$spaci_total4[] =  ' ';

		}

		$imp3 = implode("|",$spaci_total3);
		$spaci3 = str_replace("|", "", $imp3);
		
		$imp4 = implode("|",$spaci_total4);
		$spaci4 = str_replace("|", "", $imp4);


		$printer->initialize();
		$printer->setJustification(Escpos\Printer::JUSTIFY_LEFT);
		$printer->text('Staf Kasir');
		$printer->text($spaci4);
		$printer->text($penjualan['kasir']);
		$printer->text("\n");
		$printer->text("--------------------------------");


		foreach ($penjualan_detail as $row) {


        $jumlah = $row['qty'].' '; //2 text
        $nama = $row['nama']; //7 text
        $harga = decimals($row['total']); // 7 text

        if(strlen($row['nama']) > 20){
        	$nama = substr($row['nama'], 0,20);
        }

        $total_text =  strlen($jumlah) + strlen($nama) + strlen($harga);
        $total_spaci = ((int)32 - (int)$total_text);

        $spaci_array= array();
        for ($i=1; $i <= $total_spaci; $i++) { 
        	$spaci_array[] =  ' ';

        } 

        $imp = implode("|",$spaci_array);
        $spaci = str_replace("|", "", $imp);


        $printer->text($jumlah);
        $printer->text($nama);
        $printer->text($spaci);
        $printer->text($harga);

    }
    $printer->text("\n");
    $printer->text("--------------------------------");

    $printer->text('Total Tagihan');
    $printer->text($spaci1);
    $printer->text($harga_subtotal);
    $printer->text("\n");
    $printer->text("--------------------------------");

    $printer->text('Total Bayar');
    $printer->text($spaci2);
    $printer->text($total_bayar);

    $printer->text('Kembalian');
    $printer->text($spaci3);
    $printer->text($total_kembalian);


        $printer->feed(4); // mencetak 5 baris kosong, agar kertas terangkat ke atas
        $printer->close();
    }

      public function print($id_penjualan){
    	$data['id_penjualan'] = $id_penjualan;
    	$this->load->view('pos/print',$data);	
    }

    public function print_indo($id_penjualan){
    	$data['id_penjualan'] = $id_penjualan;
    	$this->load->view('pos/print_indo',$data);	
    }

}


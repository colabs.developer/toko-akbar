<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}
	public function index()
	{
		

	}

	public function my_profile()
	{
		$userid = $_SESSION['userid'];
		$data['user'] = $this->db->get_where('admin', array('id_user' => $userid))->row_array();
		$this->load->view('admin/header',null);
		$this->load->view('form_profile',$data);
		$this->load->view('admin/footer');
	}


	public function change_pass()
	{
		
		$this->load->view('admin/header',null);
		$this->load->view('change_pass',null);
		$this->load->view('admin/footer');
	}

	public function update_data()
	{
		if($data = $this->input->post()){
			$userid = $_SESSION['userid'];
			$get = $this->db->query("SELECT * FROM admin WHERE email ='".$data['email']."' AND id_user != '".$userid."'");

			if($get->num_rows() > 0)
			{
				echo "Email sudah ada";
			}else{

				$update['nama'] = $data['nama'];
				$update['alamat'] = $data['alamat'];
				$update['kelamin'] = $data['jenis_kelamin'];
				$update['tlp'] = $data['tlp'];
				$update['email'] = $data['email'];

				$this->db->update('admin',$update, array('id_user' => $userid));
				echo 1;
			}
		}else{
			redirect(base_url().'user/my_profile');
		}
	}

	public function ganti_pass()
	{
		if($data = $this->input->post()){
			$userid = $_SESSION['userid'];
			$get = $this->db->query("SELECT * FROM admin WHERE password ='".md5($data['old_pass'])."' AND id_user = '".$userid."'");

			if($get->num_rows() > 0 )
			{
				if($data['pass1'] == $data['pass2'])
				{
					$update['password'] = md5($data['pass1']);
					$this->db->update('admin',$update,array('id_user' => $userid));
					echo 1;	
				}else{
					echo "Password tidak sama";
				}
			}else{
				echo "Password lama salah";
			}
			
			
		}
	}

	public function form_pelanggan($id='')
	{	
		$get_kas = $this->db->query("SELECT * FROM kode_cabang ");
		$data['cabang'] = $get_kas->result_array();

		if($id != '')
		{
			$get = $this->db->query("SELECT * FROM data_pelanggan WHERE id = '".$id."' ");
			$data['user'] = $get->row_array();
			$data['id'] = $id;

		}

		$this->load->view('admin/header',null);
		$this->load->view('form_pelanggan',$data);
		$this->load->view('admin/footer');
	}

	public function pelanggan()
	{	
		$get = $this->db->query("SELECT * FROM data_pelanggan a LEFT JOIN kode_cabang b ON a.id_cabang = b.id_cabang WHERE id_mercant IS NULL");

		$data['pelanggan'] = $get->result_array();

		$this->load->view('admin/header',null);
		$this->load->view('pelanggan',$data);
		$this->load->view('admin/footer');
	}

	public function simpan_user()
	{
		$data = $this->input->post();

		$input['nama'] = $data['nama'];
		$input['kelamin'] = $data['jenis_kelamin'];
		$input['alamat'] = $data['alamat'];
		$input['kota'] = $data['kota'];
		$input['nama'] = $data['nama'];
		$input['propinsi'] = $data['provinsi'];	
		$input['tlp'] = $data['tlp'];
		$input['kode_pos'] = $data['kode_pos'];
		$input['email'] = $data['email'];
		$input['pass'] = $data['password'];
		$input['id_cabang'] = $data['id_cabang'];
		$input['limit_order'] = $data['limit_order'];
		

		if(isset($data['id']))
		{
			$id = $data['id'];
			if($this->db->update('data_pelanggan',$input,array('id' => $id))){
				echo 1;
			}else{
				echo 0;
			}
		}else{
			$input['id_up'] = $_SESSION['userid'];
			if($this->db->insert('data_pelanggan',$input)){
				echo 1;
			}else{
				echo 0;
			}
		}



		
	}
}

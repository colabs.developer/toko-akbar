<?php
header('Access-Control-Allow-Origin: *');  

class Api extends CI_Controller {

  public function __construct()
  {
    parent::__construct();


  }


  public function dologin()
  {
    if(!empty($this->input->post()))
    {
      $data = $this->input->post();
      $pass = md5($data['password']);
      $get = $this->db->get_where('admin',array('email' => $data['email'] , 'pass' => $pass));
      
      if($hasil = $get->row_array())
      {

        $sess['iduser'] = $hasil['id'];
        $sess['nama'] = $hasil['nama'];
        $sess['level'] = $hasil['level'];
        $sess['status'] = 1;
        $sess['msg'] = 'berhasil';
        
        echo json_encode($sess);


      }else{
       $sess['status'] = 0;
       $sess['msg'] = 'Password atau email';

       echo json_encode($sess);
     }

   }else{
     $sess['status'] = 1;
     $sess['msg'] = 'Gagal Konek Server';

     echo json_encode($sess);
   }
 }


 public function total_penjualan(){
  $get = $this->db->query("SELECT SUM(sub_total) as total, COUNT(id) as total_transaksi from penjualan ")->row_array();
  
  $product = $this->db->query("SELECT 
    SUM(case when status=1 then 1 else 0 end) as total_aktive, 
    SUM(case when status=0 then 1 else 0 end) as total_nonaktive, 
    COUNT(id) as total FROM produk ")->row_array();


  $data['total_penjualan'] = decimals($get['total']);
  $data['total_transaksi'] = $get['total_transaksi'];
  $data['total_produk'] = $product['total'];
  $data['produk_aktive'] = $product['total_aktive'];
  $data['produk_nonaktive'] = $product['total_nonaktive'];

  echo json_encode($data);
}


public function penjualan(){
  $tgl = date('Y-m-d');
  $sql = "SELECT * FROM penjualan  WHERE DATE(tgl) = '".$tgl."'";

  $get = $this->db->query($sql)->result_array();

  $data = array();
  foreach ($get as $row) {
    $data[] = array('no_nota' => $row['no_nota'], 
      'tgl' => date('d M Y', strtotime($row['tgl'])), 
      'sub_total' => decimals($row['sub_total']));
  }

  echo json_encode($data);

}

public function penjualan_detail(){
  $penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
    LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
    LEFT JOIN produk c ON b.id_produk = c.id 
    WHERE a.id = ".$id_penjualan)->result_array();

  $penjualan = $this->db->query("SELECT * FROM penjualan WHERE id=".$id_penjualan)->row_array();
  $data = array();
  foreach ($penjualan_detail as $row) {
    $data[] =  array( 'nama' => $row['nama'],
                      'qty'   => $row['qty'],
                      'total' => decimals($row['total'])
                    );
  }

  echo json_encode($data);


}

public function notifikasi($id_user=''){

  $get = $this->db->get_where('notifikasi', array('id_user' => $id_user))->result_array();
  $data = array();
  foreach ($get as $row) {

    $data[] = array('judul' => $row['judul'],
      'id' => $row['id'],
      'isi' => substr($row['isi'], 0, 80),
      'tanggal' => date('d M Y', strtotime($row['tanggal'])),
      'read' => $row['baca']);
  }

  echo json_encode($data);
}

public function notifikasi_detail($id_notif=''){

  $row = $this->db->get_where('notifikasi', array('id' => $id_notif))->row_array();


  $data[] = array('judul' => $row['judul'],
    'isi' => $row['isi'],
    'tanggal' => date('d M Y', strtotime($row['tanggal'])),
    'read' => $row['baca']);
  

  echo json_encode($data);
}



public function produk_terlaris($limit=''){
  if($limit != ''){
    $where = ' LIMIT '.$limit;
  }else{
    $where = '';
  }

  $produk_best = $this->db->query("SELECT b.nama, SUM(a.qty) as total_qty FROM penjualan_detail a 
    LEFT JOIN produk b ON a.id_produk = b.id
    GROUP BY a.id_produk ORDER BY total_qty DESC ".$where)->result_array();

  echo json_encode($produk_best);
}

public function produk_minimum($limit=''){
  if($limit != ''){
    $where = ' LIMIT '.$limit;
  }else{
    $where = '';
  }

  $produk_stok = $this->db->query("SELECT b.nama , (SUM(a.masuk) - SUM(a.keluar)) as sisa_stok , b.stok_alert FROM produk_transaksi a 
    LEFT JOIN produk b ON a.id_produk = b.id
    GROUP BY a.id_produk ".$where)->result_array();

  $data = array();
  foreach ($produk_stok as $row) {
    if($row['sisa_stok'] <= $row['stok_alert']){
      $data[] = array('nama' => $row['nama'], 'sisa_stok' => $row['sisa_stok']);        
    }
  }

  echo json_encode($data);
}

public function laporan_stok($limit=''){
  if($limit != ''){
    $where = ' LIMIT '.$limit;
  }else{
    $where = '';
  }

  $get = $this->db->query("SELECT b.nama, c.nama_kategori, SUM(a.masuk) as masuk , SUM(a.keluar) as keluar FROM `produk_transaksi` a 
    LEFT JOIN produk b ON a.id_produk = b.id
    LEFT JOIN kategori c ON b.id_kategori = c.id_kategori ".$where."
    GROUP BY a.id_produk")->result_array();

  $data = array();

  foreach ($get as $row) {
    $sisa_stok = $row['masuk'] - $row['keluar'];
    $data[] = array('nama' => $row['nama'],
      'nama_kategori' => $row['nama_kategori'],
      'sisa_stok' => $sisa_stok
    );
  }
  echo json_encode($data);
}


public function produk($limit='',$where=''){
  if($limit != ''){
    $where = ' LIMIT '.$limit;
  }else{
    $where = '';
  }

  $get = $this->db->query("SELECT a.*, b.nama_kategori, c.nama_jenis FROM produk a 
    LEFT JOIN kategori b ON a.id_kategori = b.id_kategori
    LEFT JOIN data_jenis c ON a.id_jenis = c.id ".$where)->result_array();

  $data = array();
  foreach ($get as $row) {
    $data[] = array('nama' => $row['nama'],
      'harga_beli' => $row['harga_beli'],
      'harga_jual' => $row['harga'],
      'nama_kategori' => $row['nama_kategori'],
      'nama_jenis'  => $row['nama_jenis'],
      'status'  => $row['status']);

  }

  echo json_encode($data);
}

public function detail_produk($id_produk=''){

  $row = $this->db->query("SELECT a.*,b.nama_kategori , c.nama_jenis FROM produk a 
    LEFT JOIN kategori b ON a.id_kategori = b.id_kategori
    LEFT JOIN data_jenis c ON a.id_jenis = c.id WHERE a.id='".$id_produk."' ")->row_array();

  $data = array('nama' => $row['nama'],
    'harga_beli' => $row['harga_beli'],
    'harga_jual' => $row['harga'],
    'nama_kategori' => $row['nama_kategori'],
    'nama_jenis'  => $row['nama_jenis'],
    'status'  => $row['status']);

  echo json_encode($data);
}




}

<link rel="stylesheet" href="<?php echo base_url().'admin_assets/select2/select2.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'admin_assets/select2/select2-bootstrap.min.css' ?>">
<script src="<?php echo base_url().'admin_assets/select2/select2.full.js'; ?>"></script>

<style type="text/css">
 .tt-menu {
  width: 422px;
  margin: 12px 0;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
  -moz-border-radius: 8px;
  border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  box-shadow: 0 5px 10px rgba(0,0,0,.2);
}
.tt-menu, .gist {
  text-align: left;
}
.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion:hover {
  cursor: pointer;
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;

}

.twitter-typeahead{
  width: 100%;
}
.ul-custom{
  background-color:#eee;
  cursor:pointer;
  position: absolute;
  width: auto;
}
.ul-custom li{
  padding:12px;
  border:thin solid #F0F8FF;
}
.ul-custom li:hover{
  background-color:#7FFFD4;
}


</style>



<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">





            <form id="formstok">
             <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Stok Masuk</h4><br>

               <div class="form-group">
                <label>Supplier</label>
                <select id="id_supplier" name="id_supplier" class="form-control" aria-describedby="validationsupplier">
                  <option value="">Pilih Supplier</option>
                  <?php 
                  
                  $get = $this->db->get_where('supplier', array('status' => 1))->result_array();

                  foreach ($get as $row) {
                    ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
                    <?php
                  }
                  ?>
                </select>
                <div id="validationsupplier" class="invalid-feedback">
                Supplier Harus diisi
              </div>
              </div>

             </div>
             <div class="col-lg-6" style="text-align: right;">
              <input type="hidden" name="status_tr" value="SM">
              <button type="submit" class="btn btn-success btn-fw btn-submit">Simpan</button>
            </div>
          </div>



          <table class="table table-striped">
            <thead>
              <tr>
                <th> Barcode </th>
                <th style="width: 25%;"> Name Produk </th>
                <th style="width: 15%;"> Qty </th>
                <th> Unit </th>
                <th> Price </th>
                <th> Total Price (Rp) </th>
                <th></th>
              </tr>
            </thead>
            <tbody id="disini">
             <tr>
              <td>
                <input type="text" name="barcode[]" class="form-control barcode1" placeholder="Barcode" key="1">
              </td>
              <td style="width: 200px;">          
                <input type="text"  name="nama_produk[]" class="form-control nama_produk" id="nama_produk1" placeholder="Produk" key="1">  
                <div id="list_produk1"></div>
                <input type="hidden" name="id_produk[]" class="id_produk1" onchange="get_harga(1,$(this).val())" key="1"> 
              </td>
              <td>


                <input type="text" class="form-control"  name="jumlah[]" id="jumlah1" onkeyup="hitung(1);">

              </td>
              <td>
                <p id="satuan1"></p>
              </td>
              <td>


                <input type="text" class="form-control"  name="harga[]" id="harga1">

              </td>
              <td>


                <input type="text" class="form-control"  name="total[]" id="total1">
                <input type="hidden" name="status[]" value="in">

              </td>
              <td>

              </td>
            </tr>

          </tbody>
          <tfoot>
            <tr>
              <td colspan="6">
                <a id="tambah_produk" style="color: blue;cursor: pointer;"><i class="mdi mdi-plus"></i> Tambah Barang</a>
              </td>
            </tr>
          </tfoot>
        </table>

      </form>
    </div>
  </div>
</div>

</div>
</div>
</div>


<script type="text/javascript">




  function get_produk(){
    if (event.keyCode === 13) {
      var value = $(this).val();
      if(value != ''){
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'inventori/cari_produk'; ?>",
          data: {value:value},
          dataType: 'json',
          success: function(data) {
            $('#harga'+id).val(data.harga);
            $('#satuan'+id).text(data.satuan);
            $('.nama_produk'+id).val(data.nama);
          }
        });

      }else{
        $('.nama_produk').focus();
      }
    }
  }






  function get_harga(id,value){

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/detail_produk'; ?>",
      data: {value:value},
      dataType: 'json',
      success: function(data) {
        $('#harga'+id).val(data.harga);
        $('#satuan'+id).text(data.satuan);
        $('.barcode'+id).val(data.kode);
        $('#jumlah'+id).focus();

      }
    });


  }

  function hitung(id)
  {
    var jml = $('#jumlah'+id).val();
    var harga = $('#harga'+id).val();
    var hasil = jml * harga;

    $('#total'+id).val(hasil);

  }



  var sample_data = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch:'<?php echo base_url(); ?>inventori/fetch',
    remote:{
      url:'<?php echo base_url(); ?>inventori/fetch/%QUERY',
      wildcard:'%QUERY'
    }
  });


  $('#tambah_produk').click(function(){
    var jml= parseInt($('.nama_produk').last().attr('key'))+1;


    var html = '<tr><td><input type="text" name="barcode" class="form-control barcode'+jml+'" placeholder="Barcode" key="'+jml+'"></td>'+
    '<td style="width: 200px;">'+          
                '<input type="text"  name="nama_produk[]" class="form-control nama_produk" id="nama_produk'+jml+'" placeholder="Produk" key="'+jml+'">'+  
                '<div id="list_produk'+jml+'"></div>'+
                '<input type="hidden" name="id_produk[]" class="id_produk'+jml+'">'+ 
              '</td>'+
    '<td><input type="text" class="form-control"  name="jumlah[]" id="jumlah'+jml+'" onkeyup="hitung('+jml+');"></td>'+
    '<td><p id="satuan'+jml+'"></p></td>'+
    '<td><input type="text" class="form-control"  name="harga[]" id="harga'+jml+'"></td>'+
    '<td><input type="text" class="form-control"  name="total[]" id="total'+jml+'"><input type="hidden" name="status[]" value="in"></td>'+
    '<td></td></tr>';

    $('#disini').append(html);




    $('.barcode'+jml).keyup(function(event){
      if (event.keyCode === 13) {
        var id = $(this).attr('key');
        var value = $(this).val();
        if(id != ''){
          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'inventori/cari_produk'; ?>",
            data: {value:value},
            dataType: 'json',
            success: function(data) {
              $('.id_produk'+jml).val(data.id);
              $('#harga'+jml).val(data.harga);
              $('#satuan'+jml).text(data.satuan);
              $('#jumlah'+jml).focus();
            }
          });

        }else{
          $('.id_produk'+jml).focus();
        }
      }
    });

        $('#nama_produk'+jml).keyup(function(){  
     var query = $(this).val();  
     if(query != '') 
     {  
      $.ajax({  
       url:"<?php echo base_url().'inventori/cari/'; ?>"+query,  
       method:"POST",    
       success:function(data)  
       {  
        $('#list_produk'+jml).fadeIn();  
        $('#list_produk'+jml).html(data);  

        $('.li-items').click(function(){
          var id_produk =  $(this).attr('key');
          var nama_produk = $(this).text();
          $('#nama_produk'+jml).val(nama_produk);
          $('.id_produk'+jml).val(id_produk);
          $('#list_produk'+jml).fadeOut(); 

          get_harga(jml,id_produk);
        });

      }  
    });  
    }  


  }); 


       });




  $('#formstok').submit(function(event){
   event.preventDefault();

   if($('#id_supplier').val() == '')
   {
     $('#id_supplier').addClass('is-invalid');
    return false;
  }


  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'inventori/simpan_stok_masuk'; ?>",
    data: $(this).serialize(),
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      window.location = '<?php echo base_url().'inventori/stok_masuk' ?>';
      $('.btn-submit').prop('disabled', false);
      $('.btn-submit').removeClass('btn-secondary').addClass('btn-success').text('Save');
    }
  });
});

  //   $( ".nama_produk" ).select2({
  //   theme: "bootstrap"
  // });




  $('#formstok').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
      e.preventDefault();
      return false;
    }
  });

  $('.barcode1').keyup(function(event){
    if (event.keyCode === 13) {
      var id = $(this).attr('key');
      var value = $(this).val();
      if(id != ''){
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'inventori/cari_produk'; ?>",
          data: {value:value},
          dataType: 'json',
          success: function(data) {
            $('.id_produk1').val(data.id);
            $('#harga'+id).val(data.harga);
            $('#satuan'+id).text(data.satuan);
            $('#jumlah1').focus();
          }
        });

      }else{
        $('.id_produk').focus();
      }
    }
  });


  $(document).ready(function(){
   $('#nama_produk1').keyup(function(){  
     var query = $(this).val();  
     if(query != '') 
     {  
      $.ajax({  
       url:"<?php echo base_url().'inventori/cari/'; ?>"+query,  
       method:"POST",    
       success:function(data)  
       {  
        $('#list_produk1').fadeIn();  
        $('#list_produk1').html(data);  

        $('.li-items').click(function(){
          var id_produk =  $(this).attr('key');
          var nama_produk = $(this).text();
          $('#nama_produk1').val(nama_produk);
          $('.id_produk1').val(id_produk);
          $('#list_produk1').fadeOut(); 

          get_harga('1',id_produk);
        });

      }  
    });  
    }  


  }); 



 });





</script>
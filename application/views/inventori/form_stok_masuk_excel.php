<link rel="stylesheet" href="<?php echo base_url().'admin_assets/select2/select2.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'admin_assets/select2/select2-bootstrap.min.css' ?>">
<script src="<?php echo base_url().'admin_assets/select2/select2.full.js'; ?>"></script>

<style type="text/css">
 .tt-menu {
  width: 422px;
  margin: 12px 0;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
  -moz-border-radius: 8px;
  border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  box-shadow: 0 5px 10px rgba(0,0,0,.2);
}
.tt-menu, .gist {
  text-align: left;
}
.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion:hover {
  cursor: pointer;
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;

}

.twitter-typeahead{
  width: 100%;
}
.ul-custom{
  background-color:#eee;
  cursor:pointer;
  position: absolute;
  width: auto;
}
.ul-custom li{
  padding:12px;
  border:thin solid #F0F8FF;
}
.ul-custom li:hover{
  background-color:#7FFFD4;
}


</style>



<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">



           <div class="row">
            <div class="col-lg-6">
             <h4 class="card-title">Stok Masuk (EXCEL)</h4><br>

             <div class="form-group">
              <label>Supplier</label>
              <select id="id_supplier" name="id_supplier" class="form-control" aria-describedby="validationsupplier">
                <option value="">Pilih Supplier</option>
                <?php 

                $get = $this->db->get_where('supplier', array('status' => 1))->result_array();

                foreach ($get as $row) {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
                  <?php
                }
                ?>
              </select>
              <div id="validationsupplier" class="invalid-feedback">
                Supplier Harus diisi
              </div>
            </div>

          </div>
          <div class="col-lg-6" style="text-align: right;">
            <input type="hidden" name="status_tr" value="SM">
            <button type="button" class="btn btn-success btn-fw btn-submit">Simpan</button>
            
            <form class="form-excel" style="margin-top: 50px;">
              <div class="form-group">
                <input type="file" name="userfile" class="form-control">
                <div class="invalid-feedback">
                  File excel harus di isi
                </div>
              </div> 

              <p><b>Note : ID PRODUK menggunakan ID produk di APLIKASI</b></p>

              <button type="submit" class="btn btn-primary btn-upload" style="width: 150px;">Upload Excel</button>

              <button type="button" class="btn btn-danger btn-reset" style="width: 150px;">Reset</button>

            </form>

          </div>
        </div>



        <table class="table table-striped">
          <thead>
            <tr>
              <th> Barcode </th>
              <th style="width: 25%;"> Name Produk </th>
              <th style="width: 15%;"> Qty </th>
              <th> Price </th>
              <th> Total Price (Rp) </th>
              <th></th>
            </tr>
          </thead>
          <tbody id="load_table">
            <?php foreach ($stok as $row) { ?>
              <tr>
                <td><?= $row['barcode']; ?></td>
                <td><?= $row['nama']; ?></td>
                <td><?= $row['masuk']; ?></td>
                <td><?= decimals($row['harga']); ?></td>
                <td><?= decimals($row['harga'] * $row['masuk']); ?></td>
              </tr>

            <?php } ?>
          </tbody>
        </table>


      </div>
    </div>
  </div>

</div>
</div>
</div>


<script type="text/javascript">

  $('.form-excel').submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);

    var inputexcel = $('input[name="userfile"]').val();
    if(inputexcel == ''){
      $('input[name="userfile"]').addClass('is-invalid');
      return false;
    }

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'import/upload_stok_masuk'; ?>",
      data:formData,
      dataType: 'html',
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      beforeSend: function() {
        $('.btn-upload').prop('disabled', true);
        $('.btn-upload').removeClass('btn-primary').addClass('btn-secondary').text('Loading');
      },
      success: function(data) {
        swal({
          icon: "success",
          text: "Succes import stok",
        });
        $('#load_table').html(data);
        $('.btn-upload').prop('disabled', false);
        $('.btn-upload').removeClass('btn-secondary').addClass('btn-primary').text('Upload Excel');

      }
    });
  });

  $('.btn-reset').click(function(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'inventori/reset_temp_stok_masuk'; ?>",
    dataType: "json",
    success: function(data) {

      if(data.status == true){
        $('#load_table').html('');
      }

    }
  });
 });

  $('.btn-submit').click(function(event){
   event.preventDefault();
   var id_supplier = $('#id_supplier').val();
   
   if(id_supplier == '')
   {
     $('#id_supplier').addClass('is-invalid');
     return false;
   }




   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'inventori/simpan_stok_masuk_excel'; ?>",
    data: {id_supplier:id_supplier},
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      window.location = '<?php echo base_url().'inventori/stok_masuk' ?>';
      $('.btn-submit').prop('disabled', false);
      $('.btn-submit').removeClass('btn-secondary').addClass('btn-success').text('Save');
    }
  });
 });


</script>
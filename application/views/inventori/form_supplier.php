 
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form class="formbarang">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Supplier</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw">Simpan</button>
             </div>
           </div>

           <div class="row">
            <div class="col-lg-6">

              <?php if(isset($id)){
                ?>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <?php
              } ?>
              <div class="form-group">
                <label for="exampleInputEmail1">Name Suppiler<span class="text-danger" title="Reuired">*</span></label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?php if(isset($id)){ echo $supplier['nama']; } ?>" required>
              </div>

              <div class="form-group">
                <label>Nama Owner <span class="text-danger" title="Reuired">*</span></label>
                <input type="text" class="form-control" name="nama_owner" value="<?php if(isset($id)){ echo $supplier['nama_owner']; } ?>" required>
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Alamat <span class="text-danger" title="Reuired">*</span></label>
                <textarea  class="form-control" id="alamat" name="alamat" required><?php if(isset($id)){ echo $supplier['alamat']; } ?></textarea>
              </div>

              <div class="form-group">
                <label>Kota</label>
                <input type="text" class="form-control" id="kota" name="kota" value="<?php if(isset($id)){ echo $supplier['kota']; } ?>">
              </div>

              <div class="form-group">
                <label>Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="<?php if(isset($id)){ echo $supplier['kode_pos']; } ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">No Telephone <span class="text-danger" title="Reuired">*</span></label>
                <input type="text" class="form-control" id="tlp" name="tlp" placeholder="" value="<?php if(isset($id)){ echo $supplier['tlp']; } ?>" required>
              </div>

              <div class="form-group">
                <label>Status <span class="text-danger" title="Reuired">*</span></label><br>
                <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="Inactive" id="toggle-status">
              </div>

            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label>Nama Rekening</label>
                <input type="text" name="nama_rekening" class="form-control" placeholder="Nama Rekening" value="<?php if(isset($id)){ echo $supplier['nama_rekening']; } ?>">
              </div>

              <div class="form-group">
                <label>Bank</label>
                <select class="form-control" name="id_bank">
                  <option>Pilih Bank</option>
                  <?php foreach ($bank as $row) { ?>
                      <option value="<?= $row['id']; ?>" <?php if(isset($id)){ if($row['id'] == $supplier['id_bank']){ echo "selected"; } } ?>><?= $row['nama_bank']; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>Nomor Rekening</label>
                <input type="text" name="no_rekening" class="form-control" placeholder="Nomor Rekening" value="<?php if(isset($id)){ echo $supplier['no_rekening']; } ?>">
              </div>
            </div>


          </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>




<script type="text/javascript">

  <?php if(isset($id)){ 
    if($supplier['status'] == 1){
      ?>
      $('#toggle-status').bootstrapToggle('on');
      <?php
    }
  } 
  ?>
  

  $(".formbarang").submit(function(event){
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/simpan_supplier'; ?>",
      data: $(this).serialize(),
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'inventori/supplier'; ?>';
        }else{
          swal({
            icon: "warning",
            text: data,
          });
        }
      }
    });

  });
</script>
<style type="text/css">
thead tr th:last-child
{
    text-align: left;
}
tbody tr td:last-child
{
    text-align: center;
}

</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Mercant</h3> </div>
           <!--  <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div> -->
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                  
                   <div class="card">
                    
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="11%;">Pemilik</th>
                                        <th width="11%;">Nama Mercant</th>
                                        <th>Alamat</th>
                                        <th>Ket</th>
                                        <th>Foto</th>
                                        <!-- <th>Edit</th> -->

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no =1; foreach ($mercant as $row) {


                                        ?>
                                        <tr>
                                            <td scope="row"><?php echo $no; ?></td>
                                            <td><?php echo $row['pemilik']; ?></td>
                                            <td><?php echo $row['nama']; ?></td>
                                            <td><?php echo $row['alamat']; ?></td>
                                            <td ><?php echo substr($row['ket'], 0,70); ?></td>
                                            <td ><img src="<?php echo base_url().'assets/mercant/'.$row['foto']; ?>" style="width: 200px;height: auto;"></td>
                                           <!--  <td style="text-align: center;"><a href="<?php echo base_url().'admin/form_pelanggan/'.$row['id_mercant']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td> -->

                                        </tr>
                                        <?php
                                        $no++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

    <script type="text/javascript">
        $('.hapus').click(function(event){
            event.preventDefault();
            var id_barang = $(this).attr('key');


            swal({
              title: "Apakah kamu yakin ?",
              text: "hapus data barang ini",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {

                  $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_barang'; ?>",
                data: {id_barang:id_barang},
                success: function(data){

                    if(data == 1)
                    {
                       swal("Success!","Berhasil hapus barang.", "success")
                       .then((value) => {
                          location.reload();
                      });
                   }

               }
           });


            } else {

            }
        });




        });
    </script>

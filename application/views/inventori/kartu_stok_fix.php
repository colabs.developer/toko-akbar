<style type="text/css">
thead tr th:last-child {
    text-align: center;
}

thead tr th {
    text-align: center;
}

thead tr td {
    text-align: center;
}

tbody tr td:last-child {
    text-align: center;
}

.tt-menu {
    width: 422px;
    margin: 12px 0;
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
}

.tt-menu,
.gist {
    text-align: left;
}

.tt-suggestion {
    padding: 3px 20px;
    line-height: 24px;
}

.tt-suggestion:hover {
    cursor: pointer;
    color: #fff;
    background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf;

}

.twitter-typeahead {
    width: 100%;
}


.card__title {
    padding: 8px;
    font-size: 22px;
    font-weight: 700;
}

.card__title.loading {
    height: 1rem;
    width: 50%;
    border-radius: 3px;
}

.loading {
    position: relative;
    background-color: #e2e2e2;
}

/* The moving element */
.loading::after {
    display: block;
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    transform: translateX(-100%);
    background: -webkit-gradient(linear, left top,
            right top, from(transparent),
            color-stop(rgba(255, 255, 255, 0.2)),
            to(transparent));

    background: linear-gradient(90deg, transparent,
            rgba(255, 255, 255, 0.2), transparent);

    /* Adding animation */
    animation: loading 0.8s infinite;
}

/* Loading Animation */
@keyframes loading {
    100% {
        transform: translateX(100%);
    }
}
</style>
<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<div class="main-panel">
    <div class="content-wrapper">


        <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">

                <div class="card">
                    <div class="card-body">
                        <form method="POST" class="form-cari">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Bulan</label>
                                    <select name="bulan" class="form-control bulan">
                                        <option value="">Pilih Bulan</option>
                                        <?php for ($i=1; $i <= 12; $i++) {  ?>
                                        <option value="<?= $i ?>">Bulan <?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Tahun</label>
                                    <select name="tahun" class="form-control tahun">
                                        <option value="">Pilih Tahun</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Kode Produk</label>
                                    <select class="form-control kode_produk" name="kode_produk">
                                        <option value="">Pilih Kode</option>
                                        <?php foreach ($kode as $row) {
                    ?>
                                        <option value="<?=$row['id']?>"><?=$row['nama_kode']; ?></option>
                                        <?php
                  } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Supplier</label>
                                    <select class="form-control supplier" name="id_supplier">
                                        <option value="">Pilih Supplier</option>
                                        <?php foreach ($supplier as $row) {
                    ?>
                                        <option value="<?=$row['id']?>"><?=$row['nama']; ?></option>
                                        <?php
                  } ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-info btn-sm" style="margin-top: 35px;"><i
                                            class="mdi mdi-search-web"></i> Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title">Kartu Stok</h4>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <button type="button" class="btn btn-secondary print-stok"><i
                                        class="mdi mdi-printer"></i> Kartu Stok</button>
                            </div>
                        </div>


                        <div class="table-responsive" id="load_kartu_stok" style="display: none;">
                            <table class="table">
                                <tr>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                    <td>
                                        <p class="card__title loading"></p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('.datatables').DataTable({
        "lengthChange": false,
        "searching": false
    });

    var sample_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo base_url(); ?>kasir/fetch',
        remote: {
            url: '<?php echo base_url(); ?>inventori/fetch/%QUERY',
            wildcard: '%QUERY',
        }
    });

    $('#prefetch .typeahead').typeahead(null, {
        nama: 'sample_data',
        display: 'nama',
        source: sample_data,
        limit: 50,
        templates: {
            suggestion: Handlebars.compile(
                '<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>'
            )
        }
    });

    $('.typeahead').on('typeahead:selected', function(e, data) {
        if (data.type == 'produk') {
            var code = data.id;
            $('.id_produk').val(code);
        }
    });

});
</script>

<script type="text/javascript">
$('.form-cari').submit(function(event) {
    event.preventDefault();

    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/cari_stok'; ?>",
        data: $(this).serialize(),
        dataType: 'html',
        beforeSend: function() {
            $('#load_kartu_stok').show();
        },
        success: function(data) {
            $('#load_kartu_stok').html(data);
        }
    });
});


$('.print-stok').click(function() {
    var bulan = $('.bulan').val();
    var tahun = $('.tahun').val();
    var kode_produk = $('.kode_produk').val();
    var supplier = $('.supplier').val();

    if (bulan == '') {
        $('.bulan').addClass('is-invalid');
        return false;
    } else {
        $('.bulan').removeClass('is-invalid');
    }

    if (tahun == '') {
        $('.tahun').addClass('is-invalid');
        return false;
    } else {
        $('.tahun').removeClass('is-invalid');
    }

    if (supplier == '' && kode_produk == '') {

        swal({
            icon: "error",
            text: "Pilih Filter Kode Produk atau Supplier",
        });
        return false;
    }

    if (supplier == '') {
        supplier = 'kosong';
    }

    if (kode_produk == '') {
        kode_produk = 'kosong';
    }



    var url = "<?= base_url().'printer/kartu_stok/' ?>" + bulan + '/' + tahun + '/' + supplier + '/' +
        kode_produk;
    window.open(url, "_blank");

});
</script>

<style>
    body,
html,
#map-canvas {
  height: 100%;
  margin: 0;
}
#map-canvas .centerMarker {
  position: absolute;
  /*url of the marker*/
  background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;
  /*center the marker*/
  top: 50%;
  left: 50%;
  z-index: 1;
  /*fix offset when needed*/
  margin-left: -10px;
  margin-top: -34px;
  /*size of the image*/
  height: 34px;
  width: 20px;
  cursor: pointer;
}
</style>

<script>



    var map = null;
var marker;

function showlocation() {
  if ("geolocation" in navigator) {
    /* geolocation is available */
    // One-shot position request.
    navigator.geolocation.getCurrentPosition(callback, error);
  } else {
    /* geolocation IS NOT available */
    console.warn("geolocation IS NOT available");
  }
}

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
};

function callback(position) {
 
  var lat = position.coords.latitude;
  var lon = position.coords.longitude;
  document.getElementById('default_latitude').value = lat;
  document.getElementById('default_longitude').value = lon;
  var latLong = new google.maps.LatLng(lat, lon);
  map.setZoom(16);
  map.setCenter(latLong);
}
//google.maps.event.addDomListener(window, 'load', initMap);



function initMap() {
     var input = document.getElementById('geocoding');
    var autocomplete = new google.maps.places.Autocomplete(input);
     var infowindow = new google.maps.InfoWindow();
    
        
    
  var mapOptions = {
    center: new google.maps.LatLng(-7.9061267,112.610963),
    zoom: 9,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map-canvas"),
    mapOptions);
  google.maps.event.addListener(map, 'center_changed', function() {
    document.getElementById('default_latitude').value = map.getCenter().lat();
    document.getElementById('default_longitude').value = map.getCenter().lng();
  });
  $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
    //do something onclick
    .click(function() {
      var that = $(this);
      if (!that.data('win')) {
        that.data('win', new google.maps.InfoWindow({
          content: 'this is the center'
        }));
        that.data('win').bindTo('position', map, 'center');
      }
      that.data('win').open(map);
    });
    
    
     autocomplete.addListener('place_changed', function() {
          infowindow.close();
          //marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          //marker.setPosition(place.geometry.location);
          //marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

        //   infowindowContent.children['place-icon'].src = place.icon;
        //   infowindowContent.children['place-name'].textContent = place.name;
        //   infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });
        
        
}
</script>
<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pelanggan</h3> </div>

        </div>

        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama" class="form-control input-default " placeholder="Nama" value="<?php if(isset($id)){ echo $mer['nama']; } ?>">

                                                <?php if(isset($id)){ ?>
                                                    <input type="hidden" name="id" value="<?php if(isset($id)){ echo $user['id']; } ?>">
                                                <?php } ?>

                                                <input type="hidden" name="user_id" value="<?php echo $_GET['user_id']; ?>">
                                            </div>

                                            <div class="form-group">
                                              <label>Discount</label>
                                              <input type="text" name="dis" class="form-control input-default " placeholder="Discount" value="<?php if(isset($id)){ echo $mer['dis']; } ?>">

                                            </div>

                                              <div class="form-group">
                                                <label>Alamat</label>
                                                <input type="text" name="alamat" class="form-control input-default " placeholder="Nama" value="<?php if(isset($id)){ echo $mer['alamat']; } ?>">

                                              </div>

                                              <div class="form-group">
                                                <label>Keterangan</label>
                                                <input type="text" name="ket" class="form-control input-default" placeholder="Nama" value="<?php if(isset($id)){ echo $mer['ket']; } ?>">
                                              </div>



                                        </div>

                                        <div class="col-md-6">


                                          <div class="form-group">
                                            <label>Foto</label><br>
                                          <input type="file" name="foto" class="form-control">
                                          </div>

                                          <div class="form-group">
                                            <label>Promo</label><br>
                                          <label><input type="checkbox" name="promo" value="1"> Promotion</label>
                                          </div>

                                          <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                              <option value="1" <?php if(isset($id)){ if($mer['status'] == '1'){ echo "selected"; } } ?>>Aktif</option>
                                              <option value="0" <?php if(isset($id)){ if($mer['status'] == '0'){ echo "selected"; } } ?>>Tidak aktif</option>
                                            </select>
                                          </div>

                                        </div>


                            </div>
                            <div class="col-md-12">
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                                
                                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsV9s5HP8MBlCn6YqGvJnyzKfwKspyqzs&libraries=places&callback=initMap" async defer></script>
                               
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="geocoding" placeholder="Masukkan alamat..">
                                    </div>
                                    
                                </div>
                        
                                
                                <div id="map-canvas" style="height: 300px"></div><br>
                                <div class="row">
                                    <div class="col-md-3">
                                     <input type="text" class="form-control" id="default_latitude" name="lat" placeholder="Latitude" readonly/>
                                </div> 
                                 <div class="col-md-3">
                                     <input type="text" class="form-control" id="default_longitude" name="long" placeholder="Longitude" readonly/>
                                </div> 
                                <!-- <div class="col-md-3">-->
                                <!--    <input type="button" class="btn map-btn" value="Set location" onclick="javascript:showlocation()" />-->
                                <!--</div> -->
                                </div>
                            </div>
                                
                            <div class="col-md-12"><br>
                                <button type="submit"  class="btn btn-info col-md-2">Save</button>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        //var formdata = $(this).serialize();
        var formdata = new FormData(this);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'mercant/simpan'; ?>",
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data){
           
                if(data == 1)
                {
                   swal("Success!","Berhasil menambahkan Mercant.", "success")
                   .then((value) => {
                      window.location = "<?php echo base_url().'mercant'; ?>";
                  });
               }

           }
       });

    });
</script>

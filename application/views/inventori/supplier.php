 <div class="main-panel">
  <div class="content-wrapper">


    <div class="row">

     <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6">
              <p>Note : Import Supplier file excel harus menggunakan format berikut</p>
              <a href="<?php echo base_url().'assets/format/excel_supplier.xlsx'; ?>" class="btn btn-info" download>Download Format Excel</a>
            </div>
            <div class="col-lg-6">
             <form class="form-excel">
              <div class="form-group">
                <label for="exampleInputEmail1">Import Supplier Excel</label>
                <input type="file" name="userfile" class="form-control">
              </div> 

              <button type="submit" class="btn btn-primary btn-upload">Upload Excel</button>

              <a href="<?php echo base_url().'export/supplier_excel' ?>" target="_blank" class="btn btn-info" download>Download Export Excel</a>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>


  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col-lg-6">
           <h4 class="card-title">Supplier</h4>
         </div>
         <div class="col-lg-6" style="text-align: right;">
          <a href="<?php echo base_url().'inventori/form_supplier' ?>" class="btn btn-success btn-fw">Tambah Baru</a>
        </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover datatables">
          <thead>
            <tr>
              <th> No </th>
              <th> Supplier </th>
              <th> Alamat </th>
              <th> Kota </th>
              <th> No Telephone </th>
              <th> Status </th>
              <th> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $no=1;
            foreach ($supplier as $row) {
              if($row['status'] == 1){
                $badge = '<span class="badge badge-primary">Aktif</span>';
                $act = 'Non Aktif';
              }else{
                $badge = '<span class="badge badge-danger">Non Aktif</span>';
                $act = 'Aktif';
              }

              ?>
              <tr>
                <td class="py-1">
                  <?php echo $no; ?>
                </td>
                <td> <?php echo $row['nama']; ?> </td>
                <td> <?php echo $row['alamat']; ?></td>
                <td><?php echo $row['kota']; ?></td>
                <td> <?php echo $row['tlp']; ?> </td>
                <td> <?php echo $badge; ?> </td>
                <td> 
                  <div class="btn-group dropdown">
                    <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Action
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo base_url().'inventori/form_supplier/'.$row['id']; ?>"><i class="fa fa-reply fa-fw"></i>Ubah</a>
                      <a class="dropdown-item hapus" style="cursor: pointer;" key="<?php echo $row['id']; ?>"><i class="fa fa-history fa-fw"></i><?php echo $act; ?></a>

                    </div>
                  </div>
                </td>
              </tr>
              <?php
              $no++;
            }
            ?>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>


<script type="text/javascript">
  $('.hapus').click(function(){
    var id = $(this).attr('key');

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/act_supplier'; ?>",
      data: {id:id},
      success: function(data) {
        if(data == 1)
        {
          location.reload();
        }

      }
    });

  });
</script>

<script type="text/javascript">

  $(document).ready(function(){
    $('.datatables').DataTable();

    $('.form-excel').submit(function(event){
      event.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'import/upload_supplier'; ?>",
        data:formData,
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        beforeSend: function() {
          $('.btn-upload').prop('disabled', true);
          $('.btn-upload').removeClass('btn-primary').addClass('btn-secondary').text('Loading');
        },
        success: function(data) {
          swal({
            icon: "success",
            text: "Succes import supplier",
          });
          $('.btn-upload').prop('disabled', false);
          $('.btn-upload').removeClass('btn-secondary').addClass('btn-primary').text('Upload Excel');

        }
      });
    });

  });

</script>

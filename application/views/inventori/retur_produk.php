<div class="main-panel">
    <div class="content-wrapper">
  
      <div class="row">
  
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
  
              <div class="row">
                <div class="col-lg-6">
                 <h4 class="card-title">Retur Produk</h4>
               </div>
               <div class="col-lg-6" style="text-align: right;">
                <a href="<?php echo base_url().'inventori/form_retur_produk' ?>" class="btn btn-success btn-fw">Tambah Baru</a>
              </div>
            </div>
  
  
            <div class="table-responsive">
              <table class="table table-hover datatables">
                <thead>
                  <tr>
                    <th> No </th>
                    <th> No Nota </th>
                    <th> Tanggal </th>
                    <th> Detail </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no=1;
                  foreach ($transaksi as $row) {
                    if($row['retur']==1){
                    ?>
                    <tr>
                      <td class="py-1">
                        <?php echo $no; ?>
                      </td>
                      <td> <?php echo $row['no_nota']; ?> </td>
                      <td> <?php echo date('d M Y', strtotime($row['waktu'])); ?></td>
                      <td>
                        <a  class="btn btn-primary btn-fw btndetail" style="color: #FFF;" href="<?php echo base_url().'inventori/retur_produk/'.$row['no_nota']; ?>"><i class="fa fa-eye"></i> Detail</a>
                      </td>
                    </tr>
                    <?php
                    }
                    $no++;
                  }
                  ?>
  
  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  
    </div>
  </div>
  </div>
  
  
  <div class="modal" tabindex="-1" role="dialog" id="mymodal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Detail Retur Produk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-striped">
             <thead>
               <tr>
                 <td>Name</td>
                 <td>Total</td>
                 <td>Satuan</td>
                 <td>Harga</td>
                 <td>Total Harga</td>
               </tr>
             </thead>
             <tbody>
               <?php 
               if(!empty($this->uri->segment(3)))
               {
  
                $nota = $this->uri->segment(3);
                $get = $this->db->query('SELECT a.*,b.nama,b.satuan FROM produk_transaksi a LEFT JOIN produk b ON a.id_produk = b.id WHERE a.status="out" AND a.no_nota="'.$nota.'"');
                if($get->num_rows() > 0)
                {
                  $hasil = $get->result_array();
  
                  foreach ($hasil as $row) {
                    ?>
                    <tr>
                      <td><?php echo $row['nama'];  ?></td>
                      <td><?php echo $row['keluar']; ?></td>
                      <td><?php echo $row['satuan'] ?></td>
                      <td><?php echo decimals($row['harga']); ?></td>
                      <td><?php echo decimals($row['harga'] * $row['keluar']); ?></td>
                    </tr>
                    <?php
                  }
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  
  
  <script type="text/javascript">
  
    $(document).ready(function(){
      <?php if(!empty($this->uri->segment(3)))
      { ?>
        $('#mymodal').modal('show');
      <?php } ?>
      $('.datatables').DataTable();
    });
  
  </script>
  
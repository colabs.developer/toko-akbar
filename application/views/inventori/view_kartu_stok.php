<table class="table table-hover datatables ">
    <thead>
        <tr>
            <th width="5%;">#</th>
            <th>Produk</th>
            <th>Awal</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Sisa</th>
            <th>Nilai Awal</th>
            <th>Nilai Masuk</th>
            <th>Nilai Keluar</th>
            <th>Nilai Akhir</th>

        </tr>
    </thead>
    <tbody>
        <?php 
                $no =1;
                $masuk = 0;
                $keluar=0;
                $awal=0;
                $sisatotal=0;
                $totalnilaiawal=0;
                $totalnilaimasuk=0;
                $totalnilaikeluar=0;
                $totalnilaiakhir=0;
                $produk_tampil=array();
                 foreach ($transaksi as $row) {

                  
                  array_push($produk_tampil,$row['id_produk']);

                  

                    $sisa = $row['stok_awal'] + $row['stok_masuk'] - $row['stok_keluar'];
                    $sisatotal += $row['stok_awal'] + $row['stok_masuk'] - $row['stok_keluar'];

                    $nilaiawal = $row['stok_awal'] * $row['harga_konsi'];
                    $totalnilaiawal += $nilaiawal;
          
                    $nilaimasuk = $row['stok_masuk'] * $row['harga_konsi'];
                    $totalnilaimasuk += $nilaimasuk;
          
                    $nilaikeluar = $row['stok_keluar'] * $row['harga_konsi'];
                    $totalnilaikeluar += $nilaikeluar;
          
                    $nilaiakhir = $nilaiawal + $nilaimasuk - $nilaikeluar;
                    $totalnilaiakhir += $nilaiakhir;
          
                  ?>
        <tr>
            <td scope="row"><?php echo $no; ?></td>
            <td style="text-align: center;"><?php echo $row['nama']; ?></td>
            <td style="text-align: right;"><?php echo decimals($row['stok_awal']); ?></td>
            <td style="text-align: right;"><?php echo decimals($row['stok_masuk']); ?></td>
            <td style="text-align: right;"><?php echo decimals($row['stok_keluar']); ?></td>
            <td style="text-align: right;"><?php echo decimals($sisa); ?></td>
            <td class="text-right"><?=decimals($nilaiawal); ?></td>
            <td class="text-right"><?=decimals($nilaimasuk); ?></td>
            <td class="text-right"><?=decimals($nilaikeluar); ?></td>
            <td class="text-right"><?=decimals($nilaiakhir); ?></td>
        </tr>
        <?php
                  $no++;
                } ?>

        <?php 
                $this->db->where('id_supplier',$id_supplier);
                                      if(COUNT($produk_tampil) > 0){
                                        $this->db->where_not_in('id', $produk_tampil);
                                      }                            
                                      $this->db->where('kode_produk',$kode_produk);
                                      $this->db->where('real_stok >', 0);
                                      $get_produk = $this->db->get('produk');
                                      
                                      $get_produk = $get_produk->result_array();
                                    

                if(COUNT($produk_tampil) > 0){
                  
                foreach ($get_produk as $prow){

                    $sisa = $prow['real_stok'] + 0 - 0;
                    $sisatotal += $prow['real_stok'] + 0 - 0;

                    $nilaiawal = $prow['real_stok'] * $prow['harga_3_satuan'];
                    $totalnilaiawal += $nilaiawal;
          
                    $nilaimasuk = 0 * $prow['harga_3_satuan'];
                    $totalnilaimasuk += $nilaimasuk;
          
                    $nilaikeluar = 0 * $prow['harga_3_satuan'];
                    $totalnilaikeluar += $nilaikeluar;
          
                    $nilaiakhir = $nilaiawal + $nilaimasuk - $nilaikeluar;
                    $totalnilaiakhir += $nilaiakhir;

                    ?>

        <tr>
            <td scope="row"><?php echo $no; ?></td>
            <td style="text-align: center;"><?php echo $prow['nama']; ?></td>
            <td style="text-align: right;"><?php echo decimals($prow['real_stok']); ?></td>
            <td style="text-align: right;"><?php echo 0; ?></td>
            <td style="text-align: right;"><?php echo 0; ?></td>
            <td style="text-align: right;"><?php echo decimals($prow['real_stok']);; ?></td>
            <td class="text-right"><?=decimals($nilaiawal); ?></td>
            <td class="text-right"><?= 0; ?></td>
            <td class="text-right"><?= 0; ?></td>
            <td class="text-right"><?= decimals($nilaiakhir); ?></td>
        </tr>

        <?php              
                  $no++;
                }
              }    
              ?>
    </tbody>
    <tfoot>
        <tr style="background-color: #f2f8f9;">
            <td colspan="2" style="text-align: right;"><strong>Total</strong></td>
            <td style="text-align: right;"><strong><?= $awal; ?></strong></td>
            <td style="text-align: right;"><strong><?= $masuk; ?></strong></td>
            <td style="text-align: right;"><strong><?= $keluar; ?></strong></td>
            <td style="text-align: right;"><strong><?= $sisatotal; ?></strong></td>
            <td style="text-align: right;"><strong><?= decimals($totalnilaiawal); ?></strong></td>
            <td style="text-align: right;"><strong><?= decimals($totalnilaimasuk); ?></strong></td>
            <td style="text-align: right;"><strong><?= decimals($totalnilaikeluar); ?></strong></td>
            <td style="text-align: right;"><strong><?= decimals($totalnilaiakhir); ?></strong></td>
        </tr>
    </tfoot>
</table>
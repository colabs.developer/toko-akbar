<!DOCTYPE html>
<?php
$tgl = date('d_m_Y');
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=List_Produk_$tgl.xls");
?>
<html>
<head>
	<title>List Products <?php echo date('d M Y'); ?></title>
</head>
<body>
<table>
	<tr>
		<th>Products</th>
		<th>Category</th>
		<th>Brand</th>
		<th>Price(1)</th>
		<th>Price(3)</th>
		<th>Price(6)</th>
		<th>Price(12)</th>
		<th>Price(24)</th>
		<th>Stock</th>
	</tr>
	<?php foreach ($products as $row) {
	?>

	<tr>
		<td><?php echo $row['nama']; ?></td>
		<td><?php echo $row['nama_kategori']; ?></td>
		<td><?php echo $row['merek']; ?></td>
		<td><?php echo decimals((int)$row['harga_marketing']); ?></td>
		<td><?php echo decimals((int)$row['harga_3_marketing']); ?></td>
		<td><?php echo decimals((int)$row['harga_6_marketing']); ?></td>
		<td><?php echo decimals((int)$row['harga_12_marketing']); ?></td>
		<td><?php echo decimals((int)$row['harga_24_marketing']); ?></td>
		<td><?php echo decimals((int)$row['stok']); ?></td>
	</tr>

	<?php
	} ?>
</table>
</body>
</html>
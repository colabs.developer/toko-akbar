<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data_Penjualann.xls");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Stok Masuk</title>
</head>
<body>

			<table>
				<tr>
					<td>Tanggal Transaksi</td>
					<td>:</td>
					<td><?= $tgl_awal.' - '.$tgl_akhir; ?></td>
				</tr>
				<tr>
					<td>Tanggal Cetak</td>
					<td>:</td>
					<td><?= date('Y-m-d H:i:s'); ?></td>
				</tr>
			</table>

			<table>
				<thead>
				<tr>
					<th>Barcode</th>
					<th>Nama Barang</th>
					<th>Jml</th>
					<th>Beli</th>
					<th>Konsinyasi</th>
					<th>Jual</th>
				</tr>
				</thead>
				<tbody>
					<?php 
					$totalqty=0;
					$totalbeli=0;
					$totalkonsi=0;
					$totaljual=0;
					foreach($barcode as $code) { 
					?>

					<tr>
						<td colspan="6"><strong>Barcode <?=$code['kode_produk']; ?></strong></td>
					</tr>
					<?php foreach ($penjualan_detail as $row) { 

						if($code['id_kode'] == $row['kode_produk']){
						$totalqty += $row['qty'];

						$beli = $row['harga_beli'] * $row['qty'];
						$totalbeli += $beli;

						$konsi = $row['harga_konsi'] * $row['qty'];
						$totalkonsi += $konsi;

						$jual = $row['harga'] * $row['qty'];
						$totaljual += $jual;
					?>

					<tr>
						<td><?=$row['kode']; ?></td>
						<td><?=$row['nama']; ?></td>
						<td><?=$row['qty']; ?></td>
						<td class="text-right"><?=decimals($beli); ?></td>
						<td class="text-right"><?=decimals($konsi); ?></td>
						<td class="text-right"><?=decimals($jual); ?></td>
					</tr>

					<?php
					} 
				}
					?>
					<tr>
						<td colspan="2">Total</td>
						<td ><strong><?= decimals($totalqty); ?></strong></td>
						<td class="text-right"><strong><?= decimals($totalbeli); ?></strong></td>
						<td class="text-right"><strong><?= decimals($totalkonsi); ?></strong></td>
						<td class="text-right"><strong><?= decimals($totaljual); ?></strong></td>
					</tr>
					<?php
					} ?>
				</tbody>
			</table>	
</body>
</html>
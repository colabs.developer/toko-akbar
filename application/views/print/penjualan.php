<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>receipt</title>
    <!-- <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@400&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Nanum+Gothic+Coding&family=Roboto+Mono:wght@500&display=swap"
        rel="stylesheet">
    <style>
    @page {
        margin: 0
    }

    body {
        margin: 0
    }

    .sheet {
        margin: 0;
        overflow: hidden;
        position: relative;
        box-sizing: border-box;
        page-break-after: always;
    }

    /** Paper sizes **/
    body.A3 .sheet {
        width: 297mm;
        height: 419mm
    }

    body.A3.landscape .sheet {
        width: 420mm;
        height: 296mm
    }

    body.A4 .sheet {
        width: 210mm;
        height: 296mm
    }

    body.A4.landscape .sheet {
        width: 297mm;
        height: 209mm
    }

    body.A5 .sheet {
        width: 148mm;
        height: 209mm
    }

    body.A5.landscape .sheet {
        width: 210mm;
        height: 147mm
    }

    body.letter .sheet {
        width: 216mm;
        height: 279mm
    }

    body.letter.landscape .sheet {
        width: 280mm;
        height: 215mm
    }

    body.legal .sheet {
        width: 216mm;
        height: 356mm
    }

    body.legal.landscape .sheet {
        width: 357mm;
        height: 215mm
    }

    /** Padding area **/
    .sheet.padding-10mm {
        padding: 5mm;
        padding-left: 0mm;
        margin-bottom: 55mm;
    }

    /** For screen preview **/
    @media screen {
        body {
            background: #e0e0e0
        }

        .sheet {
            background: white;
            box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
            margin: 5mm auto;
        }
    }

    /** Fix for Chrome issue #273306 **/
    @media print {
        body.A3.landscape {
            width: 420mm
        }

        body.A3,
        body.A4.landscape {
            width: 297mm
        }

        body.A4,
        body.A5.landscape {
            width: 210mm
        }

        body.A5 {
            width: 148mm
        }

        body.letter,
        body.legal {
            width: 216mm
        }

        body.letter.landscape {
            width: 280mm
        }

        body.legal.landscape {
            width: 357mm
        }
    }

    /*@font-face {
  font-family: 'Merchant';
  src: url('<?= base_url().'assets/fonts/Merchant.ttf'; ?>') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
    }

    */ * {
        font-family: 'Arial';
        font-size: 12px;
        font-weight: normal;
        word-spacing: 0px;
        letter-spacing: -1px;
        line-height: 1.2em;
    }

    body.receipt .sheet {
        width: 58mm;
        height: auto;
    }

    /* change height as you like */
    @media print {
        body.receipt {
            width: 58mm
        }

        .page-break {
            display: block;
            page-break-before: always;
        }

        * {
            font-family: 'Arial';
            font-size: 12px;
            word-spacing: 0px;
            word-break: break-all;
            letter-spacing: 1px;
            line-height: 1.2em;
        }
    }

    /* this line is needed for fixing Chrome's bug */
    </style>
</head>
<?php 
$id_user = $this->session->userdata('userid');
      $kasird = $this->db->query("SELECT nama as kasir FROM admin  WHERE id=".$id_user)->row_array();
 
      $kasir = strlen($kasird['kasir']);


       if(strlen($kasird['kasir']) >= 13){
          $nama_kasir = substr($kasird['kasir'], 0,13);
        }else{
          $nama_kasir = $kasird['kasir'];
        }

       
    

  
 ?>

<body class="receipt" onload="window.print()">
    <section class="sheet padding-10mm">
        <center><span style="font-weight: bold;font-size: 15px;">Akbar</span> <br> Jl. Pasar Besar No.11, Sukoharjo,
            Kec. Klojen, 65118 <br>0811-3666-470<br><br></center>
        <div style="width: 100%;">
            <div style="width: 100%;">
                <div style="width: 40%;float: left;">Kasir</div>
                <div style="width: 60%;float: right;text-align: right;"><?php echo $nama_kasir; ?></div>
            </div>
            <div style="width: 100%;">
                <div style="width: 30%;float: left;">Tanggal</div>
                <div style="width: 70%;float: right;text-align: right;">
                    <?php echo date('Y-m-d H:i:s'); ?></div>
            </div>
            <div style="width: 100%;">
                <div style="width: 30%;float: left;">Total</div>
                <div style="width: 70%;float: right;text-align: right;">
                    <?php echo format_decimals($penjualan['total']); ?>
                </div>
            </div>
            <div style="width: 50%;float: right;text-align: center;margin-top:20px;margin-bottom:30px;">

                Operator
            </div>
            <div style="width: 50%;float: right;text-align: center;margin-top:20px;margin-bottom:30px;">

                Mengetahui
            </div>


    </section>
</body>

</html>
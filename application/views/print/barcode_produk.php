<!DOCTYPE html>
<html>
<head>
	<title>Barcode produk</title>
</head>

<style type="text/css">
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FAFAFA;
		font: 12px "Tahoma";
	}
	* {
		box-sizing: border-box;
		-moz-box-sizing: border-box;
	}
	tr td {
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 5px;
		padding-right: 5px;
	}

	tr th {
		padding-top:5px;
		padding-bottom: 5px;
	}
	.page {
		width: 90mm;
		/* padding: 5mm; */
		margin: 1mm auto;
		border: 1px #D3D3D3 solid;
		border-radius: 5px;
		background: white;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	}
	.subpage {
		padding: 1cm;
		border: 5px red solid;
		height: 257mm;
		outline: 2cm #FFEAEA solid;
	}
	.page .fh5co-project {
		float: left;
		margin-bottom: -15px;
		width: 100px;
		height: 130px;
		-webkit-transform: rotate(90deg);
		-moz-transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-o-transform: rotate(90deg);
		transform: rotate(90deg);
		/* border: 2px solid; */
	}
	.page .fh5co-project > a {
	text-align: center;
	text-decoration:none;
	}
	.page .fh5co-project > a img {
	width:100px;
	margin-bottom: -10px;
	font-size:10px;
	text-decoration:none;
	}
	.page .fh5co-project > a h2 {
	text-decoration:none;
	margin-bottom: 0px;
	font-size: 10px;
	color: #999999;
	-webkit-transition: 0.3s;
	-o-transition: 0.3s;
	transition: 0.3s;
	}
	.page .fh5co-project > a:hover h2, .page .fh5co-project > a:active h2, .page .fh5co-project > a:focus h2 {
	color: #000;
	}


	.text-right{
		text-align: right;
	}

	@media screen {
        div.footer {
            display: none;
        }
    }
    @media print {
        div.footer {
            position: fixed;
            right: 0;
            bottom: 0;
        }
    }

	@page {
		margin: 0;
	}
	@media print {
		html, body {
			width: 90;
			height: 40mm;        
		}
		.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
			padding-bottom: 30px;
		}
		 #foot {
    display: block;
    position: fixed;
    bottom: 0pt;
  }
	}

	.p9{
		font-size: 9pt;
	}

	.py8 tr td{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<body>
	<div class="book">
		<div class="page">
			
			<?php
			for ($i=1; $i <= $total; $i++) { 
				
			foreach ($barang as $key) :
				foreach ($post['id_produk'] as $row) :

					if($row == $key['id']){
			?>
				<div class="fh5co-project">
					<a href="">
						<img src="<?php echo base_url("barcodes/"); ?><?php echo $key['kode']; ?>.png">
						<h2><?php echo $key['nama']; ?><br><?php echo $key['harga']; ?></h2>
					</a>
				</div>
				<?php 
					}
			endforeach; ?>
			<?php endforeach; 
			
		}
		?>

		</div>
			
	</div>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
    <title>Stok Masuk</title>
</head>

<style type="text/css">
body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    background-color: #FAFAFA;
    font: 12px "Tahoma";
}

* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}

tr td {
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 5px;
    padding-right: 5px;
}

tr th {
    padding-top: 5px;
    padding-bottom: 5px;
}

.page {
    width: 210mm;
    min-height: 297mm;
    padding: 20mm;
    margin: 10mm auto;
    border: 1px #D3D3D3 solid;
    border-radius: 5px;
    background: white;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

.subpage {
    padding: 1cm;
    border: 5px red solid;
    height: 257mm;
    outline: 2cm #FFEAEA solid;
}

.text-right {
    text-align: right;
}

@media screen {
    div.footer {
        display: none;
    }
}

@media print {
    div.footer {
        position: fixed;
        right: 0;
        bottom: 0;
    }
}

@page {
    size: A4;
    margin: 0;
}

@media print {

    html,
    body {
        width: 210mm;
        height: 297mm;
    }

    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
        padding-bottom: 30px;
    }

    #foot {
        display: block;
        position: fixed;
        bottom: 0pt;
    }
}

.p9 {
    font-size: 9pt;
}

.py8 tr td {
    padding-top: 5px;
    padding-bottom: 5px;
}
</style>

<body>
    <div class="book">
        <div class="page">
            <h1>Laporan Penjualan</h1>

            <table>
                <tr>
                    <td>Tanggal Transaksi</td>
                    <td>:</td>
                    <td><?= $tgl_awal.' - '.$tgl_akhir; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Cetak</td>
                    <td>:</td>
                    <td><?= date('Y-m-d H:i:s'); ?></td>
                </tr>
            </table>

            <table style="border-collapse: collapse;width: 100%;" border="1">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Nama Barang</th>
                        <th>Jml</th>
                        <th>Beli</th>
                        <th>Konsinyasi</th>
                        <th>Diskon</th>
                        <th>Jual</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
					$totalqty=0;
					$totalbeli=0;
					$totalkonsi=0;
					$totaljual=0;
					$totaldiskon=0;
					foreach($barcode as $code) { 
					?>

                    <tr>
                        <td colspan="6"><strong>Barcode <?=$code['kode_produk']; ?></strong></td>
                    </tr>
                    <?php foreach ($penjualan_detail as $row) { 

						if($code['id_kode'] == $row['kode_produk']){
						$totalqty += $row['qty'];

						$beli = $row['harga_beli'] * $row['qty'];
						$totalbeli += $beli;

						$konsi = $row['harga_konsi'] * $row['qty'];
						$totalkonsi += $konsi;

						$jual = ($row['harga'] * $row['qty']) - $row['diskon'];
						$totaljual += $jual;

						$diskon = $row['diskon'];
						$totaldiskon += $row['diskon'];
					?>

                    <tr>
                        <td><?=$row['kode']; ?></td>
                        <td><?=$row['nama']; ?></td>
                        <td><?=$row['qty']; ?></td>
                        <td class="text-right"><?=decimals($beli); ?></td>
                        <td class="text-right"><?=decimals($konsi); ?></td>
                        <td class="text-right"><?=decimals($diskon); ?></td>
                        <td class="text-right"><?=decimals($jual); ?></td>
                    </tr>

                    <?php
					} 
				}
					?>
                    <tr>
                        <td colspan="2">Total</td>
                        <td><strong><?= decimals($totalqty); ?></strong></td>
                        <td class="text-right"><strong><?= decimals($totalbeli); ?></strong></td>
                        <td class="text-right"><strong><?= decimals($totalkonsi); ?></strong></td>
                        <td class="text-right"><strong><?= decimals($totaldiskon); ?></strong></td>
                        <td class="text-right"><strong><?= decimals($totaljual); ?></strong></td>
                    </tr>
                    <?php
					} ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
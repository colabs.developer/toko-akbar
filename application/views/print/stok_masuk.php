<!DOCTYPE html>
<html>
<head>
	<title>Stok Masuk</title>
</head>

<style type="text/css">
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FAFAFA;
		font: 12px "Tahoma";
	}
	* {
		box-sizing: border-box;
		-moz-box-sizing: border-box;
	}
	tr td {
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 5px;
		padding-right: 5px;
	}

	tr th {
		padding-top: 10px;
		padding-bottom: 10px;
	}
	.page {
		width: 210mm;
		min-height: 297mm;
		padding: 20mm;
		margin: 10mm auto;
		border: 1px #D3D3D3 solid;
		border-radius: 5px;
		background: white;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	}
	.subpage {
		padding: 1cm;
		border: 5px red solid;
		height: 257mm;
		outline: 2cm #FFEAEA solid;
	}

	@media screen {
        div.footer {
            display: none;
        }
    }
    @media print {
        div.footer {
            position: fixed;
            right: 0;
            bottom: 0;
        }
    }

	@page {
		size: A4;
		margin: 0;
	}
	@media print {
		html, body {
			width: 210mm;
			height: 297mm;        
		}
		.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
			padding-bottom: 30px;
		}
		 #foot {
    display: block;
    position: fixed;
    bottom: 0pt;
  }
	}

	.p9{
		font-size: 9pt;
	}

	.py8 tr td{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<body>
	<div class="book">
		<div class="page">
			<h1>Laporan Barang Masuk</h1>

			<table>
				<tr>
					<td>No Nota</td>
					<td>:</td>
					<td><?= $detail['no_nota']; ?></td>
					<td>Supplier</td>
					<td>:</td>
					<td><?= $detail['nama_supplier']; ?></td>
				</tr>
				<tr>
					<td>Tanggal Transaksi</td>
					<td>:</td>
					<td><?= $detail['waktu']; ?></td>
					<td>Alamat</td>
					<td>:</td>
					<td><?= $detail['alamat']; ?></td>
				</tr>
				<tr>
					<td>Tanggal Cetak</td>
					<td>:</td>
					<td><?= date('Y-m-d H:i:s'); ?></td>
					<td></td>
					<td></td>
				</tr>
			</table>

			<table style="border-collapse: collapse;width: 100%;" border="1">
				<thead>
				<tr>
					<th>Nama Barang</th>
					<th>Jumlah</th>
					<th>Satuan</th>
					<th>Harga Satuan</th>
					<th>Sub Total</th>
				</tr>
				</thead>
				<tbody>
					<?php
					$total=0;
					$totalmasuk=0;
					 foreach ($stok_masuk as $row) { 

						if($tipe == 'terima'){
							$harga = $row['harga_konsi'];
						}else if($tipe == 'kirim'){
							$harga = $row['harga_jual'];
						}
							 $total += $harga * $row['masuk'];
							 
							 $totalmasuk += $row['masuk'];
					 	?>
					<tr>
						<td><?= $row['nama']; ?></td>
						<td style="text-align: center;"><?= $row['masuk']; ?></td>
						<td style="text-align: center;"><?= $row['nama_satuan']; ?></td>
						<td style="text-align: right;"><?= decimals($harga); ?></td>
						<td style="text-align: right;"><?= decimals($harga * $row['masuk']); ?></td>
					</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td>Total</td>
						<td style="text-align: center;"><?= $totalmasuk; ?></td>
						<td colspan="3" style="text-align: right;"><strong><?= format_decimals($total); ?></strong></td>
					</tr>
				</tfoot>
			</table>
			
			<br><br><br><br><br><br><br>
			<div style="width: 50%;float: right;text-align: center;">
				
				Operator
			</div>
			<div style="width: 50%;float: right;text-align: center;">
				
				Mengetahui
			</div>
		</div>
	</div>
</body>
</html>
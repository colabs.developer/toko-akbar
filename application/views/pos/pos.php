<title>POS AKBAR</title>
<link rel="shortcut icon" href="<?php echo base_url().'favicon.ico'; ?>" />
<link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">
<link rel="stylesheet"
    href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
<script src="<?= base_url().'assets/kasir/jquery-1.12.4.js'; ?>"></script>
<!-- 
 <link rel="stylesheet" href="https://twitter.github.io/typeahead.js/css/examples.css" /> -->
<style type="text/css">
.tt-menu {
    width: 422px;
    margin: 12px 0;
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
}

.tt-menu,
.gist {
    text-align: left;
}

.tt-suggestion {
    padding: 3px 20px;
    line-height: 24px;
}

.tt-suggestion:hover {
    cursor: pointer;
    color: #fff;
    background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf;

}

.twitter-typeahead {
    width: 100%;
}

.form-control {
    border-color: #000;
}

.tabel-stok td,
.tabel-stok th {
    padding: 7px;
}
</style>



<script src="<?= base_url().'assets/kasir/handlebars.js'; ?>"></script>
<script src="<?= base_url().'assets/kasir/typeahead.bundle.js'; ?>"></script>

<script src="<?= base_url().'assets/kasir/sweetalert.min.js'; ?>"></script>

<nav class="navbar navbar-expand-lg navbar-light bg-light"
    style="background: linear-gradient(120deg, #00e4d0, #5983e8);">
    <a class="navbar-brand" href="<?php echo base_url(); ?>" style="color:#FFF;margin: 0 auto;"><i
            class="mdi mdi-arrow-left" aria-hidden="true"></i> Kembali</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto" style="margin: 0 auto;">
            <li class="nav-item">
                <a class="nav-link" href="#" style="color: #FFF;font-size: 20px;">AKBAR POS</a>
            </li>

        </ul>
        <!-- <span class="navbar-text">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">Lihat
                Stok</button>
        </span> -->


    </div>
</nav>

<div>
    <div>

        <div class="row">

            <div class="col-lg-9 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">





                        <form id="formstok">



                            <table class="table">
                                <tr>
                                    <th>Barcode</th>
                                    <th style="width: 25%;">Produk</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th style="width: 16%;">Diskon</th>
                                    <th></th>
                                </tr>
                                <tr style="background-color: #f2f8f9;">
                                    <td>
                                        <input type="text" name="cari" class="textcari form-control"
                                            placeholder="Barcode" style="height: 45px;font-size: 16px;">
                                    </td>
                                    <td>

                                        <div id="prefetch">
                                            <input type="text" id="search_box" name="nama_produk"
                                                class="form-control nama_produk typeahead" placeholder="Produk"
                                                autocomplete="false" aria-describedby="ariaproduk"
                                                style="height: 45px;font-size: 16px;">
                                            <div class="invalid-feedback" id="ariaproduk">
                                                Produk harus di isi.
                                            </div>
                                        </div>
                                        <input type="hidden" name="id_produk" class="id_produk">
                                        <input type="hidden" name="id_customer" class="id_customer">
                                    </td>
                                    <td>
                                        <p class="hargaproduk1"></p>
                                        <input type="hidden" name="harga" class="harga">
                                    </td>
                                    <td>

                                        <input type="number" class="form-control qty" placeholder="Jumlah"
                                            aria-describedby="ariaqty" style="height: 45px;font-size: 16px;">
                                        <div class="invalid-feedback" id="ariaqty">
                                            Jumlah harus di isi.
                                        </div>

                                    </td>
                                    <td>
                                        <input type="number" class="form-control diskon" placeholder="Diskon %" min="0"
                                            style="height: 45px;font-size: 16px;">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-add"
                                            style="height: 45px;font-size: 16px;">Tambah</button>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th tyle="width: 200px;"> Nama Produk </th>
                                        <th style="text-align: center;"> Total </th>
                                        <th style="text-align: right;"> Harga </th>
                                        <th style="text-align: right;">Diskon</th>
                                        <th style="text-align: right;"> Total harga </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="disini">
                                    <?php 
               

                foreach ($temp as $row) {

                 ?>
                                    <tr>
                                        <td style="width: 200px;">
                                            <?php echo $row['nama']; ?>
                                        </td>
                                        <td style="text-align: center;">
                                            <?php echo $row['qty']; ?>x
                                        </td>
                                        <td style="text-align: right;">

                                            <?php echo decimals($row['harga']); ?>

                                        </td>
                                        <td style="text-align: right;">

                                            <?php echo decimals($row['diskon']); ?>

                                        </td>
                                        <td style="text-align: right;">
                                            <p id="total1" style="margin: 0px;">
                                                <?php echo decimals($row['total']); ?></p>
                                            <input type="hidden" name="status[]" value="in">

                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm btn-delete"
                                                key="<?php echo $row['id']; ?>"><span
                                                    class="mdi mdi-delete"></span></button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3" style="padding-top:10px;">
                <div class="card">
                    <div class="card-body" style="padding:10px 20px;">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="caribarcode" placeholder="Barcode">
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="carinama" placeholder="Nama Produk">
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary btn-block btncaristok"><span class="mdi mdi-magnify"></span></button>
                            </div>
                        </div>
                        <div class="col-md-100" style="max-height:340px;overflow:auto;">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered tabel-stok">
                                    <thead>
                                        <tr>
                                            <th>Barcode</th>
                                            <th>Nama Produk</th>
                                            <th>Stok</th>
                                        </tr>
                                    </thead>
                                    <tbody id="load_stok">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body" style="padding:10px 20px;">

                        <div style="width: 100%;box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.02);border-radius: 8px;
      border: 1px solid rgba(0,0,0,.125);padding: 15px;margin-bottom: 15px;">
                            <div class="row" style="margin-bottom: 10px;">
                                <a href="<?= base_url('printer/penjualan'); ?>" target="_blank"
                                    class="btn btn-primary btn-block"
                                    style="margin-left: 15px;margin-right: 15px;margin-bottom: 15px;">Print
                                    Penjualan</a>
                                <div class="col-md-5">
                                    <p style="margin: 0px;font-weight: 600;">Pelanggan</p>
                                </div>
                                <div class="col-md-7" style="text-align: right;">
                                    <button type="button" class="btn btn-success btn-add-new" data-toggle="modal"
                                        data-target="#customerMdl">Tambah Baru</button>
                                </div>
                            </div>

                            <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                data-target="#exampleModal">Pilih Pelanggan</button>

                            <div style="display: none;" class="frame-customer">
                                <table class="table" style="font-size: 12px;">
                                    <tr>
                                        <td>Nama Produk</td>
                                        <td class="customer_name" style="text-align: right;"></td>
                                    </tr>
                                    <tr>
                                        <td>No Telephone</td>
                                        <td class="customer_phone" style="text-align: right;"></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="customer_email" style="text-align: right;"></td>
                                    </tr>
                                </table>
                            </div>


                        </div>


                        <?php 
    $detail = $this->db->get_where('temp_penjualan', array('id_user' => $this->session->userdata('userid')))->row_array();
    ?>

                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <p>Sub Total</p>
                                </td>
                                <td style="text-align: right;">
                                    <p style="font-size: 22px;" class="subtotal">
                                        <?php echo decimals(@$detail['total']); ?></p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p>Grand Total</p>
                                </td>
                                <td style="text-align: right;">
                                    <p style="font-size: 22px;" class="total" key="<?php echo @$detail['total']; ?>">
                                        <?php echo decimals(@$detail['total']); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <select class="form-control pembayaran"
                                        style="width: 100%;text-align: right;color: #000;font-size: 16px;margin-bottom: 15px;">
                                        <?php $get_bayar = $this->db->get_where('pembayaran', array('status' => 1))->result_array();
            foreach ($get_bayar as $row) {
              ?>
                                        <option value="<?php echo $row['id_pembayaran']; ?>">
                                            <?php echo $row['nama_pembayaran']; ?></option>
                                        <?php
            }
            ?>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="text" name="bayar" class="form-control bayar"
                                        style="width: 100%;text-align: right;color: #000;font-size: 16px;"
                                        placeholder="Total Pembayaran">
                                    <div class="invalid-feedback">
                                        Nominal harus di isi
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Kembalian</p>
                                </td>
                                <td style="text-align: right;">
                                    <p style="font-size: 22px;" class="kembalian">0</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><button type="button" class="btn btn-success btn-block btn-bayar"
                                        style="height: 45px;">BAYAR</button></td>
                            </tr>
                            <tr>
                                <td colspan="2"><button type="button" class="btn btn-secondary btn-block btn-reset"
                                        style="height: 45px;margin-top: 10px;">RESET</button></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pelanggan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="customer">
                    <input type="text" class="form-control typeahead" placeholder="Customer Name">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="customerMdl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-customer">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pelanggan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama <span class="text-danger" title="Reuired">*</span></label>
                        <input type="text" class="form-control form-customer" name="nama" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email <span class="text-danger" title="Reuired">*</span></label>
                        <input type="text" class="form-control form-customer" name="email" required>
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">No Telephone <span class="text-danger"
                                title="Reuired">*</span></label>
                        <input type="number" class="form-control form-customer" name="tlp" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Kota <span class="text-danger" title="Reuired">*</span></label>
                        <input type="text" class="form-control form-customer" name="kota" required>
                    </div>

                    <div class="form-group">
                        <label>Alamat <span class="text-danger" title="Reuired">*</span></label>
                        <textarea class="form-control form-customer" name="alamat" id="alamat" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success">Simpan Pelanggan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
function decimals(angka, prefix) {
    var number_string = angka.toString().replace(/[^,\d]/g, ''),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

    if (angka < 0) {
        return '-' + rupiah;
    } else {
        return rupiah;
    }

}




function get_total() {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'kasir/get_total'; ?>",
        dataType: 'json',
        success: function(data) {



            var grand_total = parseInt(data.total);

            $('.subtotal').text(data.total_text);
            $('.total').text(decimals(grand_total));
            $('.total').attr('key', data.total);
        }
    });

    $('.btn-delete').click(function() {
        var id = $(this).attr('key');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/delete_produk'; ?>",
            data: {
                id: id
            },
            dataType: 'html',
            success: function(data) {
                $('#disini').html(data);

                get_total();
            }
        });
    });

}


$(document).ready(function() {



    $('.textcari').focus();

    $('.btn-delete').click(function() {
        var id = $(this).attr('key');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/delete_produk'; ?>",
            data: {
                id: id
            },
            dataType: 'html',
            success: function(data) {
                $('#disini').html(data);

                get_total();
            }
        });
    });

    $('.btncaristok').click(function() {
        var kode = $('#caribarcode').val();
        var nama_produk = $('#carinama').val();

        $.ajax({
            type: "GET",
            data: {
                kode: kode,
                nama_produk: nama_produk
            },
            url: "<?php echo base_url().'kasir/cari_stok'; ?>",
            dataType: 'html',
            success: function(data) {
                $('#load_stok').html(data);
            }
        });

    });




    var sample_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo base_url(); ?>kasir/fetch',
        remote: {
            url: '<?php echo base_url(); ?>kasir/fetch/%QUERY',
            wildcard: '%QUERY',
        }
    });

    $('#prefetch .typeahead').typeahead(null, {
        nama: 'sample_data',
        display: 'nama',
        source: sample_data,
        limit: 50,
        templates: {
            suggestion: Handlebars.compile(
                '<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>'
            )
        }
    });


    var customer_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo base_url(); ?>kasir/fetch_customer',
        remote: {
            url: '<?php echo base_url(); ?>kasir/fetch_customer/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#customer .typeahead').typeahead(null, {
        nama: 'customer_data',
        display: 'nama',
        source: customer_data,
        limit: 50,
        templates: {
            suggestion: Handlebars.compile(
                '<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>'
            )
        }
    });

    $('#customer').on('typeahead:selected', function(e, data) {

        if (data.type == 'customer') {
            var code = data.id;
            $('.id_customer').val(code);

            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'kasir/cari_customer_by_id'; ?>",
                data: {
                    code: code
                },
                dataType: 'json',
                success: function(data) {
                    $('.customer_name').text(data.nama);
                    $('.customer_phone').text(data.tlp);
                    $('.customer_email').text(data.email);
                    $('.frame-customer').show();
                    $('#exampleModal').modal('hide');
                }
            });
        }

    });


    $(".form-customer").submit(function(event) {
        event.preventDefault();


        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/simpan_pelanggan'; ?>",
            data: $(this).serialize(),
            success: function(data) {

                if (data == 1) {
                    swal({
                        icon: "success",
                        text: "Success add data customer",
                    });

                    $('#customerMdl').modal('hide');
                    $('.form-customer').val('');
                }
            }
        });

    });

    $('.typeahead').on('typeahead:selected', function(e, data) {
        if (data.type == 'produk') {
            var code = data.id;
            $('.id_produk').val(code);

            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'kasir/cari_produk_by_id'; ?>",
                data: {
                    code: code
                },
                dataType: 'json',
                success: function(data) {

                    $('.textcari').val(data.kode);
                    $('.hargaproduk1').text(data.harga);

                    $('.hargaproduk1').attr('key', data.harga);
                    $('.harga').val(data.harga);
                    $('.qty').focus();


                }
            });
        }
    });


    $('.btn-add').click(function() {
        var id_produk = $('.id_produk').val();
        var harga_produk = $('.harga').val();
        var qty = $('.qty').val();
        var diskon = $('.diskon').val();

        if (id_produk == '') {
            $('#search_box').addClass('is-invalid');
            $('#ariaproduk').show();
            return false;
        } else {
            $('#search_box').removeClass('is-invalid');
            $('#ariaproduk').hide();
        }

        if (qty == '') {
            $('.qty').addClass('is-invalid');
            $('#ariaqty').show();
            return false;
        } else {
            $('.qty').removeClass('is-invalid');
            $('#ariaqty').hide();
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/add_temp'; ?>",
            data: {
                id_produk: id_produk,
                qty: qty,
                harga_produk: harga_produk,
                diskon: diskon
            },
            dataType: 'html',
            success: function(data) {
                $('#disini').html(data);

                get_total();

                $('.diskon').val('');
                $('.textcari').val('');
                $('.nama_produk').val('');
                $('.qty').val('');
                $('.hargaproduk1').hide();
                $('.textcari').focus();

            }
        });

    });

});



$('.textcari').keyup(function(event) {
    if (event.keyCode === 13) {
        var code = $(this).val();
        if (code != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'kasir/cari_produk'; ?>",
                data: {
                    code: code
                },
                dataType: 'json',
                success: function(data) {
                    if (data.habis) {

                        swal({
                            text: data.habis,
                            icon: "warning",
                        })
                    } else {
                        $('.nama_produk').val(data.nama);
                        $('.id_produk').val(data.id);
                        $('.hargaproduk1').text(data.harga);

                        $('.hargaproduk1').attr('key', data.harga);
                        $('.harga').val(data.harga);
                        $('.qty').focus();
                    }
                }
            });

        } else {
            $('.nama_produk').focus();
        }
    }
});


$(".nama_produk").keyup(function(event) {
    if (event.keyCode === 13) {
        $('.qty').focus();
    }
});

$(".qty").keyup(function(event) {
    if (event.keyCode === 13) {
        $('.diskon').focus();
    }
});

$(".diskon").keyup(function(event) {
    var diskon = $('.diskon').val();



    if (event.keyCode === 13) {

        var id_produk = $('.id_produk').val();
        var harga_produk = $('.harga').val();
        var diskon = $('.diskon').val();
        var qty = $('.qty').val();

        if (id_produk == '') {
            $('#search_box').addClass('is-invalid');
            $('#ariaproduk').show();
            return false;
        } else {
            $('#search_box').removeClass('is-invalid');
            $('#ariaproduk').hide();
        }

        if (qty == '') {
            $('.qty').addClass('is-invalid');
            $('#ariaqty').show();
            return false;
        } else {
            $('.qty').removeClass('is-invalid');
            $('#ariaqty').hide();
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/add_temp'; ?>",
            data: {
                id_produk: id_produk,
                qty: qty,
                harga_produk: harga_produk,
                diskon: diskon
            },
            dataType: 'html',
            async: true,
            success: function(data) {
                $('#disini').html(data);

                get_total();

                $('.diskon').val('');
                $('.textcari').val('');
                $('.nama_produk').val('');
                $('.qty').val('');
                $('.hargaproduk1').text('');
                $('.textcari').focus();

            }
        });
    }
});



function get_harga(id, value) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/detail_produk'; ?>",
        data: {
            value: value
        },
        dataType: 'json',
        success: function(data) {
            $('#harga' + id).val(data.harga);
            $('#satuan' + id).text(data.satuan);
        }
    });


}

function hitung(id) {
    var jml = $('#jumlah' + id).val();
    var harga = $('#harga' + id).val();
    var hasil = jml * harga;

    $('#total' + id).val(hasil);

}




$(".bayar").on("keyup", function(event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();

    }

    var total_int = parseInt($('.total').attr('key'));



    var total = parseInt($('.total').attr('key'));
    var hasil = parseInt($(this).val()) - parseInt(total);

    $('.kembalian').text(decimals(hasil));

});

$('.bayar').keyup(function(event) {
    if (event.keyCode === 13) {
        if ($('.bayar').val() == '') {
            $('.bayar').addClass('is-invalid');
            return false;
        } else {
            $('.bayar').removeClass('is-invalid');
        }


        $('.swal-button--confirm').focus();
        var totalgrand = $('.total').text();
        swal({
                title: "Rp " + totalgrand,
                text: "apakah yakin ingin menyelesaikan transaksi ?",
                icon: "warning",

                buttons: {
                    cancel: true,
                    confirm: "Done",
                },
            })
            .then((willDelete) => {
                if (willDelete) {
                    var bayar = $(this).val();
                    var id_pembayaran = $('.pembayaran').val();
                    var id_customer = $('.id_customer').val();
                    var invoice = $('.invoice-type').val();
                    var total_int = parseInt($('.total').attr('key'));



                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url().'kasir/bayar'; ?>",
                        data: {
                            bayar: bayar,
                            id_pembayaran: id_pembayaran,
                            id_customer: id_customer,
                            invoice: invoice
                        },
                        dataType: 'json',
                        async: false,
                        success: function(data) {

                            if (data.status == 1) {
                                window.open('<?php echo base_url().'kasir/print/' ?>' + data
                                    .id_penjualan, '_blank',
                                    'location=yes,height=570,width=520,scrollbars=yes,status=yes'
                                );
                                location.reload();
                            } else {
                                window.open('<?php echo base_url().'kasir/print_indo/' ?>' +
                                    data.id_penjualan, '_blank',
                                    'location=yes,height=570,width=520,scrollbars=yes,status=yes'
                                );
                                location.reload();
                            }



                        }
                    });

                }
            });
    }
});

$('.btn-reset').click(function() {
    swal({
            title: "Apakah anda yakin?",
            text: "akan reset transaksi",
            icon: "error",

            buttons: {
                cancel: true,
                confirm: "Done",
            },
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'kasir/reset'; ?>",
                    async: true,
                    success: function(data) {
                        if (data == 1) {
                            location.reload();
                        }

                    }
                });
            }
        });
});

$('.btn-bayar').click(function() {

    if ($('.bayar').val() == '') {
        $('.bayar').addClass('is-invalid');
        return false;
    } else {
        $('.bayar').removeClass('is-invalid');
    }

    $('.swal-button--confirm').focus();
    var totalgrand = $('.total').text();
    swal({
            title: "Rp " + totalgrand,
            text: "apakah yakin ingin menyelesaikan transaksi ?",
            icon: "warning",

            buttons: {
                cancel: true,
                confirm: "Done",
            },
        })
        .then((willDelete) => {
            if (willDelete) {
                var bayar = $('.bayar').val();
                var id_pembayaran = $('.pembayaran').val();
                var id_customer = $('.id_customer').val();
                var invoice = $('.invoice-type').val();



                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'kasir/bayar'; ?>",
                    data: {
                        bayar: bayar,
                        id_pembayaran: id_pembayaran,
                        id_customer: id_customer,
                        invoice: invoice
                    },
                    dataType: 'json',
                    async: false,
                    success: function(data) {
                        if (data.status == 1) {
                            window.open('<?php echo base_url().'kasir/print/' ?>' + data
                                .id_penjualan, '_blank',
                                'location=yes,height=570,width=520,scrollbars=yes,status=yes'
                            );
                            location.reload();
                        } else {
                            window.open('<?php echo base_url().'kasir/print_indo/' ?>' + data
                                .id_penjualan, '_blank',
                                'location=yes,height=570,width=520,scrollbars=yes,status=yes'
                            );
                            location.reload();
                        }



                    }
                });

            }
        });
});


$('#formstok').submit(function(event) {
    event.preventDefault();

    if ($('#cabang').val() == '') {
        alert('cabang tidak boleh kosong');
        return false;
    }


    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/simpan_stok_masuk'; ?>",
        data: $(this).serialize(),
        success: function(data) {
            window.location = '<?php echo base_url().'inventori/stok_masuk' ?>';
        }
    });
});
</script>
<script src="<?= base_url().'assets/kasir/popper.min.js'; ?>"></script>
<script src="<?= base_url().'assets/kasir/bootstrap.min.js'; ?>"></script>
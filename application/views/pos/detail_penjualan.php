<div class="row">
	<div class="col-md-12">
		
	
<table class="table table-bordered">
	<thead>
	<tr>
		<th>Barang</th>
		<th>Qty</th>
		<th>Harga</th>
		<th>Sub Total</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($detail as $row) { ?>
	
	<tr>
		<td><?= $row['nama']; ?></td>
		<td><?= $row['qty']; ?></td>
		<td style="text-align: right;"><?= format_decimals($row['harga']); ?></td>
		<td style="text-align: right;"><?= format_decimals($row['total']); ?></td>
	</tr>

	<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" style="text-align: right;">Total</td>
			<td style="text-align: right;font-weight: bold;"><?= format_decimals($pen['sub_total']); ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right;">Diskon</td>
			<td style="text-align: right;"><?= format_decimals($pen['diskon']); ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right;">Grand Total</td>
			<td style="text-align: right;font-weight: bold;"><?= format_decimals($pen['sub_total'] - $pen['diskon']); ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right;">Bayar</td>
			<td style="text-align: right;font-weight: bold;"><?= format_decimals($pen['bayar']); ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right;">Kembali</td>
			<td style="text-align: right;font-weight: bold;"><?= format_decimals($pen['kembali']); ?></td>
		</tr>
	</tfoot>
</table>

</div>
</div>
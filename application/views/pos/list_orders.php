 <style type="text/css">
.card-active {
    border-bottom: 3px solid #308ee0;
}

.icon-blue {
    color: #308ee0;
}

.icon-grey {
    color: #DDD;
}

.color-grey {
    color: #DDD;
}

.bill-body {
    padding: 25px;
    padding-top: 20px;
    background: #fff;
    box-shadow: 0 6px 12px 0 rgba(0, 0, 0, .07);

}

.bill-body table {
    font-size: 14px;
    color: #6b777e;
}

.bill-body table td {
    padding-bottom: 10px;
}
 </style>
 <div class="main-panel">
     <div class="content-wrapper">


         <div class="row">

             <div class="col-lg-12 grid-margin stretch-card">

                 <div class="card">
                     <div class="card-body">
                         <form action="" method="POST">
                             <div class="row">
                                 <div class="col-md-3">
                                     <label>Tanggal Awal</label>
                                     <input type="date" name="tgl_awal" class="form-control input-sm"
                                         value="<?php if(isset($tgl_awal)){ echo $tgl_awal; } ?>" required>
                                 </div>
                                 <div class="col-md-3">
                                     <label>Tanggal Akhir</label>
                                     <input type="date" name="tgl_akhir" class="form-control input-sm"
                                         value="<?php if(isset($tgl_akhir)){ echo $tgl_akhir; } ?>" required>
                                 </div>


                                 <div class="col-md-3">

                                     <button type="submit" class="btn btn-info btn-sm" style="margin-top: 35px;"><i
                                             class="mdi mdi-search-web"></i> Cari</button>
                                 </div>
                             </div>
                         </form>
                     </div>
                 </div>

             </div>


             <div class="col-lg-12 grid-margin stretch-card">

                 <div class="card">
                     <div class="card-body">

                         <div class="row">
                             <div class="col-lg-6">
                                 <h4 class="card-title">Data Penjualan</h4>
                             </div>
                             <div class="col-lg-6" style="text-align: right;">

                             </div>
                         </div>


                         <div class="table-responsive">
                             <table class="table table-hover">
                                 <thead>
                                     <tr>
                                         <th> Tanggal </th>
                                         <th> No Nota </th>
                                         <th> Total (Rp) </th>
                                         <th>Action</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                foreach ($penjualan as $row) {


                 ?>
                                     <tr style="cursor: pointer;" class="btn-detail" key="<?= $row['id']; ?>">
                                         <td> <?php echo date('d/m/Y H:i', strtotime($row['tgl'])); ?> </td>
                                         <td> <?php echo $row['no_nota']; ?></td>
                                         <td> <?php echo decimals($row['sub_total']); ?> </td>
                                         <td>
                                             <a class="btn btn-info btn-sm print-struk" style="color: #FFF;"
                                                 key="<?= $row['id']; ?>"><i class="mdi mdi-printer"></i> Print</a>
                                         </td>
                                     </tr>
                                     <?php
              }
              ?>


                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div>
             </div>

         </div>
     </div>
 </div>

 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Penjualan Detail</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body load_penjualan" style="width: 100%;">

             </div>
         </div>
     </div>
 </div>


 <script type="text/javascript">
$('.btn-detail').click(function() {
    var id = $(this).attr('key');
    $.ajax({
        type: "GET",
        url: "<?php echo base_url().'kasir/detail_penjualan/'; ?>" + id,
        dataType: 'html',
        success: function(data) {
            $('.load_penjualan').html(data);
        }
    });

    $('#exampleModal').modal('show');
});

$('.print-struk').click(function() {
    var id_penjualan = $(this).attr('key');

    window.open('<?php echo base_url().'kasir/print/' ?>' + id_penjualan, '_blank',
        'location=yes,height=570,width=520,scrollbars=yes,status=yes');

});
 </script>
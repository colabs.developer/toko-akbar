<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Pengaduan - Akbar</title>

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.base.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.addons.css'; ?>">

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url().'favicon.ico'; ?>" />

	<script type="text/javascript" src="<?php echo base_url().'admin_assets/js/jquery.min.js'; ?>"></script>

	<style type="text/css">
		.form-control{
			border-color: #8c96a0;
		}
	</style>

	
</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
			<div class="content-wrapper d-flex align-items-center auth theme-one" style="background-color: #2c3e50;">
				<div class="row w-100">
					<div class="col-lg-6 mx-auto">
						<div class="auto-form-wrapper">
							<center>
								<img src="<?php echo base_url().'assets/logo.svg'; ?>">
							</center>
							<form class="form-login">
								<div class="form-group">
									<label class="label">Nama</label>
									<input type="text" class="form-control" placeholder="Nama"  name="nama">
								</div>
								<div class="form-group">
									<label class="label">No. Telephone</label>
									<input type="text" class="form-control" placeholder="No. Telephone" id="tlp" name="tlp">
								</div>
								<div class="form-group">
									<label class="label">Judul</label>
									<input type="text" class="form-control" placeholder="Judul" name="judul">
								</div>
								<div class="form-group">
									<label class="label">Pesan</label>								
									<textarea class="form-control" name="pesan" placeholder="Tulis Pesan"></textarea>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success submit-btn btn-block" id="login">Kirim</button>
								</div>
								<!-- <p>Daftar akun <a href="<?php //echo base_url().'masuk/daftar'; ?>"> disini</a></p> -->
							</form>
						</div>
						<p class="footer-text text-center">copyright © <?php echo date('Y'); ?> All rights reserved.</p>
					</div>
				</div>
			</div>

		</div>

	</div>
	<script type="text/javascript">
		$('.form-login').submit(function(){
			event.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				type: "POST",
				url: "<?php echo base_url().'pengaduan/simpan_pengaduan'; ?>",
				data:formData,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(data) {
					if(data == 1)
					{
						
						swal({
							icon: "success",
							text: "Berhasil menambahkan pengaduan",
						});

						$('.form-control').val('');
					}
				}
			});
		});




	</script>

	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.base.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.addons.js'; ?>"></script>

	<script src="<?php echo base_url().'admin_assets/js/off-canvas.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/js/misc.js'; ?>"></script>

</body>

</html>
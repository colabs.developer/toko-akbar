<style type="text/css">
  thead tr th:last-child
  {
    text-align: left;
  }
  tbody tr td:last-child
  {
    text-align: center;
  }
  .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }

</style>
<div class="main-panel">
  <div class="content-wrapper">


    <div class="row">

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="cursor: pointer;" onclick="window.location='<?php echo base_url().'pengaturan/cabang'; ?>'; ">
        <div class="card card-statistics card-active">
          <div class="card-body">
            <div class="clearfix">
              <div class="row">
                <div class="col-lg-2">
                  <i class="mdi mdi-store icon-lg icon-blue"></i>
                </div>
                <div class="col-lg-10">
                  <div class="fluid-container" style="margin-top: 15px;">
                    <h3 class="font-weight-medium  mb-0">Outlet</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="cursor: pointer;" onclick="window.location='<?php echo base_url().'pengaturan/pajak'; ?>'; ">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="row">
                <div class="col-lg-2">
                  <i class="mdi mdi-book icon-lg icon-grey"></i>
                </div>
                <div class="col-lg-10">
                  <div class="fluid-container" style="margin-top: 15px;">
                    <h3 class="font-weight-medium  mb-0 color-grey">Pajak</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

         <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="cursor: pointer;" onclick="window.location='<?php echo base_url().'pengaturan/struk'; ?>'; ">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="row">
                <div class="col-lg-2">
                  <i class="mdi mdi-receipt icon-lg icon-grey"></i>
                </div>
                <div class="col-lg-10">
                  <div class="fluid-container" style="margin-top: 15px;">
                    <h3 class="font-weight-medium  mb-0 color-grey">Desain Struk</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>



      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Cabang / Outlet</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
                <a href="<?php echo base_url().'pengaturan/form_cabang' ?>" class="btn btn-success btn-fw">Tambah</a>
              </div>
            </div>


            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th width="11%;">Nama Cabang</th>
                    <th width="11%;">Kota</th>
                    <th>Alamat</th>
                    <th>Kode Nota</th> 
                    <th>Pajak</th> 
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                  <?php $no =1; foreach ($cabang as $row) {


                    ?>
                    <tr>
                      <td scope="row"><?php echo $no; ?></td>
                      <td><?php echo $row['nama']; ?></td>
                      <td><?php echo $row['kota']; ?></td>
                      <td><?php echo $row['alamat']; ?></td>
                      <td ><?php echo $row['kode_nota']; ?></td>
                      <td><?php if($row['id_pajak'] == 0){ echo "Tidak ada pajak"; }else{ echo "Ada pajak"; } ?></td>

                      <td style="text-align: center;"><a href="<?php echo base_url().'pengaturan/form_cabang/'.$row['id']; ?>" class="btn btn-primary"><i class="mdi mdi-pencil"></i></a></td>

                    </tr>
                    <?php
                    $no++;
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
  $('.hapus').click(function(event){
    event.preventDefault();
    var id_barang = $(this).attr('key');


    swal({
      title: "Apakah kamu yakin ?",
      text: "hapus data barang ini",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'admin/remove_barang'; ?>",
          data: {id_barang:id_barang},
          success: function(data){

            if(data == 1)
            {
             swal("Success!","Berhasil hapus barang.", "success")
             .then((value) => {
              location.reload();
            });
           }

         }
       });


      } else {

      }
    });




  });
</script>

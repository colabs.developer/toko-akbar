 <div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form class="formcabang">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Tambah Cabang</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw">Simpan</button>
             </div>
           </div>

           <div class="row">
            <div class="col-lg-6">

              <?php if(isset($id)){
                ?>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <?php
              } ?>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?php if(isset($id)){ echo $cabang['nama']; } ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Kode Nota</label>
                <input type="text" class="form-control" id="kode_nota" name="kode_nota" value="<?php if(isset($id)){ echo $cabang['kode_nota']; } ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">No Telephone</label>
                <input type="text" class="form-control" id="tlp" name="tlp" value="<?php if(isset($id)){ echo $cabang['tlp']; } ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Alamat</label>
                <textarea  class="form-control" id="alamat" name="alamat"><?php if(isset($id)){ echo $cabang['alamat']; } ?></textarea>
              </div>

              <div class="form-group">
                <label>Status</label>
                <select class="form-control" id="status" name="status">
                  <option>-- Pilih Status --</option>
                  <option value="1" <?php if(isset($id)){ if($cabang['status'] == 1){ echo "Selected"; }} ?>>Aktif</option>
                  <option value="0" <?php if(isset($id)){ if($cabang['status'] == 0){ echo "Selected"; }} ?>>Non Aktif</option>
                </select>
              </div>

            </div>

            <div class="col-lg-6">



              <div class="form-group">
                <label>Provinsi</label>
                <select class="form-control" name="provinsi" id="provinsi">
                  <option>-- Pilih Provinsi --</option>
                  <?php 
                  $get = $this->db->get('t_provinsi')->result_array();

                  foreach ($get as $row) {
                    ?>
                    <option value="<?php echo $row['id']; ?>" <?php if(isset($id)){ if($row['id'] == $cabang['id_provinsi']){ echo "Selected"; } } ?>><?php echo $row['nama']; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>


              <div class="form-group">
                <label>Kota</label>
                <select class="form-control" name="kota" id="kota">
                  <option>-- Pilih Kota --</option>
                  <?php 
                  if(isset($id) && $cabang['id_kabupaten'] != ''){
                    $get = $this->db->get_where('t_kabupaten', array('id_prov' => $cabang['id_provinsi']))->result_array();

                    foreach ($get as $row) {
                      ?>
                      <option value="<?php echo $row['id']; ?>" <?php if(isset($id)){ if($row['id'] == $cabang['id_kabupaten']){ echo "Selected"; } } ?>><?php echo $row['nama']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>



              <div class="form-group">
                <label>Kecamatan</label>
                <select class="form-control" name="kecamatan" id="kecamatan">
                  <option>-- Pilih Kecamatan --</option>
                  <?php 
                  if(isset($id) && $cabang['id_kecamatan'] != ''){
                    $get = $this->db->get_where('t_kecamatan', array('id_kabupaten' => $cabang['id_kabupaten']))->result_array();

                    foreach ($get as $row) {
                      ?>
                      <option value="<?php echo $row['id']; ?>" <?php if(isset($id)){ if($row['id'] == $cabang['id_kecamatan']){ echo "Selected"; } } ?>><?php echo $row['nama']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label>Kelurahan</label>
                <select class="form-control" name="kelurahan" id="kelurahan">
                  <option>-- Pilih Kelurahan --</option>
                  <?php 
                  if(isset($id)){
                    $get = $this->db->get_where('t_kelurahan', array('id_kecamatan' => $cabang['id_kecamatan']))->result_array();

                    foreach ($get as $row) {
                      ?>
                      <option value="<?php echo $row['id']; ?>" <?php if(isset($id)){ if($row['id'] == $cabang['id_kelurahan']){ echo "Selected"; } } ?>><?php echo $row['nama']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>


              <div class="form-group">
                <label>Pajak</label>
                <select class="multiple-select2 form-control" name="pajak[]" multiple="multiple" style="border:1px solid #f2f2f2 !important;">
                  <?php 
                  foreach ($pajak as $row) {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['nama_pajak'] ?></option>
                  <?php
                  }
                   ?>
                </select>
              </div>


              

            </div>

             <?php 
             $select_cabang='';
             if(isset($id)){
                $arr_cabang = (explode(",",$cabang['id_pajak']));
                foreach ($arr_cabang as $key) {
                 $select_cabang .= "'".$key."' ,";
                }
              } ?>


          </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">
  $(document).ready(function() {
    $('.multiple-select2').select2();


    $('.multiple-select2').val([<?php echo $select_cabang; ?>]);
    $('.multiple-select2').trigger('change');
});

  $('#provinsi').change(function(){
   var id= $(this).val();

   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'pengaturan/load_kota'; ?>",
    data: {id:id},
    dataType: 'html',
    success: function(data) {
      $("#kota option").remove();
      $("#kecamatan option").remove();
      $("#kelurahan option").remove();
      $('#kota').append(data);

    }
  });

 });

  $('#kota').change(function(){
   var id= $(this).val();

   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'pengaturan/load_kecamatan'; ?>",
    data: {id:id},
    dataType: 'html',
    success: function(data) {
      $("#kecamatan option").remove();
      $("#kelurahan option").remove();
      $('#kecamatan').append(data);

    }
  });

 });

  $('#kecamatan').change(function(){
   var id= $(this).val();

   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'pengaturan/load_kelurahan'; ?>",
    data: {id:id},
    dataType: 'html',
    success: function(data) {
      $("#kelurahan option").remove();
      $('#kelurahan').append(data);

    }
  });

 });


  $(".formcabang").submit(function(event){
    event.preventDefault();


    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'pengaturan/simpan_cabang'; ?>",
      data: $(this).serialize(),
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'pengaturan/cabang'; ?>';
        }
      }
    });

  });
</script>
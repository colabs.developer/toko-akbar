<link rel="stylesheet" href="<?php echo base_url().'admin_assets/select2/select2.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'admin_assets/select2/select2-bootstrap.min.css' ?>">
<script src="<?php echo base_url().'admin_assets/select2/select2.full.js'; ?>"></script>

<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">





            <form id="formstok">
             <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Produk</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              <button type="submit" class="btn btn-success btn-fw">Simpan</button>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th tyle="width: 200px;"> Nama Produk </th>
                  <th> Jumlah </th>
                  <th> Satuan </th>
                  <th> Harga </th>
                  <th> Total harga </th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="disini">
               <tr>
                 <td style="width: 200px;">
                   <div class="form-group">

                     <select class="form-control col-lg-12 nama_produk" name="id_produk[]" onchange="get_harga(1, $(this).val())">
                      <option>-- Produk --</option>
                      <?php 
                      $get = $this->db->get_where('produk', array('id_cabang' => $_SESSION['cabang']))->result_array();
                      foreach ($get as $row) {
                        ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['nama'] ?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </td>
                <td>
                 <div class="form-group">

                  <input type="text" class="form-control"  name="jumlah[]" id="jumlah1" onkeyup="hitung(1);">
                </div>
              </td>
              <td>
                <p id="satuan1"></p>
              </td>
              <td>
               <div class="form-group">

                <input type="text" class="form-control"  name="harga[]" id="harga1">
              </div>
            </td>
            <td>
             <div class="form-group">

              <input type="text" class="form-control"  name="total[]" id="total1">
               <input type="hidden" name="status[]" value="in">
            </div>
          </td>
          <td>

          </td>
        </tr>

      </tbody>
      <tfoot>
        <tr>
          <td colspan="6">
            <a id="tambah_produk" style="color: blue;cursor: pointer;"><i class="mdi mdi-plus"></i> Tambah produk</a>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</form>
</div>
</div>
</div>

</div>
</div>
</div>


<script type="text/javascript">

  $( ".nama_produk" ).select2({
    theme: "bootstrap"
  });



  function get_harga(id,value){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/detail_produk'; ?>",
      data: {value:value},
      dataType: 'json',
      success: function(data) {
        $('#harga'+id).val(data.harga);
        $('#satuan'+id).text(data.satuan);
      }
    });


  }

  function hitung(id)
  {
    var jml = $('#jumlah'+id).val();
    var harga = $('#harga'+id).val();
    var hasil = jml * harga;

    $('#total'+id).val(hasil);

  }


  $('#tambah_produk').click(function(){
    var jml= parseInt($('.nama_produk').last().attr('urut'))+1;

    var html = '<tr> <td style="width: 200px;"><div class="form-group">'+
    '<select  name="id_produk[]" class="form-control col-lg-12 nama_produk'+jml+'" onchange="get_harga('+jml+', $(this).val())">'+
    '<option>-- Produk --</option>'+
    '<?php foreach ($get as $row) { ?>'+
    '<option value="<?php echo $row['id']; ?>"><?php echo $row['nama'] ?></option><?php } ?>'+ 
    '</select></div></td>'+
    '<td><div class="form-group"><input type="text" class="form-control"  name="jumlah[]" id="jumlah'+jml+'" onkeyup="hitung('+jml+');"></div></td>'+
    '<td><p id="satuan'+jml+'"></p></td>'+
    '<td><div class="form-group"><input type="text" class="form-control"  name="harga[]" id="harga'+jml+'"></div></td>'+
    '<td><div class="form-group"><input type="text" class="form-control"  name="total[]" id="total'+jml+'"><input type="hidden" name="status[]" value="in"></div></td>'+
    '<td></td></tr>';

    $('#disini').append(html);


    $( ".nama_produk"+jml).select2({
      theme: "bootstrap"
    });


  });

$('#formstok').submit(function(event){
   event.preventDefault();
   $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/simpan_stok_masuk'; ?>",
      data: $(this).serialize(),
      
      success: function(data) {
       
      }
    });
});


</script>
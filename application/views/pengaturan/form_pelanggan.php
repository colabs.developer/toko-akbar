 <div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form class="formpelanggan">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Add Customer</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw btn-submit">Save</button>
             </div>
           </div>

           <div class="row">
            <div class="col-lg-6">

              <?php if(isset($id)){
                ?>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <?php
              } ?>
              <div class="form-group">
                <label for="exampleInputEmail1">Name <span class="text-danger" title="Reuired">*</span></label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?php if(isset($id)){ echo $pelanggan['nama']; } ?>" required>
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Email <span class="text-danger" title="Reuired">*</span></label>
                <input type="email" class="form-control" id="email" name="email" value="<?php if(isset($id)){ echo $pelanggan['email']; } ?>" required>
              </div>


              <div class="form-group">
                <label for="exampleInputEmail1">Phone Number <span class="text-danger" title="Reuired">*</span></label>
                <input type="number" class="form-control" id="tlp" name="tlp" value="<?php if(isset($id)){ echo $pelanggan['tlp']; } ?>" required>  
              </div>

              <div class="form-group">
                <label>City <span class="text-danger" title="Reuired">*</span></label>
                <input type="text" name="kota" class="form-control" value="<?php if(isset($id)){ echo $pelanggan['kota']; } ?>" required>
              </div>

              <div class="form-group">
                <label>Postal Code <span class="text-danger" title="Reuired">*</span></label>
                <input type="text" name="kode_pos" class="form-control" value="<?php if(isset($id)){ echo $pelanggan['kode_pos']; } ?>" required>
              </div>

              <div class="form-group">
                <label>Address <span class="text-danger" title="Reuired">*</span></label>
                <textarea class="form-control" name="alamat" id="alamat" required><?php if(isset($id)){ echo $pelanggan['alamat']; } ?></textarea>
              </div>

              <div class="form-group">
                <label>Status <span class="text-danger" title="Reuired">*</span></label>
                <select class="form-control" id="status" name="status" required>
                  <option>-- Status --</option>
                  <option value="1" <?php if(isset($id)){ if($pelanggan['status'] == 1){ echo "Selected"; }} ?>>Aktif</option>
                  <option value="0" <?php if(isset($id)){ if($pelanggan['status'] == 0){ echo "Selected"; }} ?>>Inaktif</option>
                </select>
              </div>

            </div>


          </div>
        </form>
      </div>
    </div>
  </div>

</div>
</div>
</div>



<script type="text/javascript">


  $(".formpelanggan").submit(function(event){
    event.preventDefault();


    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'pengaturan/simpan_pelanggan'; ?>",
      data: $(this).serialize(),
      beforeSend: function() {
        $('.btn-submit').prop('disabled', true);
        $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
      },
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'pengaturan/pelanggan'; ?>';
        }else{
          swal({
            icon: "warning",
            text: data,
          });
        }
        $('.btn-submit').prop('disabled', false);
        $('.btn-submit').removeClass('btn-secondary').addClass('btn-success').text('Save');
      }
    });

  });
</script>
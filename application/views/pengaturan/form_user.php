 <div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form class="formpegawai">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Add User</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
                <button type="submit" class="btn btn-success btn-fw btn-submit">Save</button>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">

                <?php if(isset($id)){
                  ?>
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                  <?php
                } ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Name <span class="text-danger" title="Reuired">*</span></label>
                  <input type="text" class="form-control" id="nama" name="nama" value="<?php if(isset($id)){ echo $user['nama']; } ?>" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number <span class="text-danger" title="Reuired">*</span></label>
                  <input type="number" class="form-control" id="tlp" name="tlp" value="<?php if(isset($id)){ echo $user['tlp']; } ?>" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Email <span class="text-danger" title="Reuired">*</span></label>
                  <input type="email" class="form-control" id="email" name="email" value="<?php if(isset($id)){ echo $user['email']; } ?>" required>
                </div>

                <?php if(!isset($id)){ ?>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Password <span class="text-danger" title="Reuired">*</span></label>
                  <input type="password" class="form-control" id="password" name="password"  required>
                </div>
              <?php } ?>




            </div>

            <div class="col-lg-6">

             <div class="form-group">
              <label>Privilege <span class="text-danger" title="Reuired">*</span></label>
              <select class="form-control" id="level" name="level" required>
                <option>-- Privilege --</option>
                <?php foreach ($hak_akses as $row) { ?>
                  <option value="<?php echo $row['id'] ?>" <?php if(isset($id)){ if($user['level'] == $row['id']){ echo "Selected"; }} ?>><?php echo $row['nama']; ?></option>
                  <?php
                } ?>

              </select>
            </div>

            <div class="form-group">
              <label>Status <span class="text-danger" title="Reuired">*</span></label>
              <select class="form-control" id="status" name="status" required>
                <option>-- Status --</option>
                <option value="1" <?php if(isset($id)){ if($user['status'] == 1){ echo "Selected"; }} ?>>Aktif</option>
                <option value="0" <?php if(isset($id)){ if($user['status'] == 0){ echo "Selected"; }} ?>>Inaktif</option>
              </select>
            </div>

          </div>


        </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">


  $(".formpegawai").submit(function(event){
    event.preventDefault();


    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'pengaturan/simpan_user'; ?>",
      data: $(this).serialize(),
      beforeSend: function() {
        $('.btn-submit').prop('disabled', true);
        $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
      },
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'pengaturan/users'; ?>';
        }

        $('.btn-submit').prop('disabled', false);
        $('.btn-submit').removeClass('btn-secondary').addClass('btn-success').text('Save');
      }
    });

  });
</script>
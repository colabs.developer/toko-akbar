<style type="text/css">
    thead tr th:last-child
    {
        text-align: left;
    }
     tbody tr td:last-child
    {
        text-align: center;
    }
</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Kategori</h3> </div>
           <!--  <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div> -->
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                      <button type="button" class="btn btn-info btn-flat btn-addon m-b-10 m-l-5 " onclick="location.href='<?php echo base_url().'admin/form_kategori'; ?>'"><i class="ti-plus"></i>Tambah Kategori</button>   
                   <div class="card">
                    <div class="card-title">
                        <h4>List Kategori </h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Edit</th>
                                        <!-- <th>Hapus</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no =1;
                                    foreach ($kategori as $row) {
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $no; ?></th>
                                            <td><?php echo $row['nama_kategori']; ?></td>                                    
                                            <td style="text-align: center;"><a href="<?php echo base_url().'admin/form_kategori/'.$row['id_kategori']; ?>" class="btn btn-primary">Edit</a></td>
                                             <!-- <td><button class="btn btn-danger hapus" key="<?php //echo $row['id_kategori']; ?>">Hapus</button></td> -->
                                        </tr>
                                        <?php
                                        $no++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->



    <script type="text/javascript">
        $('.hapus').click(function(event){
            event.preventDefault();
            var id_kategori = $(this).attr('key');


            swal({
              title: "Apakah kamu yakin ?",
              text: "hapus data kategori ini",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {

                  $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_kategori'; ?>",
                data: {id_kategori:id_kategori},
                success: function(data){
                  
                    if(data == 1)
                    {
                       swal("Success!","Berhasil hapus kategori.", "success")
                       .then((value) => {
                          location.reload();
                      });
                   }

               }
           });

    
            } else {
                
            }
        });




        });
    </script>


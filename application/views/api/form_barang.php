 <div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">
          


           <form class="formbarang">

              <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Tambah Produk</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw">Simpan</button>
             </div>
           </div>

            <div class="row">
              <div class="col-lg-6">

                <?php if(isset($id)){
                  ?>
                  <input type="hidden" name="id_barang" value="<?php echo $id; ?>">
                  <?php
                } ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?php if(isset($id)){ echo $barang['nama']; } ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kategori</label>
                  <select class="form-control" id="id_kategori" name="id_kategori">
                   <?php
                   $get= $this->db->get('kategori')->result_array();
                   foreach ($get as $row) {
                    ?>
                    <option value="<?php echo $row['id_kategori'] ?>" <?php if(isset($id)){ if($row['id_kategori'] == $barang['id_kategori']){  echo "selected"; } } ?>><?php echo $row['nama_kategori'] ?></option>
                    <?php
                  }
                  ?>

                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Harga</label>
                <input type="text" class="form-control" id="harga" name="harga" placeholder="Rp0" value="<?php if(isset($id)){ echo $barang['harga']; } ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Foto Produk</label>
                <input type="file" class="form-control" name="foto_produk">
              </div>

            </div>


            <div class="col-lg-6">

            <div class="form-group">
              <label for="exampleInputEmail1">Barcode</label>
              <input type="text" class="form-control" id="barcode" name="barcode" placeholder="" value="<?php if(isset($id)){ echo $barang['kode']; } ?>">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Satuan Stok</label>
              <select class="form-control" id="satuan" name="satuan">
                <option value="Pcs" <?php if(isset($id)){ if('Pcs' == $barang['satuan']){  echo "selected"; } } ?>>Pcs</option>
                <option value="Gram" <?php if(isset($id)){ if('Gram' == $barang['satuan']){  echo "selected"; } } ?>>Gram</option>
                <option value="Kg" <?php if(isset($id)){ if('Kg' == $barang['satuan']){  echo "selected"; } } ?>>Kg</option>
                <option value="Porsi" <?php if(isset($id)){ if('Porsi' == $barang['satuan']){  echo "selected"; } } ?>>Porsi</option>
                <option value="Botol" <?php if(isset($id)){ if('Botol' == $barang['satuan']){  echo "selected"; } } ?>>Botol</option>
                <option value="Gelas" <?php if(isset($id)){ if('Gelas' == $barang['satuan']){  echo "selected"; } } ?>>Gelas</option>
              </select>
            </div>

            <div class="form-group">
              <label>Keterangan</label>
              <textarea name="keterangan" id="keterangan" class="form-control"><?php if(isset($id)){ echo $barang['ket']; } ?></textarea>
            </div>
          </div>
        </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">
  $(".formbarang").submit(function(event){
    event.preventDefault();


    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_barang'; ?>",
      data: $(this).serialize(),
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'admin/barang'; ?>';
        }
      }
    });

  });
</script>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Kategori</h3> </div>

        </div>
        
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">                                                               
                                            <div class="form-group">
                                                <label>KAS</label>
                                                <select class="form-control input-default" name="kas">
                                                   <option>-- Pilih KAS --</option>
                                                   <?php foreach ($kas as $row) { ?>
                                                       <option value="<?php echo $row['id_kas']; ?>"><?php echo $row['nama_kas']; ?></option>
                                                   <?php } ?>
                                               </select>
                                           </div> 
                                           <div class="form-group">
                                            <label>Tanggal</label>
                                            <input name="waktu" type="date" class="form-control input-default " placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <input name="keterangan" type="text" class="form-control input-default " placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Masuk</label>
                                            <input name="masuk" type="text" class="form-control input-default " placeholder="">
                                        </div>

                                        <div class="form-group">
                                            <label>Keluar</label>
                                            <input name="keluar" type="text" class="form-control input-default " placeholder="">
                                        </div>



                                    </div>



                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-info col-md-2">Save</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();


        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'admin/simpan_kas'; ?>",
            data: formdata,
            success: function(data){
             if(data == 1)
             {
                swal("Success!","Berhasil simpan KAS.", "success")
               .then((value) => {
                  window.location = "<?php echo base_url().'admin/kas'; ?>";
              });
           }

       }
   });

    });
</script>

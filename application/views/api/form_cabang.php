
<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Cabang</h3> </div>

        </div>
        
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">                                                               

                                         <div class="form-group">
                                            <label>Nama Cabang</label>
                                            <input name="nama_cabang" type="text" class="form-control input-default " placeholder="Nama Cabang" value="<?php if(isset($id)){ echo $cabang['nama_cabang']; } ?>">

                                             <?php if(isset($id)){ ?>
                                                <input type="hidden" name="id_cabang" value="<?php if(isset($id)){ echo $cabang['id_cabang']; } ?>">
                                            <?php } ?>
                                        </div>

                                    </div>

                                

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info col-md-2">Save</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();

            $.ajax({
                type: "POST",
                 url: "<?php echo base_url().'admin/simpan_cabang'; ?>",
                data: formdata,
                success: function(data){
                  
                    if(data == 1)
                    {
                         swal("Success!","Berhasil simpan cabang.", "success")
                        .then((value) => {
                          window.location = "<?php echo base_url().'admin/cabang'; ?>";
                      });
                    }

                }
            });

    });
</script>
<style type="text/css">
thead tr th:last-child
{
    text-align: left;
}
tbody tr td:last-child
{
    text-align: center;
}
</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Gudang</h3> </div>
           <!--  <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div> -->
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-info btn-flat btn-addon m-b-10 m-l-5 " onclick="location.href='<?php echo base_url().'admin/form_gudang'; ?>'"><i class="ti-plus"></i>Tambah Gudang</button> 
                   <div class="card">
                    <div class="card-title">
                        <h4>List Gudang </h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cabang</th>
                                        <th>Gudang</th>
                                        <th>Edit</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no =1;
                                    foreach ($gudang as $row) {
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $no; ?></th>
                                            <td><?php echo $row['nama_cabang']; ?></td>
                                            <td><?php echo $row['nama_gudang']; ?></td>     

                                            <td style="text-align: center;"><a href="<?php echo base_url().'admin/form_gudang/'.$row['id_gudang'] ?>" class="btn btn-primary">Edit</a></td>
                                              
                                           </tr>
                                           <?php
                                           $no++;
                                       } ?>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                   </div>

               </div>
           </div>
           <!-- End PAge Content -->
       </div>
       <!-- End Container fluid  -->

        <script type="text/javascript">
        $('.hapus').click(function(event){
            event.preventDefault();
            var id_gudang = $(this).attr('key');


            swal({
              title: "Apakah kamu yakin ?",
              text: "hapus data gudang ini",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {

                  $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_gudang'; ?>",
                data: {id_gudang:id_gudang},
                success: function(data){
                  
                    if(data == 1)
                    {
                       swal("Success!","Berhasil hapus gudang.", "success")
                       .then((value) => {
                          location.reload();
                      });
                   }

               }
           });

    
            } else {
                
            }
        });




        });
    </script>




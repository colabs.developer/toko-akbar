<style type="text/css">
thead tr th:last-child
{
    text-align: left;
}
tbody tr td:last-child
{
    text-align: center;
}


</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pelanggan</h3> </div>
           <!--  <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div> -->
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    
                   <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" style="">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Kota</th>
                                        <th>Provinsi</th>
                                        <th>Cabang</th>
                                        <th>Mercant</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no =1; foreach ($pelanggan as $row) {

                                        if($row['kelamin'] == 'L')
                                        {
                                            $jk = 'Laki';
                                        }else{
                                            $jk = 'Perempuan';
                                        }
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $no; ?></th>
                                            <td><?php echo $row['nama']; ?></td>
                                            <td><?php echo $jk; ?></td>
                                            <td><?php echo $row['alamat']; ?></td>
                                            <td ><?php echo $row['kota']; ?></td>
                                            <td ><?php echo $row['propinsi']; ?></td>
                                            <td><?php echo $row['nama_cabang']; ?></td>
                                            <td style="text-align: center;"><a href="<?php echo base_url().'mercant/form_mercant/?user_id='.$row['id'] ?>"  class="btn btn-primary btn-sm"><i class="fa fa-plus" style="color:#FFF;"></i></a></td>
                                            
                                        </tr>
                                        <?php
                                        $no++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

    <script type="text/javascript">
        $('.hapus').click(function(event){
            event.preventDefault();
            var id_barang = $(this).attr('key');


            swal({
              title: "Apakah kamu yakin ?",
              text: "hapus data barang ini",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {

                  $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_barang'; ?>",
                data: {id_barang:id_barang},
                success: function(data){
                  
                    if(data == 1)
                    {
                       swal("Success!","Berhasil hapus barang.", "success")
                       .then((value) => {
                          location.reload();
                      });
                   }

               }
           });

    
            } else {
                
            }
        });




        });
    </script>



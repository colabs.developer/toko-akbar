
<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">My Profile</h3> </div>

        </div>
        
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama" class="form-control input-default " placeholder="Nama" value="<?php  echo $user['nama']; ?>">

                                            </div>

                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" name="email" class="form-control input-default " placeholder="Email" value="<?php  echo $user['email']; ?>">

                                            </div>

                                            <div class="form-group">
                                                <label>Jenis kelamin</label>
                                                <select class="form-control input-default" name="jenis_kelamin">
                                                    <option>-- Pilih Kelamin --</option>
                                                    <option value="L" <?php  if($user['kelamin'] == 'L'){ echo 'selected'; } ?>>Laki-Laki</option>
                                                    <option value="P" <?php  if($user['kelamin'] == 'P'){ echo 'selected'; } ?>>Perempuan</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <input type="text" name="alamat" class="form-control input-default " placeholder="Alamat" value="<?php  echo $user['alamat']; ?>">
                                            </div>

                                           
                                            <div class="form-group">
                                                <label>Telepone</label>
                                                <input type="text" name="tlp" class="form-control input-default " placeholder="Telepone" value="<?php echo $user['tlp'];  ?>">
                                            </div>




                                        </div>



                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info col-md-2">Save</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'user/update_data'; ?>",
            data: formdata,
            success: function(data){

                if(data == 1)
                {
                   swal("Success!","Berhasil update profile.", "success")
                   .then((value) => {
                      window.location = "<?php echo base_url().'admin/'; ?>";
                  });
               }else{
                  swal("Gagal !",data, "warning");
               }

           }
       });

    });
</script>

<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">My Profile</h3> </div>

        </div>
        
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password lama</label>
                                                <input type="password" name="old_pass" class="form-control input-default " placeholder="">

                                            </div>

                                            <div class="form-group">
                                                <label>Password baru</label>
                                                <input type="password" name="pass1" id="pass1" class="form-control input-default " placeholder="">

                                            </div>

                                             <div class="form-group">
                                                <label>Ulangi password</label>
                                                <input type="password" name="pass2" id="pass2" class="form-control input-default " placeholder="" >

                                            </div>

                                



                                        </div>



                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info col-md-2">Save</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();

        var pass1 = $('#pass1').val();
        var pass2 = $('#pass2').val();

        if(pass1 != pass2)
        {
           swal("Gagal !","Password tidak sama.", "warning");
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'user/ganti_pass'; ?>",
            data: formdata,
            success: function(data){

                if(data == 1)
                {
                   swal("Success!","Berhasil ganti password.", "success")
                   .then((value) => {
                      window.location = "<?php echo base_url().'admin/'; ?>";
                  });
               }else{
                    swal("Gagal !",data, "warning")
               }

           }
       });

    });
</script>
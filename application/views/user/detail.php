<!-- Feature Category Section & sidebar -->
<?php $count = $this->db->query("SELECT COUNT(counter_id) as total FROM conter_blog WHERE id_article = '".$blog['id_article']."'")->row_array(); ?>
    <section id="feature_category_section" class="feature_category_section single-page section_wrapper">
    <div class="container">   
        <div class="row">
             <div class="col-md-9">
                <div class="single_content_layout">
                    <div class="item feature_news_item">
                        <div class="item_img">
                            <img  class="img-responsive" src="<?php echo base_url().'img_blog/'.$blog['image']; ?>" alt="">
                        </div><!--item_img--> 
                            <div class="item_wrapper">
                                <div class="news_item_title">
                                    <h2><a href="#"><?php echo $blog['title']; ?></a></h2>
                                </div><!--news_item_title-->
                                <i class="fa fa-eye"></i> <?php echo $count['total']; ?>
                                <div class="item_meta"><a href="#"><?php echo dateIndo($blog['publish_date']); ?>,</a> by:<a href="#">Admin</a></div>

                                    <span class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-full"></i>
                                    </span>
                                    <br><br>
                                     <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5affcfedbceef4c0"></script>
                                    <div class="addthis_inline_share_toolbox"></div>
                                

                                    <div class="item_content">
                                       <?php echo $blog['isi']; ?>
                                    </div><!--item_content-->
                                    <div class="category_list">
                                        <?php 
                                        $kategori = $this->db->get_where('kategori_berita',array('aktif' => 1))->result_array();
                                        foreach ($kategori as $row) {
                                        ?>

                                            <a href="#"><?php echo $row['nama_kategori']; ?></a>
                                        <?php
                                        }
                                         ?>
                                        
                                    </div><!--category_list-->
                            </div><!--item_wrapper-->   
                    </div><!--feature_news_item-->
                    
                    <div class="single_related_news">
                     <div class="single_media_title"><h2>Related News</h2></div>
                        <div class="media_wrapper">

            <?php $kategori = $this->db->get_where("article",array('status' => 1, 'flag' => 0, 'id_kategori' => $blog['id_kategori']))->result_array();

                            foreach ($kategori as $row) {
                                if($blog['id_article'] != $row['id_article']){
                                 ?>
                                


                            <div class="media">
                                <div class="media-left">
                                    <a href="#"><img class="media-object" style="width: 91px;height: 81px;" src="<?php echo base_url().'img_blog/'.$row['image']; ?>" alt="Generic placeholder image"></a>
                                </div><!--media-left-->
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="#"><?php echo $row['title']; ?>
                                    </a></h4>
                                    <div class="media_meta"><a href="#">20Aug- 2015,</a> by:<a href="#">Admin</a></div>
                                    <div class="media_content"><p><?php echo substr(strip_tags($row['isi']), 0,50); ?></p>
                                    </div><!--media_content-->
                                </div><!--media-body-->
                            </div><!--media-->
                             <?php
                             } 
                         }
                         ?>

                        </div><!--media_wrapper-->
                    </div><!--single_related_news-->




                   <!-- 
                    div iklan
                    <div class="ad">
                        <img class="img-responsive" src="assets/img/img-single-ad.jpg" alt="Chania">
                    </div> -->

                    <div class="readers_comment">
                        <div class="single_media_title"><h2>Related Comments</h2></div>

                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=871862866256922&autoLogAppEvents=1';
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"  data-width="750px"></div>
                        
                    
                    </div><!--readers_comment-->

                  
                             
                </div><!--single_content_layout-->
             </div>

            <div class="col-md-3">
                   <div class="most_comment">
                    <div class="sidebar_title">
                        <h2>Populer artikel</h2>
                    </div>
                     
              <?php 
               $get= $this->db->query("SELECT count(b.counter_id) as total, b.id_article , a.* from conter_blog b LEFT JOIN article a ON a.id_article=b.id_article  GROUP BY b.id_article ORDER BY total desc")->result_array();
                  foreach ($get as $row) {

                     $count = $this->db->query("SELECT COUNT(counter_id) as total FROM conter_blog WHERE id_article = '".$row['id_article']."'")->row_array();
               ?>
                    <div class="media">
                        <div class="media-left">
                            <a href="<?php echo base_url().'blog/detail/'.$row['url_slug']; ?>"><img class="media-object" src="<?php echo base_url().'img_blog/'.$row['image']; ?>" alt="" style="width: 80px;height: 80px;"></a>
                        </div><!--media-left-->
                        <div class="media-body">
                            <h3 class="media-heading"><a href="#"><?php echo $row['title']; ?></a></h3>
                             <div class="comment_box">
                                <div class="comments_icon"> <i class="fa fa-eye" aria-hidden="true"></i></div>
                                 <div class="comments"><a href="#"><?php echo $count['total']; ?> View</a></div>
                             </div><!--comment_box-->
                        </div><!--media-body-->
                    </div><!--media-->
               <?php } ?>
                </div><!--most_comment-->

     
        <br><br>
                <div class="most_comment">
                    <div class="sidebar_title">
                        <h2>Artikel Terbaru</h2>
                    </div>
                     <?php 
                $this->db->order_by('id_article','DESC');
                $this->db->limit(5);
                $get = $this->db->get_where('article',array('status' => 1, 'flag' => 0))->result_array();
                  foreach ($get as $row) {

                    $count = $this->db->query("SELECT COUNT(counter_id) as total FROM conter_blog WHERE id_article = '".$row['id_article']."'")->row_array();

               ?>
                    <div class="media">
                        <div class="media-left">
                            <a href="#"><img class="media-object" src="<?php echo base_url().'img_blog/'.$row['image']; ?>" alt="" style="width: 80px;height: 80px;"></a>
                        </div><!--media-left-->
                        <div class="media-body">
                            <h3 class="media-heading"><a href="#"><?php echo $row['title']; ?></a></h3>
                             <div class="comment_box">
                                <div class="comments_icon"> <i class="fa fa-eye" aria-hidden="true"></i></div>
                                 <div class="comments"><a href="#"><?php echo $count['total']; ?> View</a></div>
                             </div><!--comment_box-->
                        </div><!--media-body-->
                    </div><!--media-->
               <?php } ?>
                </div><!--most_comment-->
            </div>
        </div>      
</section><!--feature_category_section-->

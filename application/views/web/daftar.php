<!DOCTYPE html>
<html lang="en">
<head>
	<title>Daftar Akun Growcery - Growcery</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<style type="text/css">
		.bg-abu {
			padding-top: 160px;
			padding-left: 160px!important;
			padding-right: 160px!important;
			padding-bottom: 135px;
			background: #f5f5f5;
		}
		.bg-gambar {
			padding: 100px!important;
			padding-top: 160px;
		}

		.btn-success{
  background-color: #f4d03f !important;
  border-color: #f4d03f !important;
}

.btn-outline-success{
  border-color: #f4d03f !important;
  color: #f4d03f;
}
.btn-outline-success:hover{
	background-color: #f4d03f !important;
  border-color: #f4d03f !important;
  color: #FFF;
}


@media only screen and (max-width: 600px) {
    .bg-abu{
        padding-top:100px !important;
           padding-bottom:20px;
           padding-right: 15px !important;
           padding-left: 15px !important;
           background:#FFF;
    }
    .bg-gambar{
        display: none;
        padding: 0px;
    }
    .text-line.background span {
  		background: #FFF;
	}
}

 
	</style>
</head>
<body>
	
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 bg-abu">
				<form action="<?php echo base_url().'auth/do_regis'; ?>" method="post" id="form_daftar">
					<h4>Daftar Akun Growcery</h4>
					<br>
					<div class="form-group">
						<label>Email</label>
						<input type="text" placeholder="Email" class="form-control" name="email" id="email">
						<div class="invalid-feedback">
				          Email harus diisi.
				        </div>
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" placeholder="Nama" class="form-control" name="nama" id="nama">
						<div class="invalid-feedback">
				          Nama harus diisi.
				        </div>
					</div>
					<div class="form-group">
						<label>Nomor Telephone</label>
						<input type="text" placeholder="Nomor Telephone" class="form-control" name="hp" id="hp">
						<div class="invalid-feedback">
				          Nomor Telephone harus diisi.
				        </div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" placeholder="Your Password" class="form-control" name="password" id="password">
						<div class="invalid-feedback">
				          Password harus diisi.
				        </div>
					</div>
					<button type="submit" class="btn btn-success col-md-4" style="margin-bottom: 20px;"> DAFTAR</button>
					<a href="<?php echo base_url().'masuk'; ?>" class="btn btn-outline-success col-md-4" style="margin-bottom: 20px;">MASUK</a>

				</form>
			</div>
			<div class="col-md-6 bg-gambar">
				<img src="<?php echo base_url().'assets/daftar.svg' ?>" alt="true" class="img-responsive img-fluid" style="height: 400px; margin-top: 90px;">
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$("#form_daftar").submit(function(event){

			if($('#email').val() == ''){
				$('#email').addClass('is-invalid');
				return false;
			}else{
				$('#email').removeClass('is-invalid');
			}

			if($('#nama').val() == ''){
				$('#nama').addClass('is-invalid');
				return false;
			}else{
				$('#nama').removeClass('is-invalid');
			}

			if($('#hp').val() == ''){
				$('#hp').addClass('is-invalid');
				return false;
			}else{
				$('#hp').removeClass('is-invalid');
			}

			if($('#password').val() == ''){
				$('#password').addClass('is-invalid');
				return false;
			}else{
				$('#password').removeClass('is-invalid');
			}


			$('.btn-success').removeClass('btn-success');
			$('.btn-success').addClass('btn-secondary');
			$('.btn-success').prop('disabled', true).text('Loading..');
			event.preventDefault();
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				$('.btn-success').removeClass('btn-secondary');
					$('.btn-success').addClass('btn-success');
					$('.btn-success').removeAttr("disabled").text('Tambah');
				$('.form-control').val('');

				if(response == 1){
					swal("Berhasil!", "Akun berhasil di tambahkan!", "success");
				}else{
					swal("Gagal!", response , "error");
				}

			});
		});
	</script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>

<style type="text/css">
.container-fluid{
		padding-left: 80px;
		padding-right: 80px;
	}
	.card-dekstop{
		display: block;
	}


	@media only screen and (max-width: 600px) {
		.container-fluid{
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 83px;
		padding-bottom: 60px;
	}

		.row{
		margin-right: 0px;
		margin-left: 0px;
		padding: 5px;
	}
	.card-body{
		padding: 8px;
	}
	.card-dekstop{
		display: none;
	}
	}
</style>

<div class="container-fluid">
	<div class="row" >
		<div class="col-md-3 card-dekstop">
			<div class="row">
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;border: 1px solid green;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-user" style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Akun Saya</p>
				</div>
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-shopping-bag" aria-hidden="true"style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Pesanan Saya</p>
				</div>
				
			</div>
		</div>

		<div class="col-md-9">
			<div class="card">
				<div class="card-body">
					<p style="font-weight: 700;font-size: 20px;line-height: 25px;margin-bottom: 20px;">Akun Saya</p>
					<?php 
						$id_user = $this->session->userdata('id_user');
						$row = $this->db->get_where('users', array('id' => $id_user))->row_array();
					 ?>
					<div class="card">
						<div class="card-body">
							<p class="text-comp text-grey" style="margin-bottom: 0px;">Nama</p>
							<p class="text-comp" style="margin-bottom: 25px;"><b><?php echo $row['nama']; ?></b></p>

							<p class="text-comp text-grey" style="margin-bottom: 0px;">Tanggal Lahir</p>
							<p class="text-comp" style="margin-bottom: 25px;"><b><?php echo $row['tgl_lahir']; ?></b></p>

							<p class="text-comp text-grey" style="margin-bottom: 0px;">Jenis Kelamin</p>
							<p class="text-comp" style="margin-bottom: 25px;"><b><?php echo $row['jenis_kelamin']; ?></b></p>

							<p class="text-comp text-grey" style="margin-bottom: 0px;">Email</p>
							<p class="text-comp" style="margin-bottom: 25px;"><b><?php echo $row['email']; ?></b></p>

							<p class="text-comp text-grey" style="margin-bottom: 0px;">Nomor Telephone</p>
							<p class="text-comp" style="margin-bottom: 25px;"><b><?php echo $row['telephone']; ?></b></p>


							<a href="#" class="btn btn-success" data-toggle="modal" data-target="#editprofile">Ubah Profil</a>
							<a href="#" class="btn btn-outline-success" data-toggle="modal" data-target="#gantipass">Ganti Password</a>

						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="editprofile" tabindex="-1" role="dialog" aria-labelledby="editprofileLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="editprofileLabel">Profil</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?php echo base_url().'web/update_profile'; ?>" method="POST" id="form_profile">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" name="nama" value="<?php echo $row['nama']; ?>" class="form-control input-sm">
					</div>
					
					<div class="form-group">
						<label>Tanggal Lahir</label>
						<input type="date" name="tanggal_lahir" class="form-control input-sm" value="<?php echo $row['tgl_lahir']; ?>">
					</div>

					<div class="form-group">
						<label>Nomor Telephone</label>
						<input type="text" name="telephone" class="form-control input-sm" value="<?php echo $row['telephone']; ?>">
					</div>

					<div class="form-group">
						<label>Jenis Kelamin</label>

						<div class="custom-control custom-radio">
							<input type="radio" class="custom-control-input"  name="jenis_kelamin" value="L" <?php if($row['jenis_kelamin'] == 'L') { echo "checked"; } ?> >
							<label class="custom-control-label" for="defaultUnchecked">Pria</label>
						</div>

						<div class="custom-control custom-radio">
							<input type="radio" class="custom-control-input"  name="jenis_kelamin" value="P" <?php if($row['jenis_kelamin'] == 'P') { echo "checked"; } ?>>
							<label class="custom-control-label" for="defaultUnchecked">Wanita</label>
						</div>
					</div>

					<button type="submit" class="btn btn-success btn-block btn-sm" id="btn-profile">Simpan</button>
				</form>
			</div>

		</div>
	</div>
</div>



<div class="modal fade" id="gantipass" tabindex="-1" role="dialog" aria-labelledby="gantipassLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="gantipassLabel">Ubah Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form action="<?php echo base_url().'web/update_pass' ?>" method="post" id="form_pass">
      		
      
        <p class="text-comp">Masukkan password lama dan password baru yang Anda kehendaki</p>

        <div class="form-group">
        	<label>Password Lama</label>
        	<input type="password" name="password_lama" class="form-control input-sm">
        </div>

        <div class="form-group">
        	<label>Password Baru</label>
        	<input type="password" name="password_baru" class="form-control input-sm">
        </div>

        <button class="btn btn-success btn-sm btn-block" id="btn-pass" type="submit">Simpan</button>
        	</form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

	$("#form_profile").submit(function(event){
			event.preventDefault();
			$('#btn-profile').removeClass('btn-success');
			$('#btn-profile').addClass('btn-secondary');
			$('#btn-profile').prop('disabled', true).text('Loading..');
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				
				location.reload();
			});
		});


	$("#form_pass").submit(function(event){
			event.preventDefault();
			$('#btn-pass').removeClass('btn-success');
			$('#btn-pass').addClass('btn-secondary');
			$('#btn-pass').prop('disabled', true).text('Loading..');
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				
				if(response == 1){
					swal("Berhasil!", "Password Berhasil di ubah", "success");
					$('input[name="password_lama"]').val('');
					$('input[name="password_baru"]').val('');
					$('#gantipass').modal('hide');
				}else{
					swal("Gagal!", "Password lama anda salah", "warning");
					$('input[name="password_lama"]').val('');
					$('input[name="password_baru"]').val('');
				}

				$('#btn-pass').removeClass('btn-secondary');
				$('#btn-pass').addClass('btn-success');
				$('#btn-pass').removeAttr("disabled").text('Simpan');
			});
		});
</script>
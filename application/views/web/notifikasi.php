
<style type="text/css">
	.border-aktive{
	border-color: #28a745;
}
.container-fluid{
		padding-left: 80px;
		padding-right: 80px;
	}
	.card-dekstop{
		display: block;
	}


.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
	height: auto;
    overflow-y: auto;
}
.notif-aktif{
  background-color: #fff8cc;
}
.notif-read{
  background-color: #FFFFFF;
}


	@media only screen and (max-width: 600px) {
		.container-fluid{
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 83px;
		padding-bottom: 60px;
	}

		.row{
		margin-right: 0px;
		margin-left: 0px;
		padding: 5px;
	}
	.card-body{
		padding: 8px;
	}
	.card-dekstop{
		display: none;
	}
	}
</style>
<div class="container-fluid" style="min-height: 330px;">
	<div class="row" >
		<?php 
			$id_user = $this->session->userdata('id_user');
			$get = $this->db->get_where('notifikasi', array('id_user' => $id_user))->result_array();

			foreach ($get as $row) {
			
			if($row['baca'] == 1){
				$read = 'notif-read';
			}else{
				$read = 'notif-aktif';
			}
			
		 ?>
		<div class="col-12 <?php echo $read; ?>" style="border-bottom: 1px solid #d5d5d5;padding-top: 10px;">
			<p style="color: #333;margin:0px;font-size: 14px;font-weight: 500;"><?php echo $row['judul']; ?></p>
			<p style="color: #161616;font-size: 12px;"><?php echo $row['isi']; ?></p>
		</div>
	<?php } ?>
	</div>
</div>
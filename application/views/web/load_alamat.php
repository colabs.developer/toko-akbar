<?php 
      		if($this->session->userdata('id_user')){
            $id_user = $this->session->userdata('id_user');
            $sql = "SELECT a.*, b.nama as nama_prov, c.nama as nama_kota, d.nama as kecamatan from alamat_pengiriman a 
            		LEFT JOIN provinsi b ON a.id_provinsi=b.id_prov
            		LEFT JOIN kabupaten c ON a.id_kota=c.id_kab
            		LEFT JOIN kecamatan d ON a.id_kecamatan=d.id_kec
                        WHERE a.id_user='".$id_user."'
            		";
            $alamat = $this->db->query($sql)->result_array();
      		
      		foreach ($alamat as $row) {
      		?>	

      		<div class="card" style="margin-top: 15px;">
      			<div class="card-body" style="padding: 10px;">
      				<div class="row">
      					<div class="col-md-1" style="text-align: center;">
      						<i class="fa fa-map-marker"></i>
      					</div>
      					<div class="col-md-8">
      						<p class="text-comp" style="margin-bottom: 0px;"><b><?php echo $row['nama']; ?></b></p>
      						<p class="text-comp" style="margin-bottom: 0px;"><?php echo $row['alamat']." Provinsi ".$row['nama_prov'].", ".$row['nama_kota']." Kecamatan".$row['kecamatan']; ?></p>
      					</div>
      					<div class="col-md-3" style="text-align: right;padding-top: 15px;">
      						<button type="button" class="btn btn-primary btn-sm btn-pilih" key="<?php echo $row['id']; ?>">Pilih</button>
      					</div>
      				</div>
      			</div>
      		</div>

      		<?php
      }		
}
      		?>
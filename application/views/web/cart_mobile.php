<div class="col-md-4 " style="margin-top: 80px;margin-bottom: 90px;">
	<div class="card card-mobile">
		<div class="card-body">
			<p style="font-size: 14px;font-weight: 700;">Keranjang Belanja</p>
			<div class="loader-wrapper">
				<div class="loader-animation">
					<svg><path d="M442 79.1H0V65.5h412.4v-7.1H0V0h442v79.1zm0 7.1V107H181.2v-7.1H0V86.2h442zM50.1 24.6v7.2h53.3v-7.2H50.1zm0-16.8v7.1h89.3V7.8H50.1zM19.3 38.9c10.6 0 19.2-8.7 19.2-19.4C38.5 8.7 30 0 19.3 0A19.4 19.4 0 0 0 0 19.5c0 10.7 8.6 19.4 19.3 19.4z"/></svg>
				</div>
			</div>

			<form action="<?php echo base_url().'web/add_checkout' ?>" method="post" id="form_add_temp">
				
				
				<div class="row" id="load_keranjang" style="display: none;">
					
					<?php 
					$id_user = $this->session->userdata('id_user');
					$query_p = "SELECT a.*, b.nama_produk , d.nama_satuan from transaksi_temp_detail as a 
					LEFT JOIN produk as b ON a.id_produk = b.id 
					LEFT JOIN transaksi_temp as c ON a.id_transaksi = c.id
					LEFT JOIN satuan d ON b.id_satuan = d.id
					WHERE a.id_user='$id_user' AND c.status != '2' GROUP BY a.id_produk";
					$shop = $this->db->query($query_p);

					if($shop->num_rows() > 0){
						$shop = $shop->result_array(); 
						$grand_total = 0;

						foreach ($shop as $row) {
							$grand_total += $row['sub_total'];
							?>

							<div class="col-md-12" style="margin-bottom: 15px;">
								<div class="card">
									<div class="card-body" style="padding: 12px;padding-bottom: 0px;">

										<div class="row d-flex" style="margin: 0px;margin-bottom: 20px;">
											<div>
												<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo $row['nama_produk']; ?></p>
												<p style="color: #9295a6;font-size: 12px;margin-bottom: 0px;"><?php echo rupiah($row['harga']); ?> / <?php echo $row['nama_satuan']; ?></p>		
											</div>
											<div class="ml-auto">
												<button type="button" class="btn btn-default btn-sm delete-keranjang" 
												onclick="delete_keranjang(<?php echo $row['id']; ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></button>								
											</div>
										</div>

										<input type="hidden" name="id_produk[]" value="<?php echo $row['id_produk'] ?>">
										<input type="hidden" name="harga[]" value="<?php echo $row['harga'] ?>">

										<div class="row d-flex" style="margin: 0px;margin-bottom: 15px;">
											<div>
												<div class="input-group">
													<button type="button" class="button-minus btn btn-sm btn-danger" data-field="quantity">-</button>
													<input type="text" step="1" max="" min="1" value="<?php echo $row['qty']; ?>" name="quantity[]" class="quantity-field form-control input-sm" >
													<button type="button" class="button-plus btn btn-sm btn-success" data-field="quantity">+</button>
												</div>

											</div>
											<div class="ml-auto">
												<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo rupiah($row['sub_total']); ?></p>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						<?php } ?>

						

						<?php
					}else{
						?>
						<div style="padding: 50px 25px;text-align: center;">
							
							
							<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;height: 30px;">
							<p style="font-weight: 700;font-size: 12px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
							<p style="font-size: 12px;margin-bottom: 10px;">Temukan produk dengan harga yang kompetitif dan membayar harga yang adil bagi para petani.</p>

						</div>

						<?php
					}

					
					?>

				</div>

			</form>
			<div class="d-flex" style="margin-top: 15px; padding: 8px;">
				<div>
					<p style="margin-bottom: 0px;" class="hitung-grandtotal">Subtotal: <b><?php echo rupiah(@$grand_total); ?></b></p>
				</div>
				<div class="ml-auto">
					<button type="button" id="btn-checkout" class="btn btn-primary btn-sm <?php if(!isset($grand_total)){ echo "btn-secondary"; } ?>" <?php if(!isset($grand_total)){ echo "disabled"; } ?> >Checkout</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function rupiah(angka, prefix){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
			
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
			
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}


		function delete_keranjang(id){
			$('.loader-wrapper').css('display', 'block');
			$('#load_keranjang').css('display', 'none');
			
			$.ajax({
				url : "<?php echo base_url().'web/delete_tr_temp' ?>",
				type: "POST",
				data : {id:id}
			}).done(function(response){
				$('#load_keranjang').html(response);
				var sum = 0;
				$('.hitung-price').each(function() {
					sum += Number($(this).attr('key'));
				});

				$('.loader-wrapper').css('display', 'none');
				$('#load_keranjang').css('display', 'block');

				if(sum == 0){
					$('#btn-checkout').addClass('btn-secondary');
					$('#btn-checkout').removeClass('btn-primary');
					$('#btn-checkout').prop('disabled', true);
				}else{
					$('#btn-checkout').removeClass('btn-secondary');
					$('#btn-checkout').addClass('btn-primary');
					$('#btn-checkout').removeAttr("disabled");
				}


				$('.hitung-grandtotal').html("Subtotal: <b>"+rupiah(sum, 'Rp ')+"</b>");
			});
		}





		$('#btn-checkout').click(function(){
			$('#form_add_temp').submit();
		});

		$("#form_add_temp").submit(function(event){
			event.preventDefault();
			$('#btn-checkout').removeClass('btn-success');
			$('#btn-checkout').addClass('btn-secondary');
			$('#btn-checkout').prop('disabled', true).text('Loading..');
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				// if(response == 1){
					
					window.location = '<?php echo base_url().'checkout'; ?>';
				// }
			});
		});
	</script>
<!DOCTYPE html>
<html>
<head>
	<title>Cari kebuutuhan lebih mudah - Growcery</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

  <!-- 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/select2/select2-bootstrap4.min.css'; ?>">
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script> -->


  <style type="text/css">
    .card-img-top {
  display: block;
  background: #fff center center no-repeat;
  background-size: cover;
  filter: blur(3px); /* blur the lowres image */
}

.card-img-top > img {
  display: block;
  width: 100%;
  opacity: 0; /* visually hide the img element */
}

.card-img-top.is-loaded {
  filter: none; /* remove the blur on fullres image */
  transition: filter 1s;
}
   .text-footer{
    font-size: 12px;
    font-weight: 500;
    margin-bottom: 1rem;
  }
  .area-pengiriman{
    background: #f3f4fa;
    width: 268px;
    border-radius: 3px;
  }
  .text-comp{
    font-size: 12px;
  }
  .text-comp.is-primary {
    color: #23ab96;
  }
  .text-comp.is-grey {
    color: #9295a6;
}

  .payment-card:hover{
    border: 2px solid #FFEB3B;
    cursor: pointer;
  }


  .scroll-horizontal {
    overflow-x: scroll;
    overflow-y: hidden;
    display: block;
    width: 100%;
    padding-left: 8px;
  }
  .image-group {
    white-space: nowrap;
    width: auto;
  }
  .image-placeholer {
    display: inline-block;
    background: grey;
    width: 150px;
    height: 150px;
    background: url('https://storage.googleapis.com/p-iron-bank-storage-01/groups/580568929a2b5013bf730fbc0144e304');
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    margin-right: 14px;
  }

  .item-slik{
   width: 80px !important;
   height: 80px;
   margin-right: 15px;
 }
 .item-slik img{
   width: 100%;
   height: 80px !important;
 }
 .text-slik{
   font-size: 10px;
   margin-bottom: 0px;
 }
 .p5{
   padding: 5px;
 }
.mb0
{
  margin-bottom: 0px;
}
.mb5
{
  margin-bottom: 5px;
}
 .slick-next, .slick-prev {
  font-size: 0;
  line-height: 1;
  top: 50% !important;
  width: 32px !important;
  height: 118px !important;
  -webkit-transform: translate(0,-50%);
  -ms-transform: translate(0,-50%);
  transform: translate(0,-50%);
  cursor: pointer;
  color: transparent;
  border: none;
  outline: 0;
  background: 0 0;
  background-color: rgba(184,187,204,.7) !important;
  z-index: 10;
}

.slick-next:before {
    content: '\27E9' !important;
    font-weight: 900;
    font-size: 28px;
}

.slick-prev:before {
    content: '\27E8' !important;
    font-weight: 900;
    font-size: 28px;
}



.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  font-weight: normal;
  color: #000;
  font-size: 12px;
  font-weight: 700;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #FFF;
  border:2px solid #DDD;
  border-radius: 50px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  /*content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;*/

}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #000;
  font-size: 12px;
  font-weight: 700;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  /*content: '\2714';
  text-indent: .9em;
  color: #333;*/
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}



.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}


.btn-success{
  /*background: linear-gradient(105.53deg, #FFDD00 -31.14%, #FBB034 105.05%, #FFDD00 105.06%, rgba(255, 221, 0, 0) 105.08%) !important;*/
  background-color: #ffe030 !important;
  border-color: #FFDD00 !important;
}

.btn-outline-success{
  border-color: #ffe030 !important;
  color: #ffe030;
}
.btn-outline-success:hover{
  background-color: #ffe030 !important;
  border-color: #ffe030 !important;
  color: #FFF;
}

.btn-success.focus, .btn-success:focus{
  box-shadow: 0 0 0 0.2rem #ffe030;
}

.btn-order-mobile{
  display: block;
  width: 60px;
  height: 60px;
  border-radius: 50px;
  position: fixed;
  bottom: 0px;
  right: 0px;
  z-index: 3;
  margin-right: 20px;
  margin-bottom: 60px;
}

.footer-mobile{
  display: none;
}

.masuk-mobile{
  display: none;
}



.loader-wrapper {
  height: 300px;
  padding: 12px;
}

.loader-animation {
  background: #f5f6f7;
  height: 100%;
  width:100%;
  overflow: hidden;
  position: relative;
}

.loader-animation svg {
  position: absolute;
  fill:#fff;
  top:0;
  left:-1px;
  width:442px;
  height:107px;
}

.loader-animation::before {
  background-color: #f5f6f7;
  background-image: url(https://i.postimg.cc/VLC3WF9t/3pk-Fb-IT7-rn.gif);
  background-repeat: repeat-y;
  background-size: 100% 1px;
  content: ' ';
  display: block;
  height: 100%;
}



.loading {
  height: 300px;
  padding: 12px;
}

.loading-animation {
  background: #FFF;
  height: 100%;
  width:100%;
  overflow: hidden;
  position: relative;
}

.loading-animation svg {
  position: absolute;
  fill:#fff;
  top:0;
  left:-1px;
  width:442px;
  height:107px;
}

.loading-animation::before {
  background-color: #FFF;
  background-image: url(https://i.postimg.cc/VLC3WF9t/3pk-Fb-IT7-rn.gif);
  background-repeat: repeat-y;
  background-size: 100% 1px;
  content: ' ';
  display: block;
  height: 100%;
}

.radio-cabang{
  width: 100%;
}

.container-fluid{
    margin-top: 100px;
}

.form-mobile{
    display: none;
  }

  form-dekstop{
    display: block;
  }


@media only screen and (max-width: 600px) {
  .form-mobile{
    display: block;
  }

  .form-dekstop{
    display: none;
  }

  .masuk-mobile{
  display: block;
}

.container-fluid{
    margin-top: 0px;
}
.fixed-mobile {
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1030;
}
.jarak-header{
  margin-top: 80px;
}


.badge-order{
      position: absolute !important;
    top: 0px;
    background: red;
    color: #FFF;
}
.footer{
  display: none;
}

.footer-mobile{
  display: flex;
      background: #FFF;
    position: fixed;
    bottom: 0px;
    left: 0px;
    right: 0px;
    padding: 5px;
}
.foot a{
  color: #959595;
  text-decoration: none;
}

.p-foot{
  font-size: 12px;
  margin-bottom: 0px;
}

.icon-foot{
  font-size: 16px;
}

.foot-active a{
    color: #000;
  font-weight: bold;
  text-decoration: none;
}



}


</style>
</head>
<body 
<?php if(isset($body)){ ?> style="background-color: #f3f4fa;" <?php } ?>
>


<!--Navbar -->
<nav class="mb-1 navbar fixed-mobile fixed-top navbar-expand-lg navbar-light" style="height: 72px;background: linear-gradient(105.53deg, #FFDD00 -31.14%, #FBB034 105.05%, #FFDD00 105.06%, rgba(255, 221, 0, 0) 105.08%);background: #ffe030 !important;">
  <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="https://growcery.id/foto/144x144.png" style="height: 45px;width: auto;"></a>

   <form class="form-inline form-mobile" style="margin-left: -25px;">
    <input class="form-control cari-form" style="width: 160px;" type="search" placeholder="Cari buah, sayur.." aria-label="Search">
  </form>
   
  <?php if($this->session->userdata('id_user')){ ?>


  <div class="dropdown" style="margin-right: -23px;">
  <button class="navbar-toggler" type="button" id="dropdownakun" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 5px;font-size: 20px;border-color: #FFF;">
  <i class="fa fa-list" style="color: #FFF;"></i>
</button>
<div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="dropdownakun" style="left: -159px;">
   <a class="dropdown-item" href="<?php echo base_url().'account' ?>">
      <p style="margin-bottom: 0px;font-size: 12px;"><b><?php echo $this->session->userdata('nama'); ?></b></p>
      <p style="margin-bottom: 0px;font-size: 10px;"><?php echo $this->session->userdata('email'); ?></p>
    </a>
     <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="<?php echo base_url().'notifikasi'; ?>"><p style="margin-bottom: 0px;font-size: 12px;font-weight: bold;">Notifikasi</p></a>
     <div class="dropdown-divider"></div>
     <a class="dropdown-item" href="<?php echo base_url().'history'; ?>"><p style="margin-bottom: 0px;font-size: 12px;font-weight: bold;">Pesanan</p></a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="<?php echo base_url().'auth/keluar' ?>"><p style="margin-bottom: 0px;font-size: 12px;"><b>Keluar</b></p></a>
  </div>
</div>

      <a href="<?php echo base_url().'notifikasi'; ?>" style="color: #FFF;font-size: 20px;" class="form-mobile"><span class="fa fa-bell"></span></a>
   


<?php }else{ ?>
 <a class="nav-link btn btn-secondary masuk-mobile" 
    aria-haspopup="true" aria-expanded="false" style="font-size: 14px;color: #363636;background-color: #FFF;padding-left: 20px;padding-right: 20px;border-color: #FFF;" href="<?php echo base_url().'masuk' ?>">
    <i class="fa fa-sign-in" aria-hidden="true">&nbsp;</i> Masuk </a>
<?php } ?>

<div class="collapse navbar-collapse" id="navbarSupportedContent-333">
  <ul class="navbar-nav mr-auto">
    <li class="nav-item area-pengiriman" style="margin-left: 20px;">
      <a class="nav-link d-flex" href="#" style="font-size: 10px;" data-toggle="modal" data-target="#exampleModal"><div>Area Pengiriman <br> <b><?php echo $this->session->userdata('nama_cabang'); ?></b></div><div class="ml-auto"><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 22px;"></i></i></div></a>
    </li>
  </ul>

 
  <form class="form-inline form-dekstop" style="width: 100%;margin-left: 20px;">
    <input class="form-control cari-form" style="height: 44px;width: 750px;"  type="search" placeholder="Cari buah, sayur.." aria-label="Search">
  </form>

  <ul class="navbar-nav ml-auto nav-flex-icons">

      <?php if($this->session->userdata('id_user')){ ?>
   <li class="nav-item" style="margin-right: 15px;">
    <a class="nav-link btn btn-secondary " 
    aria-haspopup="true" aria-expanded="false" style="font-size: 14px;color: #363636;background-color: #FFF;padding-left: 20px;padding-right: 20px;border-color: #FFF;" href="<?php echo base_url().'history' ?>">
    <i class="fa fa-shopping-bag" aria-hidden="true">&nbsp;</i>  Pesanan Saya </a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link btn btn-secondary dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
    aria-haspopup="true" aria-expanded="false" style="font-size: 14px;color: #363636;background-color: #FFF;padding-left: 20px;padding-right: 20px;border-color: #FFF;">
    <i class="fa fa-user">&nbsp;</i>  <?php echo $this->session->userdata('nama'); ?> </a>


    <div class="dropdown-menu dropdown-menu-right dropdown-default"
    aria-labelledby="navbarDropdownMenuLink-333">
    <a class="dropdown-item" href="<?php echo base_url().'account' ?>">
      <p style="margin-bottom: 0px;font-size: 12px;"><b><?php echo $this->session->userdata('nama'); ?></b></p>
      <p style="margin-bottom: 0px;font-size: 10px;"><?php echo $this->session->userdata('email'); ?></p>
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="<?php echo base_url().'auth/keluar' ?>"><p style="margin-bottom: 0px;font-size: 12px;"><b>Keluar</b></p></a>
   
  </div>
</li>
<?php }else{ ?>
  <li class="nav-item" style="margin-right: 15px;">
    <a class="nav-link btn btn-secondary " 
    aria-haspopup="true" aria-expanded="false" style="font-size: 14px;color: #363636;background-color: #FFF;padding-left: 20px;padding-right: 20px;border-color: #FFF;" href="<?php echo base_url().'masuk' ?>">
    <i class="fa fa-sign-in" aria-hidden="true">&nbsp;</i> Masuk </a>
  </li>
<?php } ?>
</ul>

</div>
</nav>
<!--/.Navbar -->



<?php 
  if($id_user = $this->session->userdata('id_user')){


         $count = $this->db->query("SELECT SUM(qty) as total FROM transaksi_temp_detail a LEFT JOIN transaksi_temp b ON a.id_transaksi = b.id WHERE b.id_user = '".$id_user."' and b.status != '2' ");
    
    if($count->num_rows() > 0){
      $count = $count->row_array();
      $badge = '<span class="badge badge-light badge-order">'.$count['total'].'</span>';
    }else{
      $badge =  '';
    }

  }
 ?>

<style type="text/css">
.container-fluid{
		padding-right: 300px;
		padding-left: 300px;
		margin-top: 20px;
	}
	.card-dekstop{
		display: block;
	}


	@media only screen and (max-width: 600px) {
		.container-fluid{
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 83px;
		padding-bottom: 60px;
	}

		.row{
		margin-right: 0px;
		margin-left: 0px;
		padding: 5px;
	}
	.card-body{
		padding: 8px;
	}
	.card-dekstop{
		display: none;
	}
	}
</style>
<?php 


if($payment['grand_total'] >= 100000){
	$ongkir = 0;
}else{
	$ongkir = 20000; 
}
 ?>



<div class="container-fluid" >
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div style="width:auto;text-align: right;padding-top: 5px;padding-right: 0px;padding-left: 15px;">
						<i class="fa fa-shopping-bag" aria-hidden="true" style="font-size: 25px;color: #23ab96;">&nbsp;</i> 
					</div>
					<div class="col-md-10" style="padding-left: 10px;">
						<p class="text-comp is-primary"><b>Segera selesaikan pembayaran anda <br> sebelum hasil habis</b></p>
					</div>

					<div class="area-pengiriman" style="width: 100%;padding: 5px 10px;margin-left: 15px;margin-right: 15px;margin-bottom: 15px;">
						<p class="text-comp is-grey" style="margin-bottom: 0px;">Batas waktu pembayaran tagihan</p>
						<p style="font-size: 14px;font-weight: 700;margin-bottom: 0px;"><?php
						   $date = $payment['tgl_aja'];
                        $date1 = str_replace('-', '/', $date);
                        $tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));

                        echo date_indo($tomorrow).' Pukul 16:00:00'; ?></p>
					</div>
					</div>
					
					

					<div style="margin-bottom: 15px;border-bottom: 1px solid #DDD;padding-bottom: 15px;">
						<p class="text-comp is-grey" style="margin-bottom: 0px;">Jumlah Tagihan</p>
						<p style="font-size: 14px;font-weight: 700;margin-bottom: 0px;"><?php echo rupiah($payment['pembayaran']); ?></p>

					</div>


					<div style="margin-bottom: 15px;border-bottom: 1px solid #DDD;padding-bottom: 15px;">
						<p class="text-comp is-grey" style="margin-bottom: 0px;">Nomor Pesanan</p>
						<p style="font-size: 14px;font-weight: 700;margin-bottom: 0px;"><?php echo $payment['kode_transaksi']; ?></p>

					</div>

					<div style="margin-bottom: 0px;">
						<p class="text-comp is-grey" style="margin-bottom: 0px;">Bank Pembayaran</p>

						<div class="row">
							<div class="col-md-1" style="padding-right: 0px;">
									<img src="<?php echo 'https://growcery.id/foto/bank/'.$payment['logo']; ?>" class="img-fluid">
							</div>
							<div class="col-md-10">
								<p style="font-size: 14px;font-weight: 700;margin-bottom: 0px;"><?php echo $payment['nama_bank']; ?></p>
								<p class="text-comp"><?php echo $payment['no_rekening']; ?></p>
							</div>
						</div>
					
						

					</div>

					<div style="width: 100%;margin-right: 15px;padding: 8px;">
						<div id="accordion" style="width: 100%;">
  <div class="card">
    <div class="card-header" id="headingOne" style="padding: 8px;">
      <h6 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color: #333;font-weight: 700;text-decoration: none;">
          ATM <?php echo $payment['nama_bank']; ?>
        </button>
      </h6>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
       <ol>
       	<li>Masukkan Kartu ATM <?php echo $payment['nama_bank']; ?> Anda.</li>
		<li>Masukan PIN ATM Anda.</li>
		<li>Pilih Menu Transaksi Lainnya.</li>
		<li>Pilih menu Transfer dan Ke Rek <?php echo $payment['nama_bank']; ?>.</li>
		<li>Masukkan no rekening <?php echo $payment['nama_bank']; ?> yang dituju.</li>
		<li>Masukkan Nominal Jumlah Uang yang akan di transfer.</li>
		<li>Layar ATM akan menampilkan data transaksi anda </li>
       </ol>
      </div>
    </div>
  </div>
 
  <div class="card">
    <div class="card-header" id="headingThree" style="padding: 8px;">
      <h6 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color: #333;font-weight: 700;text-decoration: none;">
          M-<?php echo $payment['nama_bank']; ?>
        </button>
      </h6>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <ol>
        	<li>Pilih menu "m-<?php echo $payment['nama_bank']; ?>" di ponsel Anda dan tekan OK/YES</li>
<li>Pilih menu "m-Transfer" dan tekan OK/YES</li>
<li>Pilih “Antar Bank” dan tekan OK/YES</li>
<li>Masukkan "Kode Bank" tujuan dan tekan OK/YES</li>
<li>Masukkan nomor rekening tujuan dan tekan OK/YES</li>
<li>Masukkan Jumlah Uang yang akan ditransfer dan tekan OK/YES</li>
<li>Masukkan PIN m-<?php echo $payment['nama_bank']; ?> Anda dan tekan OK/YES</li>
<li>Pastikan data-data transaksi yang muncul di layar konfirmasi telah benar. Jika benar, tekan OK/YES dan lanjutkan ke no. 9. Jika tidak, diamkan selama beberapa saat</li>
<li>sampai message tersebut hilang dan otomatis transaksi tersebut akan dibatalkan.</li>
<li>Masukkan PIN m-<?php echo $payment['nama_bank']; ?> Anda dan tekan OK/YES</li>
<li>Setelah beberapa saat, Anda akan menerima message yang berisi informasi transaksi transfer Anda Berhasil atau Gagal</li>
        </ol>
      </div>
    </div>
  </div>
</div>
					</div>

					<input type="hidden" name="id_transaksi" id="id_transaksi" value="<?php echo $this->uri->segment(3); ?>">
					<div style="margin-left: 10px;margin-right: 10px;">
						<button class="btn btn-success btn-sm btn-block btn-konfirmasi" type="button">Konfirmasi Pembayaran</button>
					</div>


					
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$('.btn-konfirmasi').click(function(){
		$(this).removeClass('btn-success');
		$(this).addClass('btn-secondary');
		$(this).prop('disabled', true).text('Loading..');
		var id_transaksi = $('#id_transaksi').val();

			$.ajax({
				url : "<?php echo base_url().'web/move_transaksi'; ?>",
				type: "POST",
				data: {id_transaksi:id_transaksi},
			}).done(function(response){
				window.location = "<?php echo base_url().'history' ?>"
			});
	});
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Masuk Akun Growcery - Growcery</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	<style type="text/css">
		.bg-abu {
			padding-top: 160px;
			padding-left: 160px!important;
			padding-right: 160px!important;
			padding-bottom: 270px;
			background: #f5f5f5;
		}
		.bg-gambar {
			padding: 100px!important;
			padding-top: 160px;
		}

.text-line {
  margin-top: 15px;
  text-align: center;
  font-size: 12px;
  color: #8a8a8a;
}
.text-line.background {
  position: relative;
  z-index: 1;
}
.text-line.background:before {
  border-top: 1px solid #969696;
  content: "";
  margin: 0 auto;
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  bottom: 0;
  width: 95%;
  z-index: -1;
}
.text-line.background span {
  background: #f5f5f5;
  padding: 0 15px;
}

.btn-success{
  background-color: #f4d03f !important;
  border-color: #f4d03f !important;
}

.btn-outline-success{
  border-color: #f4d03f !important;
  color: #f4d03f;
}
.btn-outline-success:hover{
	background-color: #f4d03f !important;
  border-color: #f4d03f !important;
  color: #FFF;
}

.btn-success.focus, .btn-success:focus{
  box-shadow: 0 0 0 0.2rem #f4d03f;
}

@media only screen and (max-width: 600px) {
    .bg-abu{
        padding-top:100px !important;
           padding-bottom:20px;
           padding-right: 15px !important;
           padding-left: 15px !important;
           background:#FFF;
    }
    .bg-gambar{
        display: none;
        padding: 0px;
    }
    .text-line.background span {
  		background: #FFF;
	}
}

	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-12 bg-abu">
			<form action="<?php echo base_url().'auth/do_login'; ?>" method="post" id="form_login">
				<div class="form-group">
						<label>Email</label>
						<input type="text" placeholder="Your Email" class="form-control" name="email" id="email">
						<div class="invalid-feedback">
				          Email harus diisi.
				        </div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" placeholder="Your Password" class="form-control" name="password" id="password">
						<div class="invalid-feedback">
				          Email harus diisi.
				        </div>
						<p class="lost-password" style="font-size: 12px;margin-top: 10px;">Lupa password ?</p>
					</div>
					<button type="submit"  class="btn btn-success col-md-4" style="margin-bottom: 20px;"> MASUK</button>

					<a href="<?php echo base_url().'daftar'; ?>" class="btn btn-outline-success col-md-4" style="margin-bottom: 20px;">DAFTAR</a>
					
					<p class="text-line background"><span>Bisa juga menggunakan</span></p>

					<!-- <a href="<?php echo $loginURL; ?>">Login Google</a> -->
					<button type="button" onclick="googleSignIn()" class="btn btn-primary btn-block"><i class="fa fa-google" aria-hidden="true">&nbsp;</i> Masuk Google</button>
			</form>
		</div>
		<div class="col-md-6 bg-gambar">
				<img src="<?php echo base_url().'assets/login.svg' ?>" alt="true" class="img-responsive img-fluid" style="height: 400px; margin-top: 90px;">
			</div>
	</div>
</div>

	<script type="text/javascript">
		$("#form_login").submit(function(event){

			if($('#email').val() == ''){
				$('#email').addClass('is-invalid');
				return false;
			}else{
				$('#email').removeClass('is-invalid');
			}

			if($('#password').val() == ''){
				$('#password').addClass('is-invalid');
				return false;
			}else{
				$('#password').removeClass('is-invalid');
			}

			event.preventDefault();
			$('.btn-success').removeClass('btn-success');
			$('.btn-success').addClass('btn-secondary');
			$('.btn-success').prop('disabled', true).text('Loading..');
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				if(response == 1){
					$('.btn-success').removeClass('btn-secondary');
					$('.btn-success').addClass('btn-success');
					$('.btn-success').removeAttr("disabled").text('LOGIN');
					window.location = '<?php echo base_url(); ?>';
				}else{
					$('.form-control').val('');
					$('.btn-success').removeClass('btn-secondary');
					$('.btn-success').addClass('btn-success');
					$('.btn-success').removeAttr("disabled").text('LOGIN');
					swal("Gagal!", "Email atau password salah!", "error");
				}
			});
		});
	</script>

	 <script src="https://www.gstatic.com/firebasejs/7.14.4/firebase-app.js"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src="https://www.gstatic.com/firebasejs/7.14.4/firebase-analytics.js"></script>

  <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/7.14.4/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.14.4/firebase-firestore.js"></script>
   <script>
    // TODO: Replace the following with your app's Firebase project configuration
  var firebaseConfig = {
  apiKey: "AIzaSyAKaO11Tp1ILOCdhJZepSAi8m3wwu-JkCM",
  authDomain: "grocery-275303.firebaseapp.com",
  databaseURL: "https://project-id.firebaseio.com",
  projectId: "grocery-275303",
  storageBucket: "grocery-275303.appspot.com",
  messagingSenderId: "263409002809"
};

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);


    googleSignIn=()=> {
    	base_provider = new firebase.auth.GoogleAuthProvider();
    	firebase.auth().signInWithPopup(base_provider).then(function(result){
    		
    		var email = result.user.email;
    		var nama = result.user.displayName;
    		var emailVerified = result.user.emailVerified;

    		$.ajax({
				url : "<?php echo base_url().'auth/login_google' ?>",
				type: "POST",
				data : {email:email, nama:nama, emailVerified:emailVerified}
			}).done(function(response){
				if(response == 1){
					window.location = '<?php echo base_url(); ?>';
				}
			});


    	}).catch(function(err){
    		console.log(err);
    	})
    }
  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
	
</html>
	







 <style type="text/css">
       .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }
  .bill-body{
        padding: 25px;
    padding-top: 20px;
    background: #fff;
    box-shadow: 0 6px 12px 0 rgba(0,0,0,.07);

  }

  .bill-body table{
        font-size: 14px;
    color: #6b777e;
  }

  .bill-body table td{
       padding-bottom: 10px;
  }
 </style>
 <div class="main-panel">
  <div class="content-wrapper">


<div class="row">

    <div class="col-lg-12 grid-margin stretch-card">

    <div class="card">
      <div class="card-body">
        <form action="" method="POST">
          <div class="row">
            <div class="col-md-4">
              <label>Tanggal Awal</label>
              <input type="date" name="tgl_awal" class="form-control input-sm">
            </div>
             <div class="col-md-4">
              <label>Tanggal Akhir</label>
              <input type="date" name="tgl_akhir" class="form-control input-sm">
            </div>
            <div class="col-md-4">

              <button type="submit" class="btn btn-info btn-sm" style="margin-top: 35px;"><i class="mdi mdi-search-web"></i> Cari</button>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
  
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col-lg-6">
           <h4 class="card-title">Arus Kas</h4>
         </div>
         <div class="col-lg-6" style="text-align: right;">
          
        </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th> Tanggal </th>
              <th> Status </th>
              <th> Keterangan </th>
              <th> Total </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($kas as $row) {

              if($row['status'] == 'out'){
                $text= 'text-danger';
              }else{
                $text = 'text-success';
              }
              ?>
              <tr style="cursor: pointer;" class="action-bill">
                <td><?php echo date('d-m-Y H:i:s' , strtotime($row['tgl'])); ?></td>
               
                <td class="<?php echo $text; ?>"> <?php echo $row['status']; ?></td>
                 <td> <?php echo $row['keterangan']; ?></td>
                <td class="<?php echo $text; ?>"> <?php echo format_decimals($row['total']); ?> </td>
              </tr>
              <?php
            }
            ?>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>

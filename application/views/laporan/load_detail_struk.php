<?php 
if($id_penjualan != ''){
 $penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
  LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
  LEFT JOIN produk c ON b.id_produk = c.id 
  WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
 $penjualan = $this->db->query("SELECT * FROM penjualan WHERE id=".$id_penjualan)->row_array();

 $setting = $this->db->get('setting')->row_array();

 ?>

  <div class="col-md-8" style="background-color: #000000b5;">

  </div>
  <div class="col-md-4" style="background-color: #f3f3f3;">
    <div style="padding-top: 15px;"><div style="text-align: right;font-size: 16px;cursor: pointer;" class="close-bill">X</div></div>
    <div class="bill-body">
      <div style="font-size: 20px;
      color: #6b777e;
      font-weight: 700;
      text-align: center;"><?php echo $setting['nama']; ?></div>
      <p style="text-align: center;color: #6b777e;margin-bottom: 0px;"><?php echo $setting['alamat']; ?></p>

      <div style="border-bottom: 2px dashed #aaa;
      margin: 20px 0;"></div>

      <table style="width: 100%;">
        <?php foreach ($penjualan_detail as $row) { ?>
          <tr>
            <td><?php echo $row['nama']; ?></td>
            <td>x <?php echo $row['qty']; ?></td>
            <td style="text-align: right;"><?php echo decimals($row['total']); ?></td>
          </tr>
        <?php } ?>
      </table>

      <div style="border-bottom: 2px dashed #aaa;
      margin: 20px 0;"></div>

      <table style="width: 100%;">
        <tr>
          <td>Sub Total</td>
          
          <td style="text-align: right;"><?php echo decimals($penjualan['sub_total']); ?></td>
        </tr>
        <tr>
          <td>Total</td>
          
          <td style="text-align: right;"><?php echo decimals((int)$penjualan['sub_total'] + (int)$penjualan['ongkir']); ?></td>
        </tr>
      </table>

      <div style="border-bottom: 2px dashed #aaa;
      margin: 20px 0;"></div>

      <table style="width: 100%;">
        <tr>
          <td>Total Pembayaran</td>
          
          <td style="text-align: right;"><?php echo decimals($penjualan['bayar']); ?></td>
        </tr>
        <tr>
          <td>Kembalian</td>

          <td style="text-align: right;"><?php echo decimals($penjualan['kembali']); ?></td>
        </tr>
      </table>


    </div>
    <br>
    <div class="row">
      <div class="col-lg-12" style="text-align: right;">
       <a class="btn btn-info btn-sm print-struk" style="color: #FFF;" key="<?php echo $id_penjualan; ?>"><i class="mdi mdi-printer"></i> Print</a>
     </div>
   </div>
   

 </div>
<?php } ?>
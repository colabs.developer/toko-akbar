 <style type="text/css">
       .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }
  .bill-body{
        padding: 25px;
    padding-top: 20px;
    background: #fff;
    box-shadow: 0 6px 12px 0 rgba(0,0,0,.07);

  }

  .bill-body table{
        font-size: 14px;
    color: #6b777e;
  }

  .bill-body table td{
       padding-bottom: 10px;
  }
 </style>
 <div class="main-panel">
  <div class="content-wrapper">


<div class="row">
  
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col-lg-6">
           <h4 class="card-title">Stok</h4>
         </div>
         <div class="col-lg-6" style="text-align: right;">
          
        </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover datatables">
          <thead>
            <tr>
              <th> Produk </th>
              <th> Kategori </th>
              <th> Barcode </th>
              <th> Stok </th>
              <th> Nilai Stok (Rp) </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($stok as $row) {
              
              $sisa_stok = $row['masuk'] - $row['keluar'];
              $harga = $sisa_stok * $row['harga'];

              ?>
              <tr style="cursor: pointer;" class="action-bill">
                <td> <?php echo $row['nama']; ?> </td>
                <td> <?php echo $row['nama_kategori']; ?></td>
                 <td> <?php echo $row['kode']; ?></td>
                 <td> <?php echo $sisa_stok; ?></td>
                <td> <?php echo decimals($harga); ?> </td>
              </tr>
              <?php
            }
            ?>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
     $('.datatables').DataTable();
  });
</script>

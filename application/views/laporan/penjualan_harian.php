
<style type="text/css">
 .card-active{
  border-bottom: 3px solid #308ee0;
}
.icon-blue{
  color: #308ee0;
}
.icon-grey{
  color: #DDD;
}
.color-grey{
  color: #DDD;
}
.bill-body{
  padding: 25px;
  padding-top: 20px;
  background: #fff;
  box-shadow: 0 6px 12px 0 rgba(0,0,0,.07);

}

.bill-body table{
  font-size: 14px;
  color: #6b777e;
}

.bill-body table td{
 padding-bottom: 10px;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css">

<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>

<div class="main-panel">
  <div class="content-wrapper">


    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">

        <div class="card">
          <div class="card-body">
            <form action="" method="POST">
              <div class="row">
                <div class="col-md-3">
                  <label> Date</label>
                  <input type="date" name="tgl_awal" class="form-control input-sm tgl_awal " value="<?php if(isset($tgl_awal)){ echo $tgl_awal; } ?>" >
                  <div class="invalid-feedback">
                    Please choose a date.
                  </div>
                </div>
                <?php if($_SESSION['level'] == 1){ ?>
                  <div class="col-md-3">
                    <label>Sales</label>
                    <select class="form-control input-sm" name="id_user">
                      <option value="">-- Sales --</option>
                      <?php foreach ($user as $row) {
                        ?>
                        <option value="<?php echo $row['id']; ?>" <?php if(isset($id_user)){ if($row['id'] == $id_user){ echo "selected"; } } ?>><?php echo $row['nama']; ?></option>
                        <?php
                      } ?>
                    </select>
                  </div>
                <?php } ?>
                <div class="col-md-3">
                 <label>Status</label>
                 <select class="form-control input-sm" name="status">
                  <option value="">-- Status --</option>
                  
                  <option value="0" <?php if(isset($status)){ if($status == 0){ echo "selected"; } } ?>>Offline</option>
                  <option value="1" <?php if(isset($status)){ if($status == 1){ echo "selected"; } } ?>>Online</option>
                  
                </select>
              </div>
              <div class="col-md-3">
                <button type="submit" class="btn btn-info btn-sm" style="margin-top: 35px;"><i class="mdi mdi-search-web"></i> Find</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>


    <div class="col-lg-12 grid-margin stretch-card">

      <div class="card">
        <div class="card-body">

          <div class="row">
            <div class="col-lg-6">
             <h4 class="card-title">Sales Transaction</h4>
           </div>
           <div class="col-lg-6" style="text-align: right;">
            <button type="button" class="btn btn-primary print-daily"><i class="mdi mdi-printer"></i> Daily Sales</button>
          </div>
        </div>


        <div class="table-responsive">
          <table class="table table-hover datatables">
            <thead>
              <tr>
                <th> Date </th>
                <th> Product </th>
                <th> Total (Rp) </th>
                <th> Customer </th>
                <th> Status </th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $total = 0;
              foreach ($penjualan as $row) {

                $total += $row['sub_total'];

                if($row['status_penjualan'] == 1){
                  $status = '<span class="badge badge-success">Online</span>';
                }else{
                  $status = '<span class="badge badge-danger">Offline</span>';
                }

                ?>
                <tr>
                  <td> <?php echo $row['tgl']; ?> </td>
                  <td> <?php echo $row['produk']; ?></td>
                  <td> <?php echo decimals_excel($row['sub_total']); ?> </td>
                  <td> <?php echo $row['nama_customer']; ?></td>
                  <td> <?php  echo $status; ?></td>

                </td>
              </tr>
              <?php
            }
            ?>


          </tbody>
          <tfoot>
            <td></td>
            <td>Total :</td>
            <td style="text-align: right;font-weight: bold;"><?php echo decimals_excel($total);  ?></td>
            <td colspan="2"></td>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>



<div class="row div-bill" style="position: fixed;height: 100%;width: 101%;top: 0;right: 0;left: 0;bottom: 0;z-index: 9999;display: none;"></div>

<script type="text/javascript">
 $('.action-bill').click(function(){
  var id_penjualan = $(this).attr('key');
  location.href = "<?php echo base_url().'laporan/transaksi_penjualan/' ?>"+id_penjualan;

});

 var tables = $('.datatables').DataTable( {
   order: [[ 0, "desc" ]],
   dom: 'Bfrtip',
   lengthChange: false,
   buttons: [
   {
    extend: 'excelHtml5',
    exportOptions: {
      columns: [ 0, 1, 2, 3, 4 ]
    }
  },
  {
    extend: 'pdfHtml5',
    exportOptions: {
      columns: [ 0, 1, 2, 3, 4 ]
    }
  }
  ]
        //buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
      } );
 
 tables.button().container().appendTo( '.datatables .col-md-6:eq(0)' );

 $(document).ready(function(){
  <?php if($id_penjualan != ''){ ?>
    $('.div-bill').show();
  <?php } ?>

  //$('.datatables').DataTable();

  

  $('.datathhhables').DataTable( {
        // order: [[ 0, "desc" ]],
      } );

  $('.print-struk').click(function(){
    var id_penjualan = $(this).attr('key');

    window.open('<?php echo base_url().'kasir/print/' ?>'+id_penjualan, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');

  });

  $('.btn-recipt').click(function(){
    var id_penjualan = $(this).attr('key');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'laporan/get_detail_struk'; ?>",
      data: {id_penjualan:id_penjualan},
      dataType: 'html',
      success: function(data) {
       $('.div-bill').html(data).show();

       $('.close-bill').click(function(){
        $('.div-bill').hide();
      });

       $('.print-struk').click(function(){
        var id_penjualan = $(this).attr('key');

        window.open('<?php echo base_url().'kasir/print/' ?>'+id_penjualan, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');

      });
       
     }
   });

    
  });
});

 $('.close-bill').click(function(){
  $('.div-bill').hide();
});

 $('.print-daily').click(function(){
  var tgl_awal = $('.tgl_awal').val();
  if(tgl_awal == ''){
    $('.tgl_awal').addClass('is-invalid');
    return false;
  }

    $('.tgl_awal').removeClass('is-invalid');
    window.open('<?php echo base_url().'laporan/print/' ?>'+tgl_awal, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');


});


</script>
 <style type="text/css">
   .select2-container .select2-selection--single{
    height: 35px;
    margin-top: -5px;
  }

  .select2-container--bootstrap .select2-selection {
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    background-color: #fff;
    border: 1px solid #f2f2f2;
    border-radius: 2px !important;
    color: #555;
    font-size: 14px;
    outline: 0;
    font-size: 0.75rem;
    padding: 0.56rem 0.75rem !important;
    line-height: 14px !important;
    font-weight: 300;
  }
</style>


<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form method="post" class="formbarang">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Produk Baru</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw btn-submit">Simpan</button>
             </div>
           </div>

           <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Gambar Produk</label>
                <input type="file" class="form-control" name="files[]" multiple=""/>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Produk<span class="text-danger" title="Reuired">*</span> </label>
                <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?php if(isset($id)){ echo $barang['nama']; } ?>" required>
              </div>
                  <div class="form-group">
                <label for="exampleInputEmail1">Kode Produk<span class="text-danger" title="Reuired">*</span> </label>
                 <select class="form-control" id="kode_produk" name="kode_produk"  required>
                  <option value="">Pilih Kode</option>
                  <?php
                  $get= $this->db->get('kode_produk')->result_array();
                  foreach ($get as $row) {
                    ?>
                    <option value="<?php echo $row['id'] ?>" <?php if(isset($id)){ if($row['id'] == $barang['kode_produk']){  echo "selected"; } } ?>><?php echo $row['nama_kode'] ?></option>
                    <?php
                  }
                  ?>

                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Kategori <span class="text-danger" title="Reuired">*</span></label>
                <select class="form-control" id="id_kategori" name="id_kategori" onchange="get_jenis($(this).val());" required>
                  <option value="">Pilih Kategori</option>
                  <?php
                  $get= $this->db->get_where('kategori', array('status' => 1))->result_array();
                  foreach ($get as $row) {
                    ?>
                    <option value="<?php echo $row['id_kategori'] ?>" <?php if(isset($id)){ if($row['id_kategori'] == $barang['id_kategori']){  echo "selected"; } } ?>><?php echo $row['nama_kategori'] ?></option>
                    <?php
                  }
                  ?>

                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Jenis <span class="text-danger" title="Reuired">*</span></label>
                <select class="form-control" id="id_jenis" name="id_jenis" required>


                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Merek <span class="text-danger" title="Reuired">*</span></label>
                <select class="form-control" id="id_merek" name="id_merek" required>
                 <option value="">Pilih Merek</option>
                 <?php
                 $get= $this->db->get_where('data_merek', array('status' => 1))->result_array();
                 foreach ($get as $row) {
                  ?>
                  <option value="<?php echo $row['id'] ?>" <?php if(isset($id)){ if($row['id'] == $barang['id_merek']){  echo "selected"; } } ?>><?php echo $row['nama_merek'] ?></option>
                  <?php
                }
                ?>

              </select>
            </div>
          </div>
          <div class="col-lg-6">

            <?php if(isset($id)){
              ?>
              <input type="hidden" name="id_barang" value="<?php echo $id; ?>">
              <?php
            } ?>


            <div class="form-group">
              <label for="exampleInputEmail1">Satuan Stok <span class="text-danger" title="Reuired">*</span></label>
              <select class="form-control" id="satuan" name="satuan" required>
                <?php foreach ($unit as $row) { ?>
                  <option value="<?php echo $row['id'] ?>" <?php if(isset($id)){ if($row['id'] == $barang['satuan']){  echo "selected"; } } ?>><?php echo $row['nama_satuan']; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Supplier <span class="text-danger" title="Reuired">*</span></label>

              <select class="form-control col-lg-12 supplier" name="supplier" required>
                <option value="">-- Supplier --</option>
                <?php 
                $get = $this->db->get('supplier')->result_array();
                foreach ($get as $row) {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['nama'] ?></option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Keterangan</label>
              <textarea name="keterangan" id="keterangan" class="form-control"><?php if(isset($id)){ echo $barang['ket']; } ?></textarea>
            </div>
          </div>


          <div class="col-lg-6">

           <div class="form-group">
            <label>Notif Stok Min</label>
            <input type="number" name="stok_alert" class="form-control" value="<?php if(isset($id)){ echo $barang['stok_alert']; } ?>">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Barcode <span class="text-danger" title="Reuired">*</span></label>
            <input type="text" class="form-control" id="barcode" name="barcode" placeholder="" value="<?php if(isset($id)){ echo $barang['kode']; } ?>" required>
          </div>

          <div class="form-group">
            <label> Display <span class="text-danger" title="Reuired">*</span></label><br>
            <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="Inactive" id="toggle-status" data-onstyle="success">
          </div>

        </div>

        <div class="col-lg-12" style="    background: #3d3d3d;
        color: #ffffff;
        color: #ece4e0;
        text-align: center;
        border-radius: 5px;
        padding: 7px 0px;
        margin-bottom: 20px;"><p style="margin:0px;">Harga</p></div>

        <div class="col-lg-6">


          <div class="form-group">
            <label>Harga Beli <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" name="harga_beli" class="form-control" value="<?php if(isset($id)){ echo $barang['harga_beli']; } ?>" required>
          </div>


          <div class="form-group">
            <label for="exampleInputEmail1">Harga Jual <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga1"  value="<?php if(isset($id)){ echo $barang['harga']; } ?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Harga Konsi<span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga3" value="<?php if(isset($id)){ echo $barang['harga_3_satuan']; } ?>" required>
          </div>

        </div>




      </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">

  <?php if(isset($id)){ 
    if($barang['status'] == 1){
      ?>
      $('#toggle-status').bootstrapToggle('on');
      <?php
    }
  }
  ?>

  function get_jenis(id){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/get_jenis/'; ?>"+id,
    dataType: 'html',
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    success: function(data) {

      $('#id_jenis').html(data);

      <?php if(isset($id)){ ?>
       $('#id_jenis').val(<?=$barang['id_jenis']?>);
       
     <?php } ?>

   }
 });
 }

 $(document).ready(function(){




  $(".supplier").select2({
    theme: "bootstrap"
  });

  <?php if(isset($id)){ 
    if(isset($barang['id_supplier'])){
      ?>
      $('.supplier').val(<?php echo $barang['id_supplier']; ?>).trigger('change');
      <?php
    }
  } 
  ?>

  <?php if(isset($id)){ ?>
    get_jenis(<?=$barang['id_kategori']?>);
  <?php } ?>

  
  
  
});








 $(".formbarang").submit(function(event){
  event.preventDefault();

  var formData = new FormData(this);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_barang'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      if(data == 1)
      {
        window.location = '<?php echo base_url().'admin/barang'; ?>';
      }
    }
  });

});

</script>
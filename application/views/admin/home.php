<style type="text/css">
  .table-custom td {
    padding-left: 0px;
  }
</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<?php
$id_user = $this->session->userdata('userid');
$get = $this->db->query("SELECT (SUM(sub_total) - SUM(diskon)) as total, COUNT(id) as total_transaksi from penjualan ")->row_array();
$now = $this->db->query("SELECT (SUM(sub_total) - SUM(diskon)) as total, COUNT(id) as total_transaksi from penjualan WHERE DATE(tgl) = '".date('Y-m-d')."' ")->row_array();
$sales = $this->db->query("SELECT (SUM(a.sub_total) - SUM(a.diskon)) as total, b.nama from penjualan a LEFT JOIN admin b ON a.uid = b.id WHERE DATE(tgl) = '".date('Y-m-d')."' GROUP BY a.uid LIMIT 5 ")->result_array();

$bulan = $this->db->query("SELECT DATE(tgl) as tanggal,(SUM(sub_total) - SUM(diskon)) as total_transaksi FROM penjualan GROUP BY tanggal")->result_array();

$product = $this->db->query("SELECT COUNT(id) as total FROM produk ")->row_array();


$produk_best = $this->db->query("SELECT b.nama, SUM(a.qty) as total_qty FROM penjualan_detail a 
  LEFT JOIN produk b ON a.id_produk = b.id
  GROUP BY a.id_produk ORDER BY total_qty DESC LIMIT 5")->result_array();

$produk_stok = $this->db->query("SELECT b.nama , (SUM(a.masuk) - SUM(a.keluar)) as sisa_stok , b.stok_alert FROM produk_transaksi a 
  LEFT JOIN produk b ON a.id_produk = b.id
  GROUP BY a.id_produk LIMIT 5")->result_array();

$tanggale=array();
$month = date('m');
$year = date('Y');

for($d=1; $d<=31; $d++)
{
  $time=mktime(12, 0, 0, $month, $d, $year);          
  if (date('m', $time)==$month){       
    $tanggale[]=date('Y-m-d', $time);
  }

}

$hari = array();
$total = array();
$total_marketing = array();

foreach ($tanggale as $tgl) {
  $hari[] =date('d', strtotime($tgl));
  $total[] = 0;
  foreach ($bulan as $row) {

    if($tgl == date('Y-m-d',strtotime($row['tanggal']))){
      $hari[] = date('d', strtotime($tgl));
        $total[] = (int)$row['total_transaksi'];
      
      
      
    }
  }

}



?>

<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-cube text-danger icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Total Penjualan</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">Rp<?php echo decimals($now['total']); ?></h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0">
              <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> All Sale : Rp<?php echo decimals($get['total']); ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-poll-box text-success icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Total Transaksi</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0"><?php echo $now['total_transaksi']; ?></h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0">
              <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Total Transaksi : <?php echo $get['total_transaksi']; ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-receipt text-warning icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Total Produk</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0"><?php echo $product['total']; ?></h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Semua Produk
            </p>
          </div>
        </div>
      </div>
      <!--<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">-->
        <!--  <div class="card card-statistics">-->
          <!--    <div class="card-body">-->
            <!--      <div class="clearfix">-->
              <!--        <div class="float-left">-->
                <!--          <i class="mdi mdi-account-location text-info icon-lg"></i>-->
                <!--        </div>-->
                <!--        <div class="float-right">-->
                  <!--          <p class="mb-0 text-right">Employees</p>-->
                  <!--          <div class="fluid-container">-->
                    <!--            <h3 class="font-weight-medium text-right mb-0">246</h3>-->
                    <!--          </div>-->
                    <!--        </div>-->
                    <!--      </div>-->
                    <!--      <p class="text-muted mt-3 mb-0">-->
                      <!--        <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales-->
                      <!--      </p>-->
                      <!--    </div>-->
                      <!--  </div>-->
                      <!--</div>-->
                    </div>
                    <div class="row">
                      <div class="col-lg-12 grid-margin stretch-card">
                       <div class="card">
                        <div class="card-body">
                         <!-- <canvas id="myChart"></canvas> -->

                         <figure class="highcharts-figure">
                          <div id="container"></div>
                         
                        </figure>
                      </div>
                    </div>
                  </div>


                   <div class="col-lg-12 grid-margin stretch-card">
                       <div class="card">
                        <div class="card-body">
                         <!-- <canvas id="myChart"></canvas> -->

                         <figure class="highcharts-figure">
                          <div id="container-bulan"></div>
                         
                        </figure>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-4">
                   <div class="card card-statistics">
                    <div class="card-body">
                      <h4 class="text-primary">Produk Terlaris</h4>
                      <table class="table table-custom">
                        <?php foreach ($produk_best as $row) { ?>
                         <tr>
                          <td style="text-align: left;"><?php echo $row['nama']; ?></td>
                          <td style="text-align: right;"><?php echo $row['total_qty']; ?></td>
                        </tr>
                      <?php } ?>

                    </table>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
               <div class="card card-statistics">
                <div class="card-body">
                  <h4 class="text-primary">Minimal Stok</h4>
                  <table class="table table-custom">
                    <?php foreach ($produk_stok as $row) { 
                      if($row['sisa_stok'] <= $row['stok_alert']){
                        ?>
                        <tr>
                          <td style="text-align: left;"><?php echo $row['nama']; ?></td>
                          <td style="text-align: right;" class="text-danger">
                            <?php echo $row['sisa_stok']; ?> 
                          </td>
                        </tr>
                        <?php
                      }
                    } ?>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-lg-4">
             <div class="card card-statistics">
              <div class="card-body">
                <h4 class="text-primary">Penjualan Kasir Hari ini</h4>
                <table class="table table-custom">
                  <?php foreach ($sales as $row) { ?>
                    <tr>
                    <td style="text-align: left;"><?php echo $row['nama']; ?></td>
                    <td style="text-align: right;">Rp<?php echo decimals($row['total']); ?></td>
                  </tr>
                  <?php } ?>
                  
                </table>
              </div>
            </div>
          </div>
        </div>

<!--             <div class="row" style="margin-top: 20px;">
              <div class="col-lg-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Orders</h4>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>
                              #
                            </th>
                            <th>
                              First name
                            </th>
                            <th>
                              Progress
                            </th>
                            <th>
                              Amount
                            </th>
                            <th>
                              Sales
                            </th>
                            <th>
                              Deadline
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="font-weight-medium">
                              1
                            </td>
                            <td>
                              Herman Beck
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $ 77.99
                            </td>
                            <td class="text-danger"> 53.64%
                              <i class="mdi mdi-arrow-down"></i>
                            </td>
                            <td>
                              May 15, 2015
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-medium">
                              2
                            </td>
                            <td>
                              Messsy Adam
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $245.30
                            </td>
                            <td class="text-success"> 24.56%
                              <i class="mdi mdi-arrow-up"></i>
                            </td>
                            <td>
                              July 1, 2015
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-medium">
                              3
                            </td>
                            <td>
                              John Richards
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $138.00
                            </td>
                            <td class="text-danger"> 28.76%
                              <i class="mdi mdi-arrow-down"></i>
                            </td>
                            <td>
                              Apr 12, 2015
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-medium">
                              4
                            </td>
                            <td>
                              Peter Meggik
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $ 77.99
                            </td>
                            <td class="text-danger"> 53.45%
                              <i class="mdi mdi-arrow-down"></i>
                            </td>
                            <td>
                              May 15, 2015
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-medium">
                              5
                            </td>
                            <td>
                              Edward
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $ 160.25
                            </td>
                            <td class="text-success"> 18.32%
                              <i class="mdi mdi-arrow-up"></i>
                            </td>
                            <td>
                              May 03, 2015
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-medium">
                              6
                            </td>
                            <td>
                              Henry Tom
                            </td>
                            <td>
                              <div class="progress">
                                <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0"
                                aria-valuemax="100"></div>
                              </div>
                            </td>
                            <td>
                              $ 150.00
                            </td>
                            <td class="text-danger"> 24.67%
                              <i class="mdi mdi-arrow-down"></i>
                            </td>
                            <td>
                              June 16, 2015
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->

          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>

      <script>
        //var ctx = document.getElementById("myChart").getContext('2d');
  // var myLineChart = new Chart(ctx, {
  //   label: '# of Orders',
  //   type: 'line',
  //   data: [{
  //     x: 10,
  //     y: 20
  //   }, {
  //     x: 15,
  //     y: 10
  //   }]
  // });


  // var myChart = new Chart(ctx, {
  //   type: 'bar',
  //   data: {
  //     labels: <?php //echo json_encode($hari); ?>,
  //     datasets: [{
  //       label: '# of Orders',
  //       data: <?php //echo json_encode($total); ?>,
  //       borderWidth: 1,
  //       backgroundColor: '#5983e8',
  //     }]
  //   },
  //   options: {
  //     scales: {
  //       yAxes: [{
  //         ticks: {
  //           beginAtZero:true
  //         }
  //       }]
  //     }
  //   }
  // });


  Highcharts.chart('container', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'Daily Sales'
  },
  // subtitle: {
  //   text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: <?php echo json_encode($hari); ?>
  },
  yAxis: {
    title: {
      text: 'Total Sales'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: 'Sales',
    data: <?php echo json_encode($total); ?>
  }]
});


    Highcharts.chart('container-bulan', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'Penjualan Bulanan'
  },
  // subtitle: {
  //   text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: <?php echo json_encode($bulane); ?>
  },
  yAxis: {
    title: {
      text: 'Total Penjualan'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: 'Penjualan',
    data: <?php echo json_encode($total_bulanan); ?>
  }]
});


  
</script>
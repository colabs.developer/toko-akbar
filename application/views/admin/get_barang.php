  <div class="row">
   <div id="food1">
    <div class="col s6 m7 jmlbarang">
      <div class="card">
        <div class="card-image">
          <img src="http://trial.lembubarokah.com/foto_produk/{{ x.foto }}">

        </div>
        <div class="card-content" style="padding: 5px 10px;">
         <h3 style="font-size: 1.3rem;    margin: 1.36rem 0 0.8rem 0;">{{ x.nama }}</h3>
         <p style="color: #212121;font-weight: 600;">{{ x.harga }}</p>
       </div>
       <div class="card-action">

         <button class="waves-effect waves-light btn" id="btn-modal{{ x.number }}" type="button" key="{{ x.id_barang }}" style="background: #03A9F4;color: #FFF;border-radius: 20px;width: 100%;">Beli</button>


       </div>
     </div>
   </div>
 </div>

</div>
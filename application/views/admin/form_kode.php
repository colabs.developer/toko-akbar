<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">

            <div class="basic-form">
              <form method="POST" id="formdata">

               <div class="row">
                <div class="col-lg-6">
                  <h4 class="card-title">Kode Produk</h4>
                </div>
                <div class="col-lg-6" style="text-align: right;">
                 <button type="submit" class="btn btn-success btn-fw btn-submit">Simpan</button>
               </div>
             </div>

             <div class="row">
              <div class="col-md-6">                                                               


                <div class="form-group">
                <label>Kode Produk<span class="text-danger" title="Reuired">*</span></label>
                <input name="kode_produk" type="text" class="form-control input-default " placeholder="Kode Produk" value="<?php if(isset($id)){ echo $kode['kode_produk']; } ?>" required>
              </div>

               <div class="form-group">
                <label>Nama Kode <span class="text-danger" title="Reuired">*</span></label>
                <input name="nama_kode" type="text" class="form-control input-default " placeholder="Nama Kode" value="<?php if(isset($id)){ echo $kode['nama_kode']; } ?>" required>

                <?php if(isset($id)){ ?>
                  <input type="hidden" name="id_kode" value="<?php if(isset($id)){ echo $kode['id']; } ?>">
                <?php } ?>
              </div>


            </div>


          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

$('#formdata').submit(function(event){
  event.preventDefault();
  

  var formdata = $(this).serialize() + "&status=" + status;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_kode'; ?>",
    data: formdata,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data){

      if(data == 1)
      {

        window.location = "<?php echo base_url().'admin/kode_produk'; ?>";

      }

    }
  });

});
</script>
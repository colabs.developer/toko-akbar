<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">

            <div class="basic-form">
              <form method="POST" id="formdata">

               <div class="row">
                <div class="col-lg-6">
                  <h4 class="card-title">Jenis Barang</h4>
                </div>
                <div class="col-lg-6" style="text-align: right;">
                 <button type="submit" class="btn btn-success btn-fw btn-submit">Simpan</button>
               </div>
             </div>

             <div class="row">
              <div class="col-md-6">   

                <div class="form-group">
                <label>Kategori <span class="text-danger" title="Reuired">*</span></label>
                <select name="id_kategori" class="form-control">
                  <option value="">Pilih Kategori</option>
                  <?php foreach ($kategori as $row) { ?>
                    <option value="<?= $row['id_kategori']; ?>" <?php if(isset($id)){ if($jenis['id_kategori'] == $row['id_kategori']){ echo "selected"; } } ?> ><?= $row['nama_kategori']; ?></option>
                  <?php } ?>
                </select>
              </div>

               <div class="form-group">
                <label>Jenis Barang <span class="text-danger" title="Reuired">*</span></label>
                <input name="nama_jenis" type="text" class="form-control input-default " placeholder="Jenis Barang" value="<?php if(isset($id)){ echo $jenis['nama_jenis']; } ?>" required>

                <?php if(isset($id)){ ?>
                  <input type="hidden" name="id" value="<?php if(isset($id)){ echo $jenis['id']; } ?>">
                <?php } ?>
              </div>

              

              <div class="form-group">
                <label>Status Display <span class="text-danger" title="Reuired">*</span></label><br>
                <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="Inactive" id="toggle-status" data-onstyle="success">
              </div>

            </div>


          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

 <?php if(isset($id)){ 
  if($jenis['status'] == 1){
    ?>
    $('#toggle-status').bootstrapToggle('on');
    <?php
  }
}
?>



$('#formdata').submit(function(event){
  event.preventDefault();

  if($('#toggle-status').is(':checked')){
    status = 1;
  }else{
    status = 0;
  }

  var formdata = $(this).serialize() + "&status=" + status;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_jenis'; ?>",
    data: formdata,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data){

      if(data == 1)
      {

        window.location = "<?php echo base_url().'admin/jenis'; ?>";

      }

    }
  });

});
</script>
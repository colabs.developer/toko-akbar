 <style type="text/css">
   .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }

  .table td img, .table th img {
    border-radius: 0%;
  }
</style>
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <p>Note : Import produk file excel harus menggunakan format berikut</p>
                <a href="<?php echo base_url().'assets/format/format_excel.xlsx'; ?>" class="btn btn-info" download>Download Format Excel</a>
                <a href="<?php echo base_url().'marketing/produk_excel' ?>" target="_blank" class="btn btn-primary" download>Download Produk</a>
                <a href="#" id="btn-barcode" class="btn btn-warning">Genrate Barcode</a>
              </div>
              <div class="col-lg-6">
                 <form class="form-excel">
              <div class="form-group">
                <input type="file" name="userfile" class="form-control">
                <div class="invalid-feedback">
                File excel harus di isi
              </div>
              </div> 

              <button type="submit" class="btn btn-primary btn-upload">Upload Excel</button>

              
            </form>
              </div>
            </div>
           
          </div>
        </div>
      </div>

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Produk</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              <a href="<?php echo base_url().'admin/form_barang' ?>" class="btn btn-success btn-fw">Add</a>
            </div>
          </div>


          <div class="table-responsive">
            <table class="table table-striped table-data">
              <thead>
                <tr>
                  <th>#</th>
                  <th> Nama </th>
                  <th> Kode Produk </th>
                  <th> Kategori </th>
                  <th> Harga Beli (Rp) </th>
                  <th> Harga Jual (Rp) </th>
                  <th> Status </th>
                  <th> Action </th>
                </tr>
              </thead>
              <tbody>


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>


<div class="modal fade" id="produkmdl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            
          </div>
          <div class="col-lg-12">
            <p>Nama Produk : <span id="detail_nama"></span></p>
            <p>Barcode : <span id="detail_barcode"></span></p>
            <p>Kategori : <span id="detail_kategori"></span></p>
            <p>Satuan : <span id="detail_unit"></span></p>
            <p>Notif Stock : <span id="detail_stok"></span></p>
            <p>Supplier : <span id="detail_supplier"></span></p>
            <p>Keterangan : <span id="detail_desc"></span></p>
             <hr>
          </div>
            
           <div class="col-lg-12" style="    background: #3d3d3d;
    color: #ffffff;
    color: #ece4e0;
    text-align: center;
    border-radius: 5px;
    padding: 7px 0px;
    margin-bottom: 20px;"><p style="margin:0px;">Harga</p></div>

          <div class="col-lg-12">
            <p>Harga Beli : <span id="detail_beli"></span></p>
            <p>Harga Jual : <span id="detail_satu"></span></p>
            <p>Harga Konsi : <span id="detail_tiga"></span></p>
          </div>

     

         
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalbarcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Total Print</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>Masukkan Total yang ingin di print</label>
        <input type="number" id="total_barcode" class="form-control" /> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="kirim-barcode"><i class="mdi mdi-printer"></i> Print Barcode</button>
      </div>
    </div>
  </div>
</div>





<script>
  var tabel = null;

  $(document).ready(function() {

    fecthing_data();
    function fecthing_data(){


      tabel = $('.table-data').DataTable({
        "processing": true,
        "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax":
            {
                "url": "<?php echo base_url('admin/barang_view') ?>", // URL file untuk proses select datanya
                "type": "POST"
              },
              "deferRender": true,
              "aLengthMenu": [[10, 50, 100],[ 10, 50, 100]],
              "columns": [
                { 
            "render": function ( data, type, row ) {

              var html  = "<input type='checkbox' name='id_produk' value='"+row.id+"' /> ";

              return html


            }
          },
              { "data": "nama" },
              { "data": "kode_produk" },
              { "data": "nama_kategori" },
              { "data": "harga_beli" },
              { "data": "harga" }, 
              { "render": function (data, type, row) {
               var html ="";

               if(row.status == 1){
                html = '<span class="badge badge-success"><i class="mdi mdi-check-circle"></i></span>';
              }else{
                html = '<span class="badge badge-danger"><i class="mdi mdi-close-circle"></i></span>';
              }

              return html;
            }
          },
          { 
            "render": function ( data, type, row ) {
              var html  = "<a class='btn btn-info btn-sm btn-detail'  key='"+row.id+"'><i class='mdi mdi-eye' style='color:#FFF;'></i></a> <a class='btn btn-primary btn-sm' href='<?php echo base_url().'admin/form_barang/'; ?>"+row.id+"'><i class='mdi mdi-pencil-circle'></i></a> "
              html += "<a class='hapus btn btn-danger btn-sm' onclick='delete()' key='"+row.id+"' style='color:#FFF;'><i class='mdi mdi-delete-circle'></i></a>"

              return html


            }
          },
          ],
        });

    }

    $( document ).on("click", ".hapus", function(event){

      event.preventDefault()
      var id = $(this).attr('key');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'admin/remove_barang'; ?>",
        data: {id:id},
        success: function(data) {
          if(data == 1)
          {
            $('.table-data').DataTable().destroy();
            fecthing_data();
          }

        }
      });
    });

    $(document).on("click", ".btn-detail", function(event){
      var id_produk = $(this).attr('key');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'admin/get_detail_produk'; ?>",
        data: {id_produk:id_produk},
        dataType: 'json',
        success: function(data) {

          $('#exampleModalLabel').text(data.nama);
          $('#detail_nama').text(data.nama);
          $('#detail_barcode').text(data.barcode);
          $('#detail_kategori').text(data.kategori);
          $('#detail_unit').text(data.unit);
          $('#detail_stok').text(data.stok);
          $('#detail_supplier').text(data.supplier);
          $('#detail_desc').text(data.keterangan);
          $('#detail_nama').text(data.nama);
          $('#detail_beli').html(data.harga_beli);
          $('#detail_satu').html(data.harga);
          $('#detail_tiga').html(data.harga_3_satuan);

        }
      });

      $('#produkmdl').modal('show');

    });

  });


  $('.form-excel').submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);

    var inputexcel = $('input[name="userfile"]').val();
    if(inputexcel == ''){
      $('input[name="userfile"]').addClass('is-invalid');
      return false;
    }

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'import/upload'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      $('.btn-upload').prop('disabled', true);
      $('.btn-upload').removeClass('btn-primary').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {
      swal({
      icon: "success",
      text: "Succes import products",
    });
      $('.btn-upload').prop('disabled', false);
      $('.btn-upload').removeClass('btn-secondary').addClass('btn-primary').text('Upload Excel');
    
    }
  });
  });


  $('#btn-barcode').click(function(){

    $('#modalbarcode').modal('show');

  });

  $('#kirim-barcode').click(function(){
    var id_produk = []; 
    var total = $('#total_barcode').val();

    var is_checked = $('input[name="id_produk"]:checked').length;
   
    if(parseInt(is_checked) <= 0){
      swal({
      icon: "warning",
      text: "Pilih produk yang akan di print barcode",
    });
    $('#modalbarcode').modal('hide');
    return false;
    }


    $('input[name="id_produk"]:checked').each(function() {
   id_produk.push(this.value);
});




$.ajax({
        type: "POST",
        url: "<?php echo base_url().'printer/barcode_produk'; ?>",
        data: {id_produk:id_produk, total:total},
        success: function(data) {
          var w = window.open("<?= base_url().'printer/barcode_produk'; ?>");
          w.document.open();
          w.document.write(data);

          location.reload();
          w.document.close();

        }
      });
  });
</script>
<style type="text/css">
  thead tr th:last-child
  {
    text-align: left;
  }
  tbody tr td:last-child
  {
    text-align: center;
  }
  .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }

</style>
<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Merek Barang</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              <a href="<?php echo base_url().'admin/form_merek' ?>" class="btn btn-success btn-fw">Tambah</a>
            </div>
          </div>


          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#ID</th>
                  <th>Merek Barang</th>
                  <th>Status</th>
                  <th>Action</th>
                  <!-- <th>Hapus</th> -->
                </tr>
              </thead>
              <tbody>
                <?php 
                $no =1;
                foreach ($merek as $row) {

                 if($row['status'] == 1){
                  $status = '<span class="badge badge-success">Aktive</span>';
                }else{
                  $status = '<span class="badge badge-danger">Inaktif</span>';
                }

                ?>
                <tr>
                  <th scope="row"><?php echo $row['id']; ?></th>
                  <td><?php echo $row['nama_merek']; ?></td> 
                  <td><?php echo $status; ?></td>                                   
                  <td style="text-align: center;"><a href="<?php echo base_url().'admin/form_merek/'.$row['id']; ?>" class="btn btn-primary"><i class='mdi mdi-pencil-circle'></i>Ubah</a>
                   <a class="hapus btn btn-danger btn-sm" key="<?php echo $row['id']; ?>" style='color:#FFF;'><i class="mdi mdi-delete-circle"></i>Non Aktif</a></td>
                 </tr>
                 <?php
                 $no++;
               } ?>
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>

 </div>
</div>
</div>



<script type="text/javascript">
  $('.hapus').click(function(event){
    event.preventDefault();
    var id_merek = $(this).attr('key');


    swal({
      title: "Apakah kamu yakin ?",
      text: "Menonaktifkan merek ini",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'admin/remove_merek'; ?>",
          data: {id_merek:id_merek},
          success: function(data){

            if(data == 1)
            {
              location.reload();
            
           }

         }
       });


      } else {

      }
    });




  });
</script>


<style type="text/css">
  thead tr th:last-child
  {
    text-align: left;
  }
  tbody tr td:last-child
  {
    text-align: center;
  }
  .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }

</style>
<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Kode Produk</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              <a href="<?php echo base_url().'admin/form_kode_produk' ?>" class="btn btn-success btn-fw">Tambah</a>
            </div>
          </div>


          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#ID</th>
                  <th>Kode Produk</th>
                  <th>Nama Kode</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $no =1;
                foreach ($kode as $row) {
                ?>
                <tr>
                  <td scope="row"><?php echo $row['id']; ?></td>
                  <td><?php echo $row['kode_produk'] ?></td>
                  <td><?php echo $row['nama_kode']; ?></td>                                  
                  <td style="text-align: center;"><a href="<?php echo base_url().'admin/form_kode_produk/'.$row['id']; ?>" class="btn btn-primary"><i class='mdi mdi-pencil-circle'></i>Ubah</a>
                  </td>
                 </tr>
                 <?php
                 $no++;
               } ?>
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>

 </div>
</div>
</div>



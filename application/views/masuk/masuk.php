<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Login Admin - AKBAR</title>

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.base.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.addons.css'; ?>">

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url().'favicon.ico'; ?>" />

	<script type="text/javascript" src="<?php echo base_url().'admin_assets/js/jquery.min.js'; ?>"></script>

	
</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
			<div class="content-wrapper d-flex align-items-center auth theme-one" style="background-color: #2c3e50;">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auto-form-wrapper">
							<center>
								<img src="<?php echo base_url().'assets/logo.svg'; ?>">
							</center>
							<form class="form-login">
								<div class="form-group">
									<label class="label">Email</label>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Email" id="email" name="email">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outlines"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Password</label>
									<div class="input-group">
										<input type="password" class="form-control" placeholder="" id="txtPassword" name="password">
										<div class="input-group-append">
											<span class="input-group-text" id="btnToggle">
												<i class="mdi mdi-eye-off" id="eyeIcon"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success submit-btn btn-block" id="login">Masuk</button>
								</div>
								<!-- <p>Daftar akun <a href="<?php //echo base_url().'masuk/daftar'; ?>"> disini</a></p> -->
							</form>
						</div>
						<p class="footer-text text-center">copyright © <?php echo date('Y'); ?> All rights reserved.</p>
					</div>
				</div>
			</div>

		</div>

	</div>
	<script type="text/javascript">
		$('.form-login').submit(function(){
			event.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				type: "POST",
				url: "<?php echo base_url().'masuk/dologin'; ?>",
				data:formData,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(data) {
					if(data == 1)
					{
						window.location = '<?php echo base_url().'admin'; ?>';
					}else{
						swal({
							icon: "error",
							text: "Incorrect email or password",
						});
					}
				}
			});
		});


		let passwordInput = document.getElementById('txtPassword'),
    toggle = document.getElementById('btnToggle'),
    icon =  document.getElementById('eyeIcon');

function togglePassword() {
  if (passwordInput.type === 'password') {
    passwordInput.type = 'text';
    $('#eyeIcon').removeClass('mdi-eye-off');
    $('#eyeIcon').addClass('mdi-eye');
    //toggle.innerHTML = 'hide';
  } else {
    passwordInput.type = 'password';
    $('#eyeIcon').addClass('mdi-eye-off');
    $('#eyeIcon').removeClass('mdi-eye');
    //toggle.innerHTML = 'show';
  }
}


toggle.addEventListener('click', togglePassword, false);

	</script>

	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.base.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.addons.js'; ?>"></script>

	<script src="<?php echo base_url().'admin_assets/js/off-canvas.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/js/misc.js'; ?>"></script>

</body>

</html>
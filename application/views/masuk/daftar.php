<link href="<?php echo base_url().'admin_assets/css/helper.css'; ?>" rel="stylesheet">
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Login Admin - UPOS</title>

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.base.css'; ?>">
	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.addons.css'; ?>">

	<link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url().'favicon.ico'; ?>" />

	<script type="text/javascript" src="<?php echo base_url().'admin_assets/js/jquery.min.js'; ?>"></script>

	<style type="text/css">
		.auth.auth-bg-1{
			background: url('<?php echo base_url().'assets/bg-login2.png'; ?>') !important;
			background-size: cover;
		}
	</style>
</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
			<div class="content-wrapper d-flex align-items-center auth theme-one" style="background-color: #2c3e50;">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auto-form-wrapper">
							<center>
								<img src="<?php echo base_url().'assets/logo.svg'; ?>">
							</center>
							<form class="form-daftar">
								<div class="form-group">
									<label class="label">Nama Usaha</label>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="" name="nama_usaha">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Alamat Usaha</label>
									<div class="input-group">
										<input type="text" class="form-control" name="alamat_usaha">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Nama Pemilik</label>
									<div class="input-group">
										<input type="text" class="form-control" name="nama_pemilik">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Nomor Telephone</label>
									<div class="input-group">
										<input type="text" class="form-control" name="tlp">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Email</label>
									<div class="input-group">
										<input type="text" class="form-control" name="email">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Password</label>
									<div class="input-group">
										<input type="password" class="form-control" name="password">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<button class="btn btn-success submit-btn btn-block" type="submit">Daftar</button>
								</div>
								<p>Login akun <a href="<?php echo base_url().'masuk'; ?>"> disini</a></p>
							</form>
						</div>
						<ul class="auth-footer">
							<li>
								<a href="#">Conditions</a>
							</li>
							<li>
								<a href="#">Help</a>
							</li>
							<li>
								<a href="#">Terms</a>
							</li>
						</ul>
						<p class="footer-text text-center">copyright © 2018 All rights reserved.</p>
					</div>
				</div>
			</div>

		</div>

	</div>
	<script type="text/javascript">
		$('.form-daftar').submit(function(){
			event.preventDefault();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url().'masuk/doregis'; ?>",
				data: $(this).serialize(),
				success: function(data) {
					
					if(data == 1){
						$('.form-control').val('');
						swal({
							text: "Pedaftaran Akun Berhasil !",
							icon: "success",
							 buttons: true,
						});
					}
				}
			});
		});
		


	</script>

	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.base.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/vendors/js/vendor.bundle.addons.js'; ?>"></script>

	<script src="<?php echo base_url().'admin_assets/js/off-canvas.js'; ?>"></script>
	<script src="<?php echo base_url().'admin_assets/js/misc.js'; ?>"></script>

</body>

</html>